---
title: License
draft: true
weight: 750
---
## official

```
649TX-2NJT6-T8J6H-WPCB9-9HMM4
```


## unofficial

```cmd
slmgr /ipk MH37W-N47XK-V7XM9-C7227-GCQG9

slmgr /skms kms8.msguides.com

slmgr /ato
```


{{<alert type="info">}}
Home: TX9XD-98N7V-6WMQ6-BX7FG-H8Q99
Home N: 3KHY7-WNT83-DGQKR-F7HPR-844BM
Home Single Language: 7HNRX-D7KGG-3K4RQ-4WPJ4-YTDFH
Home Country Specific: PVMJN-6DFY6–9CCP6–7BKTT-D3WVR
Professional: W269N-WFGWX-YVC9B-4J6C9-T83GX
Professional N: MH37W-N47XK-V7XM9-C7227-GCQG9
Education: NW6C2-QMPVW-D7KKK-3GKT6-VCFB2
Education N: 2WH4N-8QGBV-H22JP-CT43Q-MDWWJ
Pro Education Key: 6TP4R-GNPTD-KYYHQ-7B7DP-J447Y
Pro Education N Key: YVWGF-BXNMC-HTQYQ-CPQ99-66QFC
Enterprise: NPPR9-FWDCX-D2C8J-H872K-2YT43
Enterprise N: DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4
{{</alert>}}


