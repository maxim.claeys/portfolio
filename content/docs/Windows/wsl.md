---
title: WSL
draft: false
weight: 800
---

## systemd fix
```sh
sudo apt update
sudo apt install -y git
git clone https://github.com/DamionGans/ubuntu-wsl2-systemd-script.git
cd ubuntu-wsl2-systemd-script/
bash ubuntu-wsl2-systemd-script.sh
```

## display environment
```sh
echo "export DISPLAY=$(route.exe print | grep 0.0.0.0 | head -1 | awk '{print $4}'):0.0" | tee ~/.bashrc
```

## resolv.conf
```sh
sudo unlink /etc/resolv.conf
sudo cp /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
sudo systemctl restart systemd-resolved
sudo systemctl restart tailscaled
```
