---
title: Setup - Winget
draft: false
weight: 750
---

## Winet script
```ps1
Set-TimeZone "Romance Standard Time"

winget install bravesoftware.bravebrowser
winget install valve.steam
winget install discord.discord
winget install notepad++.notepad++
winget install git.git
winget install videolan.vlc
winget install plex.plex
winget install vscode
winget install docker.dockerdesktop
winget install ModernFlyouts.ModernFlyouts

winget install Lexikos.AutoHotkey
winget install RaspberryPiFoundation.RaspberryPiImager
winget install Spotify.Spotify
winget install StefanSundin.Superf4
winget install Microsoft.Teams
winget install Ghisler.TotalCommander
winget install PuTTY.PuTTY
winget install RARLab.WinRAR
winget install gerardog.gsudo
winget install VMware.WorkstationPro
winget install Microsoft.PowerToys
winget install telegram.telegramdesktop
winget install Logitech.OptionsPlus
winget install Logitech.LogiBolt


windows terminal
everything

choco install firacode

```