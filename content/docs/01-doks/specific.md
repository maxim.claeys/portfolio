---
title: doks specific
draft: false
weight: 10
---

## head
```yml
---
title: Testen
page_title: Testen van het resultaat
draft: false
weight: 31
---

```

## Docusaurus Admonitions
```sh
find . -type f -name '*.md' | xargs sed -i 's/:::note/{{</*alert type="note"*/>}}/g'
find . -type f -name '*.md' | xargs sed -i 's/:::tip/{{</*alert type="tip"*/>}}/g'
find . -type f -name '*.md' | xargs sed -i 's/:::info/{{</*alert type="info"*/>}}/g'
find . -type f -name '*.md' | xargs sed -i 's/:::caution/{{</*alert type="caution"*/>}}/g'
find . -type f -name '*.md' | xargs sed -i 's/:::warning/{{</*alert type="danger" text="warning"*/>}}/g'
find . -type f -name '*.md' | xargs sed -i 's/:::danger/{{</*alert type="danger"*/>}}/g'
find . -type f -name '*.md' | xargs sed -i 's@:::@{{</*/alert*/>}}@g'
```

{{<alert type="caution">}}
  f
{{</alert>}}

{{<alert type="danger">}}
  Some content with markdown `syntax`.
{{</alert>}}

{{<alert type="info">}}
  Some content with markdown `syntax`.
{{</alert>}}

{{<alert type="note">}}
  Some content with markdown `syntax`.
{{</alert>}}

{{<alert type="tip">}}
  Some content with markdown `syntax`.
{{</alert>}}



{{< details "Help me choose" >}} Not sure which one is for you? Pick the child theme. {{< /details >}}