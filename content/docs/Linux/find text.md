---
title: Find
page_title: Finding files and text in files
draft: false
weight: 20
---

## finding files

```sh
find . -type f -name '*.md'
```

## finding text in files

```sh
grep -rnw '/path/to/somewhere/' -e 'pattern'
```
{{<alert type="info">}}
- `-r` or `-R` is recursive,
- `-n` is line number, and
-`-w` stands for match the whole word.
- `-l` (lower-case L) can be added to just give the file name of matching files.
- `-e` is the pattern used during the search
{{</alert>}}

### exclude & include

This will only search through those files which have specific extensions:
```sh
grep --include=\*.{txt,yml} -rnw '/path/to/somewhere/' -e "pattern"
grep --exclude=\*.py -rnw '/path/to/somewhere/' -e "pattern"
```

For directories it's possible to exclude one or more directories using the --exclude-dir parameter. For example, this will exclude the dirs dir1/, dir2/ and all of them matching *.dst/:
```sh
grep --exclude-dir={dir1,dir2,*.dst} -rnw '/path/to/somewhere/' -e "pattern"
```

## replace text
### single file

Replace $var1 with $var2
```sh
sed -i 's/$var1/$var2/g' file.txt
```

example: will replace `page-title:` `with page_title:`
```sh
sed -i 's/page-title:/page_title:/g' file.txt 
```

### multiple files (find)

Replace $var1 with $var2 in any files ending in `*.md`
```sh
find . -type f -name '*.md'| xargs sed -i 's/$var1/$var2/g'
```
{{<alert type="info">}}
- `.` directory to search in
- `f` searching for files
- `*.md` search pattern 
{{</alert>}}


example: 
will replace `page-title:` `with page_title:` in all `markdown` files under the directory `.`
```sh
find . -type f -name '*.md'| xargs sed -i 's/page-title:/page_title:/g'
```

replace `weight: 9` with `weight: 10`
```sh
find . -type f -name '*.md' | xargs sed -ri 's/(.*)(weight: )([0-9]+)(.*)/echo "\1\2$((\3+1))\4"/ge'
```
{{<alert type="info">}}
- `.` directory to search in
- `f` searching for files
- `*.md` search pattern 

to change the amount change the `+1` in `"\1\2$((\3+1))\4"`
{{</alert>}}





