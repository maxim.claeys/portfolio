---
title: Virtual Machines
draft: false
weight: 420
---

## debian vm script

```sh
echo "export DISPLAY=$(route.exe print | grep 0.0.0.0 | head -1 | awk '{print $4}'):0.0" | tee ~/.bashrc
apt update
apt upgrade -y
apt install curl gpg gnupg2 pass -y
curl -fsSL https://tailscale.com/install.sh | sh
systemctl start tailscaled

sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
service ssh restart

sed -i 's/#DNSStubListener=yes/DNSStubListener=no/' /etc/systemd/resolved.conf
systemctl restart systemd-resolved

mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
  
apt update
apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-compose -y
```

## groups and users

```sh
groupadd -g 1000 maxim
groupadd -g 1111 Docker
groupadd -g 2001 kiara
groupadd -g 2002 robin
groupadd -g 1001 WORKGROUP
useradd maxim -u 1000 -g 1000 -m -s $SHELL
useradd Docker -u 1111 -g 1111 -m -s $SHELL
useradd kiara -u 2001 -g 2001 -m -s $SHELL
useradd robin -u 2002 -g 2002 -m -s $SHELL

usermod -aG WORKGROUP maxim
usermod -aG WORKGROUP Docker
usermod -aG WORKGROUP kiara
usermod -aG WORKGROUP robin
```
