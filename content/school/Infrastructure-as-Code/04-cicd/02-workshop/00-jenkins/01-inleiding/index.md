---
title: Inleiding
draft: false
weight: 49
---

Jenkins is een CI/CD server die al heel wat jaren op z'n teller heeft. Het resultaat is een uitgebreide community en veel integratiemogelijkheden. In deze workshop gaan we vooral aan de slag met enkele continuous integration-mogelijkheden.

## Voorbereiding

Voor deze opdracht is een Linux VM nodig. De opgave werd getest op Ubuntu 22.04, maar zal met minimale wijzigingen ook werken op andere recente distributies.
Je vindt een deployment op de interne cloud ("OPO_Infra_As_Code_CICD_workshop") die twee Ubuntu servers bevat.

- Deploy deze configuratie met twee servers.
- Installeer de laatste updates op de vm
- Installeer Java 11: `sudo apt-get install default-jdk`
- Verifieer de versie: `java -version` (je zou '11' moeten zien)
- We zullen met een Python project werken. Installeer dus meteen ook pip al: `apt install python3-pip`
- wijzig METEEN de wachtwoorden op deze servers zodat enkel jij die kent. Dat is nodig om de persoonlijke gegevens later in dit labo goed te beveiligen.

Maak een fork aan van [https://gitlab.com/ikdoeict/docenten/2122_infrastructure_as_code/labs/2122_iac-docker_flask_demo](https://gitlab.com/ikdoeict/docenten/2122_infrastructure_as_code/labs/2122_iac-docker_flask_demo)

Deze repo bevat een eenvoudige website, die we zullen gebruiken om de continuous integration te testen.

## Installatie Jenkins

We zullen Jenkins installeren op de eerste server (Server1)

```bash
# add the jenkins repostory key
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
# Toevoegen van repo aan je systeem  
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
# updaten van package list
sudo apt-get update
# installatie jenkins
sudo apt-get install jenkins
# enablen en starten van de jenkins service
sudo systemctl enable jenkins
sudo systemctl start jenkins
```

Als de server opgestart werd, moet het mogelijk zijn om de webinterface te bereiken op `[ip-adres]:8080`.

Log daar in met het wachtwoord dat je op je server kan vinden (lees de instructies op het scherm).

Doorloop daarna de wizard:

- Kies ervoor om de lijst met plugins zelf te kiezen
- Behoud de voorgestelde plugins, maar vul aan met deze extra plugins
  - Configuration as code
  - Gitlab
  - Publish over ssh

Niet alle plugins staan overigens in de lijst. Later in deze opgave zullen we er nog enkele toevoegen.

Maak als laatste stap van de wizard een user aan. Bewaar het wachtwoord uiteraard zorgvuldig.

{{<alert type="tip">}}
Na het aanmelden met de aangemaakte gebruik is het aangewezen om in je profiel de timezone in te stellen.
Anders zullen alle timestamps in UTC vermeld worden...
{{</alert>}}
