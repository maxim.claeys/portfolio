---
title: Freestyle Projects
page_title: Aanmaken van een freestyle project
draft: false
weight: 50
---

In Jenkins is alles opgebouwd rond projecten, die op één of meerdere servers kunnen uitgevoerd worden.

* Klik op het dashboard op "Create an item" en kies voor "Freestyle project"
* Gebruik `Mijn Freestyle project` als naam voor het eerste project dat we zullen aanmaken.

## Configureer de Job

Voeg je eigen repository met de Flask demo (die je geforked hebt) toe aan "source code management".
Daarvoor zal je ook een private key moeten toevoegen aan je Jenkins installatie, zodat die in jouw naam repo's kan binnen halen.

**Doe dit met de nodige omzichtigheid, je werkt met (je eigen) authenticatiegegevens!**

Maak hiervoor een nieuw keypair aan op je computer, en voeg die keys toe aan zowel je Jenkins project (private key) als Gitlab (public key).
Stel de geldigheid van de key op Gitlab in op slechts enkele weken. Na deze workshop zullen we de key ook terug verwijderen uit Gitlab, zodat je keys steeds veilig blijven.

```bash
ssh-keygen -t ed25519 -C "Gitlab key (lab IaC)"
```

![private key](add_private_key.png)

![repo_connected](repogekoppeld.png)

## Configuratie van de steps

* in het tabblad 'Build' in het project kan je build stappen toevoegen. Voeg (onderaan) een 'execute shell' build step toe, met als inhoud

```bash
pytest-3 *
```

Met deze stap willen we controleren de testen in de code allemaal goed verlopen. Anders zal de build step falen. Op deze manier zijn ontwikkelaars extra gemotiveerd om voldoende commentaar te gebruiken, en de regels te volgen ivm naamgeving, ...

Bewaar deze config, en voer deze 'build' manueel uit.

![failed_build](buildfailed.png)

Ongetwijfeld faalt je build, en de oorzaak daarvan kan je vinden in de build output. Welke package ontbreekt nog?
Installeer die package (manueel, met apt-get) op je server1.

{{<alert type="caution">}}
Opgelet: sommige packages bestaan in meerdere versies (bijvoorbeeld voor Python2 en Python3). Kies steeds voor de python-3 versie waar mogelijk. Installeer dus met pip3 als het gaat om Python packages...
{{</alert>}}

{{<alert type="caution">}}
Op deze manier merk je ook dat de Jenkins server standaard de build-server als omgeving zal gebruiken voor het doorlopen van z'n build-stappen. Dat is een potentieel probleem, want bij een update van die server zou het dus kunnen gebeuren dat sommige stappen niet meer werken. Beter is om builds in software containers te laten lopen, dan zijn die risico's er veel minder. Die werkwijze komt later nog aan bod...
{{</alert>}}

Voer na het toevoegen van het ontbrekende pakketten op de server de build opnieuw uit. De buildstep ziet er nu zo uit:

```bash
pip3 install -r requirements.txt
pytest-3 *
```

Ook nu krijg je nog een falende build, maar als alles goed gevolgd werd, is dat geen probleem meer in de infrastructuur, maar een echt probleem in de Python-code. Bekijk de output van de test daarvoor in de laatste build status.

![failed_build_detail](buildfaileddetail.png)

Los het (eenvoudige) probleem op, en start je build opnieuw.

Als alles goed gaat, dan zou je nu een werkende build moeten zien.

## Artefacts

Artefacts vormen de resultaten van de builds die je maakte. Afhankelijk van het type project kan dat erg in vorm verschillen. Dat kan een .exe zijn als je software gaat compileren, maar dan kan ook een zip zijn met bestanden, ...

In deze oefening is de artefact een rapport dat gemaakt wordt en een verslag bevat van de statische verificatietool pylint. Naast een score lees je er ook in welke verbeteringen mogelijk zijn om te voldoen aan de regels van de kunst binnen Python.

Voeg een nieuwe build stap bij ('execute shell')

```bash
pylint *.py --exit-zero > report.txt
```

Vergeet uiteraard niet om pylint ook te installeren op je build machine. Dat kan via een apt-get commando.

```bash
sudo apt-get install -y pylint
```

Voeg een post install stap toe ("archive the artefacts") waarin report.txt bewaard wordt.
Bij elke nieuwe build zal je nu een rapport kunnen downloaden.
