---
title: Inleiding
page_title: Over Gitlab CI
draft: false
weight: 52
---



![gitlab CI logo](gitlab-cicd.png)

Gitlab was initieel een platform om git repositories te bewaren, maar voegde enkele jaren geleden CI/CD functionaliteit toe aan z’n product.

Die functionaliteit wordt aangeboden in een SaaS-oplossing: je kan de dienst gebruiken zonder zelf in servers te voorzien.
Je kan echter ook zelf 'runners' bouwen, werkpaarden die de verschillende taken kunnen afwerken die in je pipeline gedefinieerd worden.
Het is die laatste aanpak die we gebruiken voor dit labo. De eerste stap wordt dus de installatie van zo’n runner op je eigen server…​

Intussen zijn er overigens tal van alternatieven voor deze SaaS CI/CD oplossing op de markt met gelijkaardige functionaliteit: Github Actions, Buddy, Circle CI, …​