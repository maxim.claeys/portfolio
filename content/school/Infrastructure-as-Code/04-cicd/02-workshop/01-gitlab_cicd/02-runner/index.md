---
title: Installatie runner
page_title: Runner
draft: false
weight: 53
---

# Runner

Een erg belangrijke component in Gitlab CI zijn de runners. Het zijn de werkpaarden van CI/CD: daar gebeurt het doorlopen van de pipelines.

Bij een abonnement op Gitlab.com zijn een beperkt aantal 'werk'-minuten inbegrepen in je abonnement, en hoef je die niet noodzakelijk zelf op te zetten.

![ci_pricing](ci-pricing.png)

Voor deze opgave werken we met een lokale Gitlab instantie, en dan is het nodig om de runner zelf te installeren.

Voor de installatie zijn er verschillende mogelijkheden:

* In een container
* Door de executable manueel uit te voeren
* door de installatie te doen via rpm/deb repositories

Wij zullen gebruik maken van een container. Zo is er een bepaalde graad van isolatie tussen wat uitgevoerd wordt door de runner, en het bovenliggend operating system.

Gebruik server1 voor het hosten van de runner.

Maak hiervoor eerst een volume aan. Dat is nodig om ervoor te zorgen dat de container ook bij het herstarten nog steeds z'n persistente data aan boord heeft..

```bash
docker volume create gitlab-runner-config
```

Start daarna de container met het gekoppelde volume:

```bash
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    --env TZ=Europe/Brussels \
    gitlab/gitlab-runner:latest
```

Daarna moet deze runner geregistreerd worden. Zo weet Gitlab welke runners beschikbaar zijn. Dat kan op projectniveau, of shared, waarbij en runner door meerdere repositories kan gebruikt worden.
Wij kiezen voor een 'specific runner'. Ga daarvoor naar Settings -> CI/CD -> Runners in je geforkte project dat ook gebruikt werd voor de Jenkins opgave.
Je vindt daar ook het registratietoken. Hou dat bij de hand, je hebt het zo meteen nodig...

```bash
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```

Volg de wizard:

* Geef de url van onze Gitlab-instance op: [https://gitlab.com/](https://gitlab.com/)
* als description gebruik je **iac_runner_[voornaamnaam]**
* als tag gebruik je **tag_voornaamnaam**
* De default executor die je kies is Docker. Een overzicht van de verschillende executors vind je op [https://docs.gitlab.com/runner/executors/](https://docs.gitlab.com/runner/executors/)
* als default image gebruik je **ubuntu**. Als in een pipeline geen type container gespecifieerd wordt, zal deze image gebruikt worden. Het is uiteraard geen goed idee om daarop te vertrouwen: specifieer steeds het image-type in je pipeline voor reproduceerbare builds.

Je runner staat nu klaar, te wachten op instructies...

![specific runner](specific_runner.png)
