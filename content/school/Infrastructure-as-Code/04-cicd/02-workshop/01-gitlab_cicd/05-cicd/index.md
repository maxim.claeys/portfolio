---
title: Continuous Deployment
draft: false
weight: 56
---

In de vorige oefening lag de focus voornamelijk op het continuous integration deel van Gitlab CI/CD.
Minstens even belangrijk is het gedeelte dat daarop volgt: het automatisch deployen indien de testen geslaagd zijn.

Er zijn in principe geen beperkingen in hoe je dat kan doen, al zie je uiteraard wel vaak patronen terugkeren. Je kan bijvoorbeeld vanuit je runner een https/ssh-connectie openen, en je artefacts uploaden naar een externe server.
Dat is ook wat we zullen doen in wat volgt, al ontbreken we nog één stukje informatie om die puzzel te maken. In zo'n geval moeten we immers gevoelige informatie (credentials, ...) gebruiken, het type informatie dat je kost wat kost uit je GIT-repo wil houden.

Gelukkig is daar een oplossing voor: `Variables`

## Variables

Een beetje verscholen, in settings -> CI/CD -> Variables is er de mogelijkheid om variabelen of zelfs bestanden op te geven die kunnen gebruikt worden doorheen het CI/CD process.
Dat kan je gebruiken om bijvoorbeeld credentials mee te geven, maar ook om build-time variabelen mee te geven of bijvoorbeeld een `.env` file die je typisch vindt bij PHP-projecten.
Je kan die variabelen beschermen zodat ze enkel op protected branches zichtbaar zijn, en er is de mogelijkheid om ze te maskeren in je logs.

## Oefening Continuous deployment

Continuous deployment gaan we oefenen door een statische site te publiceren naar Netlify.

* Maak je een lege repository aan op Gitlab.
* Plaats daarin een statische html website in een map 'src'. Het minimum is een enkele html-pagina met een 'hello world' in, maar het mag uiteraard ook net iets meer zijn..
* Maak een gratis account aan op Netlify, een dienst die toelaat om statische websites te hosten.
* Maak in Netlify een nieuwe site aan. Kies NIET voor 'new site from Git', maar maak een site aan door de map(!) met de statische site die je net maakte te slepen naar de 'sites'-pagina

![resultaat](netlify.png)

* We zullen geen domein koppelen, maar je krijgt een voorlopige url, en dat volstaat voor deze opgave. ([https://[random].netlify.app](https://[random].netlify.app))
* Bouw nu een gitlab-ci.yml die ervoor zal zorgen dat elke keer als code aangepast wordt in die pagina, dat dat dan meteen ook gedeployed wordt naar Netlify.
* Maak op Netlify een token aan dat je kan gebruiken om via code je content te pushen. Je vindt die optie bij je user settings.
* Maak met de verkregen informatie twee variabelen aan in je Gitlab-project: `NETLIFY_SITE_ID` en `NETLIFY_AUTH_TOKEN`

De commando's waarmee je de site kan pushen:

```bash
yarn global add netlify-cli
netlify deploy --site $NETLIFY_SITE_ID --auth $NETLIFY_AUTH_TOKEN --prod
```

Met bovenstaande commando's weet het Netlify commando nog niet welke map moet upgeload worden. Dat kan je duidelijk maken door een `netlify.toml` bestand aan te maken in je repo

```toml
[build]
  publish = "src"
```

{{<alert type="danger" text="warning">}}
De commando's die hierboven staan gaan uit van de veronderstelling dat je een container gebruikt met Yarn reeds beschikbaar. De (recente) Node images zijn een goed vertrekpunt...
{{</alert>}}
