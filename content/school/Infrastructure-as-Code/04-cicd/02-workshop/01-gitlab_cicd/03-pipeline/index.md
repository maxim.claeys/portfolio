---
title: Eerste Pipeline
page_title: Eerste pipeline in Gitlab CI/CD
draft: false
weight: 54
---

De eerste pipeline houden we heel eenvoudig, zodat je het systeem kan leren kennen.

Maak een bestand aan in de fork van de Flask-repository met als naam `.gitlab-ci.yml`

De inhoud van dat bestand:

```yml
image: centos:8

test:
  script:
    - exit 1
```

Nadat je dat pusht op je repo zal je zien dat er een pipeline zichtbaar wordt in het CI/CD menu.

De pipeline wordt ook doorlopen, maar als je de output opvraagt, zal je merken dat er een andere runner gebruikt wordt: Gitlab.com biedt die immers ook aan bij een abonnement. We willen echter onze eigen runner gebruiken.

Pas de `.gitlab-ci.yml` file aan zodat jouw runner aangesproken wordt. Gebruik daarvoor de `tag` die je opgaf bij het aanmaken van je runner.

```yml
image: centos:8

test:
  tags:
    - "tag_yourname"
  script:
    - exit 1
```

{{<alert type="tip">}}
In Gitlab zit een ingebouwde pipeline editor. Gebruik die om tikfoutjes te vermijden als je kleine aanpassingen wil doen...
{{</alert>}}

Na deze interventie zou je pipeline toch moeten doorlopen worden op jouw eigenste runner.

![resultaat](runonyourrunner.png)

Toch zal de build opnieuw falen.

Bekijk de details. Daar merk je dat de job als 'failed' wordt gemarkeerd omdat het resultaat '1' was.
Dat is meteen een belangrijk principe: om 'success' te hebben moet het resultaat van de taken die je uitvoert steeds '0' zijn. Dat lijkt initieel misschien vreemd, maar is eigenlijk een principe dat al heel lang bestaat. Een executable sluit bij afspraak af met 0 als er geen problemen zijn vastgesteld.

Wijzig de pipeline, zodat de laatste regel 'exit 0' wordt.
Je pipeline zou nu wel vlot moeten doorlopen worden...

![resultaat](resultfirstbuild.png)
