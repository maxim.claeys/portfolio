---
title: Jobs and stages
draft: false
weight: 55
---

`Jobs`: Zijn een fundamenteel onderdeel van een pipeline in Gitlab. In de YAML-configuratie van een gitlab-ci file zijn het top-level elementen, die minstens een script-subelement bevatten.

`Stages`: Een stage bevat jobs. Die jobs kunnen parallel uitgevoerd worden binnen dezelfde stage. Jobs die geen stage toegekend kregen, worden opgenomen in de test-stage, zoals ook zichtbaar is in de interface na de vorige oefening.

![resultaat](defaultstagetest.png)

```yaml
stages:
  - build
  - test

build-code-job: 
  stage: build
  script:
    - echo "Check the ruby version, then build some Ruby project files:"
    - ruby -v
    - rake

test-code-job1:
  stage: test
  script:
    - echo "If the files are built successfully, test some files with one command:"
    - rake test1

test-code-job2:
  stage: test
  script:
    - echo "If the files are built successfully, test other files with a different command:"
    - rake test2
```

In bovenstaand voorbeeld zijn er drie jobs, opgesplitst in twee stages, **build** & **test**.

## Aan de slag met Pylint

We gaan aan de slag met bovenstaande concepten, en voegen meteen enkele elementen toe. Maak een branch `test-pipelines` aan in de fork die ook voor Jenkins gebruikt werd.

We zullen een deel van de testen die we deden in Jenkins hernemen met Gitlab CICD, waarbij we meteen een aantal extra's demonstreren die mogelijk zijn met Gitlab.

Aanschouw volgende yml, die schatplichtig is aan het voorbeeld op de [pylint-gitlab](https://pypi.org/project/pylint-gitlab/) website.

Uiteraard zal je wel de tags nog moeten aanpassen om de juiste (jouw!) runner te selecteren.

Met die functionaliteit heb je de mogelijkheid om badges toe te voegen op je website, ... die de status van je CI/CD pipeline in een oogopslag weergeven. 
Daarvoor moet je weten dat je rechtstreeks kan linken naar bestanden in de artefacts.  (in dit geval naar badges/pylint.svg bijvoorbeeld)

```yaml
pylint:
  stage: test
  image: python:3.9-slim
  tags: 
    - "tag_roelvs"
  before_script:
    - mkdir -p public/badges public/lint
    - echo undefined > public/badges/$CI_JOB_NAME.score
    - pip install pylint-gitlab
    - pip install -r requirements.txt
  script:
    - pylint --exit-zero --output-format=text $(find -type f -name "*.py" ! -path "**/.venv/**") | tee /tmp/pylint.txt
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > public/badges/$CI_JOB_NAME.score
    - pylint --exit-zero --output-format=pylint_gitlab.GitlabCodeClimateReporter $(find -type f -name "*.py" ! -path "**/.venv/**") > codeclimate.json
    - pylint --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter $(find -type f -name "*.py" ! -path "**/.venv/**") > public/lint/index.html
  after_script:
    - anybadge --overwrite --label $CI_JOB_NAME --value=$(cat public/badges/$CI_JOB_NAME.score) --file=public/badges/$CI_JOB_NAME.svg 4=red 6=orange 8=yellow 10=green
    - |
      echo "Your score is: $(cat public/badges/$CI_JOB_NAME.score)"
  artifacts:
    paths:
      - public
    reports:
      codequality: codeclimate.json
    when: always

```

`before_script`:
Dit zijn taken die uitgevoerd worden voor een job uitgevoerd wordt, maar NA het restoren van de artefacts

`after_script`: dit zijn taken die na een job uitgevoerd worden, ook als die faalt.

`artefacts`: dit fragment bepaalt welke onderdelen achteraf opvraagbaar zijn. Gitlab heeft ook enkele 'speciale' artefacts, die kunnen integreren met het project. Zo is er het codeclimate.json bestand, waar pylint z'n output gestructureerd kwijt kan. Na een build kan je dan zo bij 'Code Quality' zien welke problemen allemaal ontdekt zijn...

![resultaat](codequality.png)

## Oefening

We kunnen deze oefening nog verder uitbreiden door ook pytest terug toe te voegen.

* Maak hiervoor een nieuwe job aan, binnen de test-stage. Je kan hiervoor terugvallen op je ervaring die je al had met Jenkins (pytest) én de officiële documentatie van Gitlab die hiervoor een voorbeeld geeft. ([https://docs.gitlab.com/ee/ci/unit_test_reports.html](https://docs.gitlab.com/ee/ci/unit_test_reports.html))

Als alles goed loopt, zou je nu ook een testrapport moeten zien nadat je pipeline doorlopen werd.

![resultaat](pytest.png)
