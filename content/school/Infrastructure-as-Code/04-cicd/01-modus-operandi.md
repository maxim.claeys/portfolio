---
title: Modus Operandi CI/CD
draft: false
weight: 48
---


Deze workshop heeft als doel de basisfunctionaliteit van CI/CD aan te leren. Via voorbeelden en bijhorende uitleg kan je de werking van CI/CD en bijhorende pipelines in de vingers krijgen.

Naast deze workshop is er ook een hoorcollege voorzien om de werking, de belangrijkste eigenschappen en toepassingen van CI/CD toe te lichten.

## Voorbereiding

Voor dit onderdeel is geen specifieke voorbereiding nodig. De slides van de theorie zijn terug te vinden op TODO

## Leerdoelen

* Het concept CI/CD begrijpen

* Pipelines opzetten met Jenkins en Gitlab CI/CD

## Opgave

Deze opgave wordt individueel afgewerkt...

De opgave werd opgevat als verschillende, opeenvolgende workshops die moeten doorlopen worden.
Nadien volgt een `project` waarbij de student moet aantonen de aangeleerde kennis van CI/CD onder de knie te hebben.

## Evaluatie

De evaluatie van dit topic gebeurt via de projectopgave.
Bij de verdediging van deze opdracht zal de score bepaald worden door:

* De praktische implementatie (70%)
* De theoretische achtergrondkennis (10%)
* Uitvoering van extra functionaliteit (20%)

Zorg er dus zeker voor dat je op het moment van de verdediging de theorie nog grondig herhaalde.
