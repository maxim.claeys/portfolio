---
title: Uitbreiding CI/CD
draft: false
weight: 57
---


# Uitdaging

De opgave rond de Flask website is eigenlijk nog niet compleet. We deden een aantal continuous integration stappen, maar automatisch deployen is nog niet uitgewerkt.

Dat is dan meteen ook deel van deze uitdaging.

* Installeer docker op server2
* Pas de gitlab-ci.yaml zo aan dat:
  * bij een push naar branch deploy (en enkel deze branch) een Docker container gebouwd wordt met deze website.
  * via SSH op de tweede server de docker container gepulld wordt en opnieuw gestart. Je mag er daarbij van uit gaan dat er geen andere containers actief zijn op die server.

Als inspiratie geven we een eenvoudige Dockerfile mee die je kan gebruiken:

```Dockerfile
FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
```

{{<alert type="tip">}}
Bovenstaande config kan hoogstens gebruikt worden voor een test-omgeving. De ingebouwde flask-webserver wordt gebruikt, maar die is enkel bedoeld voor debug-doeleinden.
Wil je flask in productie gaan gebruiken, dan ga je best op zoek naar termen als WSGI, gunicorn, ...
{{</alert>}}
