---
title: Inleiding
Page_title: Projectopgave CI/CD
draft: false
weight: 58
---

## Voorbereiding

Deze opgave wordt individueel afgewerkt...

## Leerdoelen

* Begrip van het concept CI/CD
* Opzetten van CI/CD adhv Gitlab en Gitlab-runners
* Introductie in de 'devops'-filosofie
* Automatiseren van netwerkconfiguratie aan de hand van Cisco Modeling Labs

## Opgave

### Opzet

Bij deze opgave zullen jullie een continuous integration / continuous deployment pipeline opzetten.

Het scenario waarin we werken is dat van een bedrijf waar verschillende mensen verantwoordelijk zijn voor de uitrol en het onderhouden van het bedrijfskritische netwerk.

Omdat, bij het configureren van aanpassingen, een kleine fout grote gevolgen kan hebben, wordt geopteerd om een aantal  testen uit te voeren vooraleer de netwerkconfiguratie naar productie wordt overgezet.

Om onze netwerkomgeving aan testen te kunnen onderwerpen maken we gebruik van `CML` (Cisco Modeling Labs), een geavanceerde netwerksimulator waarin we ons netwerk (of onderdelen daarvan) kunnen nabouwen (voorbeeld hieronder).

![topologie](voorbeeld_topologie.jpg)

{{<alert type="info">}}
In dit labo zullen zowel de testomgeving als de productieomgeving van het netwerk in CML gesimuleerd worden aangezien we geen toegang hebben tot fysieke apparatuur. In de praktijk zal de productieomgeving dus fysieke machines zijn of virtual appliances.
{{</alert>}}

Het doel is om bij elke geplande wijziging in het netwerk eerst een virtuele netwerkomgeving op te zetten `(development)` waarbij het configureren gebeurt samen met enkele testen vooraleer de afgewerkte configuratie beschikbaar wordt gesteld aan de mensen die de effectieve deploys doen `(deployment)`.

{{<alert type="info">}}
Je ziet dat we hier zowel de **Continuous Integration** (CI), samen met het testen, als de **Continuous Deployment** (CD) voorzien. .
{{</alert>}}

**Samengevat** kan je stellen dat we in deze opgave een team zullen hebben dat in een `development branch` de nodige aanpassingen zal doorvoeren en grondig testen op een `development-omgeving`.

Een ander team kan dan instaan voor de effectieve deployment naar de `productie-omgeving` door het configuratie-bestand (dat gecontroleerd werd in de development-omgeving) toe te passen in de pipeline van de `master branch`.  
In beide gevallen start bij het `pushen` naar hun branch (main of development) automatisch een pipeline die de respectievelijke opdrachten (jobs) voor beide teams uitvoert.

Het is **jullie taak** in deze labo-opdracht om de pipelines voor deze beide teams op te bouwen!

De **topologie** die wij gebruiken in deze opgave  is vrij eenvoudig:
Voor ons zijn eigenlijk enkel de internal en border router belangrijk.  
De unmanaged switch en ext-conn zorgen voor connectiviteit met het publieke netwerk en spelen voor ons verder geen rol.

![topologie](topologie_opgave.jpg)

### Over CML (Cisco Modeling Labs)

CML is een test- en simulatie omgeving van Cisco waarin echte images van routers, switches, firewalls, Linux-vm's,... kunnen gebruikt worden.
Met deze simulaties kunnen reële scenarios nagebouwd worden van bv. je productie-omgeving.
CML is dan ook de ideale omgeving om gebruikt te worden in een CI/CD proces waarbij bv. de test-fase kan uitgevoerd worden op dergelijk gesimuleerde routers en switches.

Je kan CML via een grafische webinterface benaderen om daar via de GUI een topologie uit te tekenen en toestellen virtueel te bekabelen.
Uiteraard is dit in een automatisatie-flow niet handig en dus kunnen we die topologie ook definiëren in een `YAML-gebaseerd` tekstbestand, bijvoorbeeld met de naam topology.yaml (op en top Infrastructure-as-Code 😉 )
In dit bestand kan dan ook de "startup-config" opgenomen worden zoals je die kent van het Cisco-curriculum.

CML op een geautomatiseerde manier benaderen kan op verschillende manieren via bv. API's (bv. python script,...), maar wij gebruiken daarvoor een python gebaseerde wrapper met de naam `cmlutils` ([meer info](https://developer.cisco.com/codeexchange/github/repo/CiscoDevNet/virlutils/)).
cmlutils kan een "simulatie" op de CML server starten of stoppen. Dit kan eenvoudigweg met **cml up** of **cml down**.

{{<alert type="info">}}
In de documentatie zal je nog heel dikwijls "virl" zien verschijnen. Dit was de naam van de vorige versie van CML maar onderliggend wordt deze terminologie nog dikwijls gebruikt.
{{</alert>}}

Onze "CML"-server is te vinden op: [zie overzicht per student](https://tinyurl.com/2qvda7ng).
Zowel de webinterface als de cmlutils gebruiken dezelfde credentials.

* username: admin
* password: Azerty123@

{{<alert type="caution">}}
Je hoeft eigenlijk enkel aan te melden op het webplatform om even een kijkje te nemen of je deployment goed gelukt is of om eventueel je deployment te verwijderen (al kan dit in principe allemaal vanuit cmlutils).
{{</alert>}}
