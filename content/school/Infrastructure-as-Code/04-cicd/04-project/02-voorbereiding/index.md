---
title: Voorbereiding
draft: false
weight: 59
---

## Development flow

We streven ernaar om een git flow op te zetten die, in een eerste stap, bij elke push naar de **development** branch een pipeline opstart. Die pipeline zal de configuratie van het netwerk (zie verder) doorvoeren en een aantal testen uitvoeren. Enkel bij het slagen van de tests wordt de nieuwe configuratiefile (running-config.cfg) voor de router vrijgegeven in een **downloadbaar artefact**.
Zo weten de netwerkingenieurs op elk moment waar ze *de laatste geteste versie* van de router-config kunnen vinden. De specfieke configuratie van de router in deze development fase zal gebeuren via Ansible.

In een volgende stap kan (bv. een ander team netwerkingenieurs) de development branch gemerged worden naar de "main branch" (production). Dit zal op zijn beurt een pipeline starten die voortbouwt op de pipeline van development en de geteste configuratie toepast op de productie-infrastructuur, ook **via Ansible**..

Developers (ingenieurs) die hun feature-branch (development) aanpassen maar niet slagen in de testen moeten terug naar de tekentafel en de config aanpassen waar nodig. Zo vermijden we dat niet werkende configs hun weg vinden naar de productie-omgeving.

## Uitvoering

Gelukkig moet je niet van nul starten. Het netwerkdepartement gaf al een deel van de manuele procedure voor deze pipelines vrij, en bezorgde ons de documentatie daarvoor. Je vindt die informatie (een basis-structuur en enkele bestanden) ook in deze repository.

Aan jullie vragen we om dit verder uit te bouwen en te gaan automatiseren.  
Gebruik daarvoor de ingebouwde CI-functionaliteit in Gitlab.

## Git Repo

Zorg eerst voor een eigen versie van de repo [2122_iac_cicd_labo_project](https://gitlab.com/ikdoeict/docenten/2122_infrastructure_as_code/labs/2122-iac-cicd-labo-project) door deze te `"forken"`.

* Geef je repo de naam "**2122-IaC-CICD_Project-naam.voornaam**"

## Voorbereiding Repo

Vooraleer we echt van start kunnen gaan moeten er eerst enkele bestandjes aangepast worden om jullie uitwerking `uniek` te houden ten op zichte van andere studenten zodat er geen conflicten ontstaan tijdens het uitvoeren van je pipelines.

* Pas in je repo volgende bestanden aan:
  * cml/development/**dev.yaml**  
    ![topologie](aanpassing1.jpg)  
    ![topologie](aanpassing2.jpg)  
    Als ip-adres gebruik je `het eerste` uit de toegewezen range in de **kolom D** van de [link https://tinyurl.com/2qvda7ng](https://tinyurl.com/2qvda7ng).

  * cml/production/**prod.yaml**  
    ![topologie](aanpassing3.jpg)  
    ![topologie](aanpassing4.jpg)
    Als ip-adres gebruik je `het tweede` uit de toegewezen range in de **kolom D** van de [link https://tinyurl.com/2qvda7ng](https://tinyurl.com/2qvda7ng).

## Deploy een gitlab runner

De meeste CD/CD systemen moeten, om hun opdrachten uit te voeren, gebruik maken van een runner (typisch op een build-machine). Gitlab ondersteunt een runner die op verschillende manieren kan gebruikt worden (executors => [link](https://docs.gitlab.com/runner/executors/)).

Voor deze labo-opgave moet je zelf nog een gitlab-runner installeren. Dit doe je op een virtuele machine op onze private cloud (zie verder).

Een runner biedt de mogelijkheid om later de testen uit te voeren. Die gebeuren immers niet op de git-server zelf (gitlab contacteert de runner om de opdrachten uit te voeren).
We kiezen er voor om als executor voor de `gitlab-runner` **docker** te gebruiken.

{{<alert type="tip">}}
Als je iets korter het pipeline-proces wil volgen, zou je ook de `shell` executor  kunnen gebruiken. Dit zorgt er voor dat je Linux-VM zelf dan gebruikt zal worden om de pipeline uit te voeren. De pipeline wordt dan rechtstreeks in de shell van je VM uitgevoerd. Niettegenstaande dit misschien makkelijker lijkt om te troubleshooten zal dit echter in de meeste situaties veel moeilijker zijn. Er zijn immers misschien al bepaalde pakketten op de machine aanwezig die kunnen conflicteren of wordt het moeilijker om veilig variabelen door te geven zonder conflict. En wat als de versies van de pakketten niet meer compatibel zijn met je pipeline vanwege een upgrade van het OS? Een docker-executor is op dat vlak een stuk handiger omdat je pipeline steeds in een geïsoleerde, "verse" container uitgevoerd zal worden.
{{</alert>}}

### Concreet

* deploy een losse VM (tpl_rocky_linux) op onze private cloud op [https://cloud.ikdoeict.be](https://cloud.ikdoeict.be).
* Geef de deployment als naam `IaC_LabCI-Runner-voornaam_familienaam`.
* Installeer de gitlab-runner ([zie link](https://docs.gitlab.com/runner/install/)) voor je Linux VM.

{{<alert type="caution">}}
Let goed op want je kan de gitlab-runner als container installeren ([Install as a Docker service](https://docs.gitlab.com/runner/install/docker.html)) of gewoon lokaal op je VM ([Install on GNU/Linux](https://docs.gitlab.com/runner/install/linux-manually.html)). Wat ons betreft mag je kiezen, maar wees goed bewust van je keuze. Het meest evidente is de runner lokaal installeren op je Linux VM. Deze zal dan wel tijdens het uitvoeren van pipelines de nodige containers opstarten. (In de CD/CD workshop werkte je met een gitlab runner container)
{{</alert>}}

* Houd er rekening mee dat, als je de runner rechtstreeks op je VM installeert, gitlab-runner misschien verhoogde rechten zal nodig hebben om automatisch de docker-deamon te kunnen aanspreken (in geval van docker-ce).

* Installeer ook `docker-ce` (de executor van gitlab-runner kan geen gebruik maken van podman).
