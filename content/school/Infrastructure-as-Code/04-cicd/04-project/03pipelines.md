---
title: Pipelines
Page_title: Bouwen van pipelines
draft: false
weight: 60
---

## Bouwen van de pipelines

Zoals je al weet uit de workshop CICD zal je de pipelines moeten opbouwen a.d.h.v. een .gitlab-ci.yml bestand.
Deze configuratie zal alles bevatten om de pipelines voor zowel development als production (main) uit te voeren.
Afhankelijk van welke branch aangesproken zal worden zullen andere delen in het .gitlab-ci.yml bestand aangesproken en uitgevoerd worden.

Je zal **twee** sporen moeten bewandelen:

* Enerzijds zal je een pipeline in .gitlab-ci.yml bouwen voor de **development branch** die de configuratie van het netwerk en het testen ervan op zich neemt.
* Anderzijds zal je in .gitlab-ci.yml een pipeline moeten bouwen (die voortbouwt op de development pipeline) in de **main branch** die het config bestand (afkomstig van de development pipeline) importeert in de productie-opgeving.

{{<alert type="caution">}}
In de production fase zullen ook de stappen van de development fase doorlopen worden om het correcte configuratiebestand te kunnen aanbieden (zie verder). Anders gezegd zal de development branch zich beperken tot het configureren en het testen van het netwerk en zal de production branch (main) alle stappen uitvoeren: configureren, testen en deployen van het netwerk.
{{</alert>}}

**Hoe** we dergelijke **pipelines bouwen** in een Gitlab omgeving kan je vinden in de [documentatie](https://docs.gitlab.com/ee/ci/README.html).
Je kan er heel complexe scenario's mee uitwerken maar wij beperken ons tot een eenvoudige, maar toch interessante uitwerking.

We geven je alvast stukken code mee om aan de slag te gaan. De commando's zijn op voorhand getest, maar je zal ze wel zelf nog moeten groeperen en eventueel aanpassen om tot verschillende `stages` & `jobs` te komen. Uiteraard zal je hier en daar nog aanvullingen moeten doen met eerder klassieke, gekende commando's (linux-commands, Ansible-commands,...)

{{<alert type="caution">}}
Onderstaande commando's werden getest in een Rocky8.5 container. Daarvan afwijken en een andere container kiezen mag, maar weet dat sommige commando's dan mogelijks licht moeten gewijzigd worden..
{{</alert>}}

```bash title="Voorbeeldcommando's om te integreren"
yum install python3 -y                                  # python installeren
yum install python3-pip                                 # python-pip installeren
pip3 install virtualenv                                 # python virtual environment voorzien
python3 -m venv venv                                    # python virtual environment aanmaken met de naam venv
source venv/bin/activate                                # virtual environment activeren
python3 -m pip install --upgrade pip                    # pip moet een upgrade krijgen om de juiste modules te kunnen installeren
pip3 install -r requirements.txt                        # pip de nodige modules in de virtual environment laten installeren 
pip3 install ansible-lint==5.4.0                              # module nodig om ansible playbooks te testen
ansible-lint -v ansible/playbook*.yml                   # commando om de ansible-playbooks te testen
cml ls                                                  # cmlutils commando dat de huidige labs op de CML-server toont
cml up -f cml/development/dev.yaml --provision          # cmlutils command dat een lab importeert naar de CML-server
cml ls  
ansible-playbook playbook-dev.yml                       # ansible playbook dat uitgevoerd wordt bij development pipeline
cml rm -f --no-confirm                                  # cmlutils command dat een lab weer verwijderd van de CML-server (belangrijk!)
cml save --extract -f ../lab-topology-production.yml    # cmlutils command dat de volledige labo-setup (inclusief configs) opslaat naar een bestand 
```

## Algemeen ivm de pipelines: **Stages**

Zoals eerder aangegeven zullen er `2 pipelines` voorzien worden.  
Deze zullen samen verantwoordelijk zijn voor `3 stages` in het CI/CD-proces.

De stages zijn:

* **prepare**   (onderdeel van de `development pipeline` en zorgt voor `linting`)
* **config**    (onderdeel van de `development pipeline` en zal de effectieve `configuratie en testing` op zich nemen)
* **deploy**    (onderdeel van de `production pipeline`, op main branch en zal `de gesteste config importeren` )

## Voorafgaande stage `before_script`

Er zal een deel van de commando's gemeenschappelijk zijn voor alle stages.  
Neem dus in je `.gitlab-ci.yml` bestand een sectie op die uitgevoerd zal worden vooraleer de echte jobs van de respectievelijke pipelines uitgevoerd worden (zie documentatie voor het gebruik van keyword `before_script`).  
Je kan natuurlijk een runner (container) kiezen waar standaard al zo veel mogelijk tools, die je zou kunnen nodig hebben, geïnstalleerd staan. Maar dikwijls zal dit ontoereikend zijn.

Neem dus in je `.gitlab-ci.yml`-bestand een sectie op die uitgevoerd zal worden vooraleer de echte jobs van de respectievelijke pipelines uitgevoerd worden (zie documentatie voor het gebruik van keyword `before_script`).

Concreet zorg je in deze fase voor:

* het installeren van **python** en **python-pip**
* het voorzien en activeren van een **python virtual environment**
* een *upgrade* voorzien van de **pip** module
* alle nodige *python modules* (uit requirements.txt) installeren.

## **Development** pipeline

Deze pipeline wordt uitgevoerd bij een wijziging (push/commit) aan de development branch en bevat dus 2 stages (**prepare** en **config**). Typisch zal dit gestart worden indien er een nieuwe configuratie (via een aangepast Ansible-playbook) voorzien wordt.

* Bouw de pipeline uit zodat in de stage **prepare** getest wordt op correct gebruik van de syntax bij de Ansible-playbooks.
  * De correcte syntax controleren bij deze playbooks gebeurt met `ansible-linting`.

* Bouw de pipeline verder uit zodat in de stage **config** het volgende uitgevoerd zal worden:
  * de virtuele development netwerkomgeving (`dey.yaml`) opstarten op de CML-server (cml ls, cml up,...)
  * een Ansible-playbook uitvoeren die interfaces configureert en connectiviteit controleert (zie hieronder)
  * de virtuele development netwerkomgeving in CML afsluit en verwijdert. (cml rm)
  * de running-config van de router **als artefact** (bestand running-config.cfg) in gitlab aanbiedt.

### **Ansible-taken**

Ansible-taken die moeten uitgevoerd worden via een playbook `playbook-dev.yml` in de **config** stage (zie hierboven)

* een ip-adres configureren (192.168.1.1/24) op de interface G0/1 van de border router die verbonden is met de andere router (internal)
* 2 loopback-interfaces toevoegen
* de loopback-interfaces voorzien van een ip-adres (resp. 172.16.x.1/24 en 172.17.x.1/24) +
* "x" is hierbij de **id**, terug te vinden in kolom E van de [tabel](https://tinyurl.com/2qvda7ng).
* de connectiviteit testen tussen beide routers (ping naar de overkant op 192.168.1.2)
* indien de vorige taak (het pingen) succesvol was:
  * een boodschap bij de Ansible-output op het scherm zet met "succesvolle ping naar internal router"
  * een backup nemen van de running-config naar een bestand "running-config.cfg" (locatie zie mappenstructuur)

{{<alert type="info">}}
Bij de uitvoering van deze pipeline zal voorafgaand de code (het script) uitgevoerd worden van de `before_script"-stage (zie hoger). Je mag/moet daar dus rekening mee houden bij het opstellen van deze pipeline.
{{</alert>}}

## **Production** pipeline

Deze pipeline zal dus uitgevoerd worden bij een merge/wijziging aan de "main branch" en bevat vooral de stage **deploy**.
In deze fase zal de deploy stage het configbestand, dat in de config stage getest en gekopieerd (backup) werd naar de mappenstructuur, geïmporteerd worden in de productie-omgeving.

{{<alert type="danger">}} Opgelet!!
Niettegenstaande in de main branch (production) de focus ligt op deployment zal voorafgaand ook hier de config-fase doorlopen worden (**enkel config en niet de prepare fase!**).
{{</alert>}}

* Bouw deze pipeline uit zodat in de stage **deploy** het volgende uitgevoerd zal worden:
  * de virtuele production netwerkomgeving (`prod.yaml`) opstarten op de CML-server (cml ls, cml up,...)
  * een linux-command dat in de running-config het ip-adres uit je development-omgeving vervangt door het ip-adres van je productie-omgeving. **(*)**
  * een Ansible-playbook uitvoeren die de `running-config.cfg` integraal importeert in de border-router. (zie hieronder)
  * het volledig virtuele productie-netwerk in CML opslaat (cml save) in een bestand `lab-topology-production.yml`.
  * de virtuele production netwerkomgeving afsluit en verwijdert. (cml rm)
  * de bewaarde virtuele lab-omgeving als artefact (bestand `lab-topology-production.yml`) in gitlab aanbiedt.

{{<alert type="note">}}
**(*)** Aangezien de running-config al gehaald werd (gekopieerd werd) uit de development omgeving (config stage) bevat deze het ip-adres van het development-netwerk. Om die reden moet dit dus in de productie-omgeving **eerst vervangen** worden **door het correcte ip-adres**.
{{</alert>}}

### **Ansible-taken**

Ansible-taken die moeten uitgevoerd worden via een playbook `playbook-prod.yml` in de **deploy** stage (zie hierboven):

* de running-config.cfg integraal importeren in de border-router
* de running-config van de router opslaan als startup-config (cfr "copy run start").

## Overzicht

Hieronder vind je schematisch hoe beide pipelines verlopen.  
Je zal je pipeline zo moeten configureren dat bepaalde stages zowel bij development als bij production zullen aangesproken worden (.pre, config) en andere enkel bij hun eigen branch (prepare, deploy). Je ziet ook dat elke pipeline een ander artefact oplevert...

```mermaid
flowchart LR
    .gitlab-ci.yml --> |development| prepare --> dev_config[config] --> |artefact| id1([running-config.cfg])
    .gitlab-ci.yml --> |production| prod_config[config] --> deploy --> |artefact| id2([lab-topology-production.yml])
```

## Nog enkele tips

* Wachtwoorden en andere gevoelige data kan je niet zomaar opnemen in je pipelines/playbooks.  
Dit kan je oplossen door **"Environment variables"** mee te geven aan Gitlab. Bij het uitvoeren van een pipeline zal gitlab zorgen dat deze variabelen doorgegeven worden aan de container tijdens het builden. Maak hiervan gebruik!
* Telkens lokaal aanpassingen aan je repo doorvoeren en pushen om te zien of je pipeline werkt zorgt voor veel vertraging en zorgt vooral voor een lange waslijst van gefaalde pipelines in gitlab. Dit kan je vermijden door je pipeline lokaal uit te voeren (vb rechtstreeks op je runner). +
Dit kan je doen op je build-vm met het commando `"sudo gitlab-runner exec docker [naam van de job]"`.
