---
title : "Uitbreiding"
draft: false
weight: 33
---

Deze repo bevat de opgave voor een facultatieve uitbreidingsoefening voor Ansible.

## Doelstelling

In deze uitbreidingsopgave:

* Toon je aan dat je Ansible goed onder de knie hebt en iets complexere opgaves kan uitwerken.

## Uitbreiding - Opgave

### **Deploy omgeving**

Deploy in de private cloud een nieuwe instantie van de template met de naam `OPO_Infrastructure_as_Code_Workshop` en geef die als naam `IaC-Ansible-Uitbreiding-Familienaam Voornaam`.

### **Taken**

* Los volgende opdrachten op met een Ansible playbook:
  * Via Ansible (dus niet manueel) wordt er op de Ansible Control host een ssh-keypair aangemaakt
  * De hierboven gegenereerde public key wordt aan de verschillende nodes toegevoegd aan de user `student`(rocky) of `ubuntu`(ubuntu). Connecties met de nodes kunnen momenteel nog niet via ssh-keys en moeten voorlopig password-based.
  De router hoef je niet op te nemen in deze opdracht.

* Met een tweede playbook realiseer je onderstaande opdrachten (connecties gebeuren nu via de ssh-keys van hierboven)
  * Zorg er voor dat de public keys van je docenten ook toegevoegd worden aan de user student(centos) of ubuntu(ubuntu). De keys van je docenten kan je vinden vian <https://gitlab.com/username.keys> (usernames: svenknockaert, roelvs, johan.donne)

### **Oplevering**

Als resultaat van deze opgave zorg je er voor dat je oplossing, met alle nodige files (indien van toepassing) aanwezig zijn in een repo op Git met de naam `2122-IaC_Ansible_uitbreiding_voornaam.familienaam`.
