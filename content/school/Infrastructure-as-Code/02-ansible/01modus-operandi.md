---
title : Modus Operandi
page_title: Inleiding
draft: false
weight: 23
---

Deze workshop heeft als doel studenten die het vak Infrastructure As Code volgen in de opleiding Elektronica-ICT aan de Odisee hogeschool, via een combinatie van vooral zelfstudie aangevuld met begeleide sessies, Ansible aan te leren.
Deze workshop zal je vooral stapsgewijs de belangrijkste principes van Ansible leren. Via voorbeelden en bijhorende uitleg kan je de werking van Ansible in de vingers krijgen.

Naast deze workshop is er ook een hoorcollege voorzien om de werking, de belangrijkste eigenschappen en toepassingen van Ansible toe te lichten.

## Voorbereiding


* Basiskennis Linux
* Git
* SSH public/private keys

## Leerdoelen


Het doel van deze gestuurde workshop is de student voorbereiden om uiteindelijk op zelfstandige wijze een complexer scenario met Ansible op te lossen. De student moet op deze manier kunnen aantonen dat hij, na het volgen van deze workshop, Ansible ook in nieuwe en complexere situaties kan toepassen.

## Opgave


Deze opgave wordt individueel afgewerkt...

De opgave werd opgevat als verschillende, opeenvolgende workshops die moeten doorlopen worden.
Nadien volgt een `project` waarbij de student moet aantonen de aangeleerde kennis van Ansible onder de knie te hebben.

## Evaluatie


De evaluatie van dit topic gebeurt via de projectopgave.
Bij de verdediging van deze opdracht zal de score bepaald worden door:

* De praktische implementatie (70%)
* De theoretische achtergrondkennis (10%)
* Uitvoering van extra functionaliteit (20%)

Zorg er dus zeker voor dat je op het moment van de verdediging de theorie nog grondig herhaalde.
