---
title: Project
page_title: "Project 1: Ansible"
draft: false
weight: 34
---

Deze repo bevat de opgave voor een project volgend op de workshop van Ansible.

## Doelstelling

In dit project:

* toon je aan dat je verschillende systeembeheertaken kan uitvoeren met Ansible
* Pas je zo veel mogelijk de `best practices` toe om Ansible-taken uit te voeren.

## Project - Opgave

### Stap 1 - Deploy instantie / Voorbereidingen

Voor dit project werk je met een aparte deployment die voorzien werd op de private cloud van onze opleiding.

* Je progressie en uiteindelijke oplossing komt terecht in een repo met de naam `2122-IaC_Ansible_Project-Familienaam Voornaam`

* Deploy een nieuwe instantie "OPO_Infra_As_Code_Ansible_Project" op de private cloud en geef deze de naam `IaC-Ansible-Project-Familienaam Voornaam`.

* Schema:
  ![schema](schema.jpg)

* Noteer je ip-adressen:
De ip-adressen kan je best hieronder noteren.

| Virtual Machine      | OS             | IP-adres | credentials      |
|----------------------|----------------|----------|------------------|
| Ansible Control Host | Rocky       |          | student/student  |
| Managed Host 1       | Ubuntu   |          | ubuntu/Azerty123 |
| Managed Host 2       | Ubuntu   |          | ubuntu/Azerty123 |
| Managed Host 3       | Rocky       |          | student/student  |
| Router               | Cisco CRS1000v |          | cisco/cisco123!  |

* Neem na opstart een snapshot van elke VM.
Op die manier kan je in een latere fase makkelijk teruggrijpen naar een `propere` versie van je VM.

{{<alert type="tip">}}
Neem eventueel tussendoor ook nog eens een snapshot vooraleer ingrijpende aanpassingen door te voeren. Of... door een snapshot terug te zetten kan je ook je playbook meerdere keren toepassen en testen alsof het de eerste keer is.
{{</alert>}}

* Zorg er voor dat je op een makkelijke manier de Control Host kan gebruiken. Voorzie je connectie met de Control Host via MobaXterm of nog beter, via een `Remote-SSH`-connectie bij Visual Studio Code of gelijkaardig.

### Stap 2 - Ansible voorbereiding

* Installeer Ansible op je Control Host
* Voorzie `Password-less SSH-Authentication` met alle nodes (host1, host2 en host3) behalve de router (wél met wachtwoord) en de Ansible Control Host (lokale authenticatie).

* Alle Playbooks, Roles, inventories, variabelen, etc... komen terecht in een subfolder van je home-folder "student". Deze subfolder heet `ansible-project`.

### Stap 3 - Inventory

* Voorzie een inventory met volgende groepen:

``` txt
@all:
  |--@controlhosts:
  |  |--controlnode1    => Ansible Control Host
  |--@dbservers:
  |  |--host3           => Load Balancer en Database server
  |--@loadbalancers:
  |  |--host3           => Load Balancer en Database server
  |--@routers:
  |  |--CSR1000v        => router
  |--@ungrouped:
  |--@webservers:
  |  |--webnode1        => host1: webserver
  |  |--webnode2        => host2: webserver
```

* Uiteraard worden variabelen (cfr. Best Practices) niet in de inventory opgenomen maar in aparte en beveiligde bestanden.

{{<alert type="note">}}
Idealiter splits je de "gevoelige" data af van de gewone variabelen en worden deze apart geëncrypteerd opgeslagen.
{{</alert>}}

{{<alert type="tip">}}
Eventueel kan je nog wachten om je bestanden met gevoelige data te beschermen om op dit moment nog wat makkelijker te kunnen werken. Maar op het einde van je opgave moet dit uiteraard wel in orde gebracht zijn.
{{</alert>}}

* Voorzie eventueel nu reeds je basis-structuur in de folder van je project.

### Stap 4 - Uitvoering

In onderstaand blok worden verschillende verwachtingen geformuleerd. 
Het is aan jou om deze zelf op een zo goed mogelijke manier te implementeren rekening houdend met best practices.

We zullen het geheel zo veel mogelijk in `rollen` proberen uit te voeren zodat je heel gestructureerd je Ansible-taken kan uitvoeren.

Probeer ook waar mogelijk zo efficiënt mogelijk te werken (loops, conditionals,...)

#### Taak 0 - Playbook

In de volgende taken wordt gevraagd om alles netjes in `rollen` uit te werken.
Uiteraard moeten deze rollen aangesproken worden door deze op te nemen in een playbook.

* Zorg er, bij het uitvoeren van onderstaande taken voor, dat je een 'playbook' met de naam `playbook_voornaam.yml` maakt met daarin verschillende `plays` om de rollen toe te passen op de verschillende servers, routers,...

{{<alert type="note">}}
Op die manier kan je telkens ook stap per stap controleren of je playbook en je rollen hun taken correct uitvoeren.
{{</alert>}}

#### Taak 1 - Common

* Creëer een nieuwe `rol` met de naam `common` die je nodes een software update/upgrade geeft en dus alle pakketten van je OS (zowel Ubuntu als Centos) op de recentste versie brengt (eventueel ook de package repository updaten!).

{{<alert type="tip">}}
De allereerste keer kan het heel lang duren vooraleer je playbook volledig uitgevoerd wordt als er heel veel updates moeten gebeuren. Heb even geduld of voer update eerst eens manueel uit.
{{</alert>}}

* Voorzie in dezelfde rol dat de `firewall actief` is én een `SSH-connecties toelaat`.

{{<alert type="tip">}}
Herstart indien nodig de firewall om de regels in werking te stellen. Maak gebruik van `Handlers`.
{{</alert>}}

#### Taak 2 - Webservers  

* Maak van de webserver-nodes effectief webservers. We gebruiken `nginx` als webserver-service. Voorzie hiervoor ook een nieuwe `rol` `webserver`.

* Breid je rol verder uit zodat ook php-pagina's uitgevoerd kunnen worden.
  * Installeer hiervoor de package `php-fpm`.
  * Aangezien we misschien ook gaan communiceren met een Mysql-database voeg je ook de package `php-mysql` toe

* Bij opstarten van de nodes moet de webservice automatisch opgestart worden.

* Voorzie in diezelfde rol dat de correcte poorten in de firewall openstaan om connecties met de webservers via `http` én `https` toe te laten.

{{<alert type="danger" text="warning">}}
Tussentijds kan je best controleren of je kan surfen naar de default-pagina van nginx vooraleer verder te werken en andere rollen uit te werken.
{{</alert>}}

#### Taak 3 - Database server

`node3` zal zowel als Load Balancer fungeren én als Database server.

* Voorzie een nieuwe Ansible-`rol` met de naam `database` waarmee `mysql-server` geïnstalleerd wordt op node3. 

{{<alert type="tip">}}

* Zorg dat de service gestart is, ook na een reboot.
* Wellicht zal je een extra python-gerelateerd packet moeten installeren (bekijk goed je foutmeldingen)
{{</alert>}}

* Voorzie in diezelfde rol dat de firewall connecties met deze databaseserver toelaat **als deze afkomstig zijn van de webservers**. 
Je hoeft dus geen rekening te houden met de poorten. Aan de firewallregels van je database-server voeg je dus de source ip-adressen (publieke netwerk) van je webservers toe aan de zone `Public` (ip's liefst op een dynamische manier en dus niet "hard coded").

* Voeg een nieuwe mysql-gebruiker toe waarmee we later in de mysql-databank kunnen aanmelden.
Gebruik als nieuwe user je eigen voornaam en kies een bijhorend wachtwoord dat je, cfr best practices, apart als variabele opslaat.

{{<alert type="note">}}
Idealiter zorg je ervoor dat wachtwoorden niet verschijnen bij het uitvoeren van je play in de output of opgenomen worden in logs.
{{</alert>}}

* Creëer in je mysql-server een nieuwe databank met de naam `voornaamfamilienaam` (aan te passen met je eigen naam uiteraard).

* Voer nu ook via deze rol een `query` uit op je mysql-server:
  * Je weet dat via een gewone command-line deze query kan uitgevoerd worden met
`mysql --user=username --password=xxxx --execute="select ....;"`.
  * de query vraagt een lijst van de users uit de tabel `user` van de database `mysql`. Neem de velden `user, host en password_last_changed` op in de query.
  * Registreer de output van deze query in een variabele.
  * Toon, bij het uitvoeren van je playbook, het resultaat van dit command (dat in een variabele zit) op het scherm.

#### Taak 4 - Website hosten

In een volgende stap voorzie je opnieuw een nieuwe rol waarbij van een `GitHub`-repo een website binnengehaald zal worden en via je nginx gehost wordt.

* Voorzie een nieuwe rol met de naam `website-github`.

* Met deze rol haal je de repo binnen op [GitHub](https://github.com/poojan1812/Ansible) en deze plaats je in een map `git-repo` op de webservers in de homefolder van de gebruiker waarmee je Ansible-opdrachten uitvoert.

* Kopieer van de repo de inhoud naar de juiste plaats zodat nginx deze als default website kan weergeven bij het surfen naar je webservers.

{{<alert type="note">}}
in principe kan je deze taak en de vorige ook in 1 taak uitvoeren.
{{</alert>}}

* Aangezien `nginx` niet standaard geconfigureerd werd bij de installatie om `php` te gebruiken moeten we dit zelf nog aanpassen in de nginx-configuratie.
  * Je mag het default-configuratie bestand van nginx overschrijven/vervangen met onderstaande code:
    ```
    server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www/html;

        index index.php index.html index.htm index.nginx-debian.html;

        server_name _;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
        }

        # pass PHP scripts to FastCGI server
        #
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
        #
        #       # With php-fpm (or other unix sockets):
                fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        #       # With php-cgi (or other tcp sockets):
        #       fastcgi_pass 127.0.0.1:9000;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #       deny all;
        }
    }

    ```

{{<alert type="danger" text="warning">}}
Controleer ook hier eventueel of de website nu effectief bereikbaar is en php-pagina's kan verwerken. test bv. `Ansible.html` en `index.php`.
{{</alert>}}

#### Taak 5 - Loadbalancer

Aangezien we 2 webservers hebben die dezelfde content aanbieden kunnen we daarvoor een `loadbalancer` plaatsen die requests verdeelt over beide nodes.

* Voorzie een nieuwe rol `loadbalancer` die de package installeert om `HAproxy` te implementeren.

* De configuratie van de loadbalancer wordt bepaald door het bestand in `/etc/haproxy/haproxy.cfg`.
  * Maak van dit bestand een template in Ansible waarin je volgende parameters dynamisch maakt (variabelen) die @runtime door Ansible ingevuld worden:
    * de poort waarop de loadbalancer luistert
    * de webservernaam van de backendservers
    * het ip-adres van de backendservers
  * Plaats uiteraard de template en de variabelen op de juiste plaats in je Ansible-project.

{{<alert type="tip">}}
uitleg over de configuratiefile van HAProxy kan je makkelijk online vinden (vb [https://www.haproxy.com/blog/the-four-essential-sections-of-an-haproxy-configuration/](https://www.haproxy.com/blog/the-four-essential-sections-of-an-haproxy-configuration/) of ...)
{{</alert>}}

#### Taak 6 - Diverse

* Maak een nieuwe rol aan met de naam `varia`.

* Zorg in deze rol dat de `hosts`-file van zowel je webservers (webnode1 en webnode2), je databaseserver (host3) én de Control Host (controlnode1) aangevuld wordt met de nodige regels om de hostnames van de webservers, de loadbalancer en de router te kunnen resolven naar hun ip-adres. Een ping naar "host3" moet dan prima werken vanop elke Linux-VM van je opstelling

* Zorg er bijkomend voor dat de hostname de naam van de node in de inventory krijgt. Dus i.p.v. de hostnaam die bepaald werd door de virtualisatieomgeving krijgen je machines de naam uit de inventory.

{{<alert type="tip">}}
Een gewijzigde hostname is niet altijd meteen zichtbaar op de bash prompt.
Controleren kan met het commando hostname
{{</alert>}}

#### Taak 7 - Router

Met Ansible kan je heel veel verschillende devices beheren.
In deze taak maken we met Ansbible connectie met een Cisco IOS Router (CSR1000v).

* Maak een nieuwe rol aan met de naam `routerconfig`.
  * Pas de "hostname" aan naar `routervoornaam`, vb. routersven.
  * Neem een backup van de running-config en sla deze op in een submap `backup` in je Ansible-projectfolder.
    (Zorg ervoor dat deze map "backup" achteraf ook aanwezig is in je repo op Gitlab samen met je volledige Ansible project.
