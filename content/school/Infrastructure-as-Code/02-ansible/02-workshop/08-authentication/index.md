---
title: SSH authentication
page_title: "WORKSHOP 8: Passwordless Authentication"
draft: false
weight: 31
---



Standaard gaat Ansible connecteren met de remote hosts met de actieve gebruiker van de Ansible Control Host. Als deze niet dezelfde is moet er een andere gebruikersnaam en wachtwoord opgegeven worden. Tot op dit moment authenticeerden we op de remote hosts via SSH met een `username` en `password`. Dit is echter niet de ideale manier.

## Doelstelling

In deze workshop leer je:

- SSH public/private keypair aanmaken.
- de SSH public key distribueren naar de remote hosts
- Ansible configureren om te authenticeren met je SSH public/private key-pair

## Workshop

Zoals in de inleiding aangegeven is authenticeren met `wachtwoorden` niet de meest ideale manier.
Wanneer je gebruik kan maken van een `key-pair` dan heeft dat volgende voordelen:

- Je account is niet meer vatbaar voor `Brute Force Attacks`.
- Het wachtwoord hoeft op geen enkele manier over het netwerk verzonden te worden.
- Authenticeren gaat makkelijker in automatiserings-processen zonder tussenkomst van de auteur

Bij authenticatie met een key-pair is het belangrijk dat de `private key` veilig op de Ansible Control Host staat. De `public key` moet dan echter wel op elke host die je wil managen met Ansible gekopieerd worden.

![ssh keys](SSH_keys.jpg)

### Stap 1 - Public/Private key pair aanmaken

- Maak een nieuw `key-pair` aan:

    ```bash
    [student@ControlHost ansible-files]$ ssh-keygen
    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/student/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /home/student/.ssh/id_rsa.
    Your public key has been saved in /home/student/.ssh/id_rsa.pub.
    The key fingerprint is:
    SHA256:wQVMRBrxuLgUOyrn/P6yLRLl5G/54rkij5yiqsxjEj4 student@ControlHost
    The keys randomart image is:
    +---[RSA 3072]----+
    |      o*=..      |
    |       *..       |
    |    . o +        |
    |    o+ . .       |
    |   == . S        |
    |. .ooo           |
    |o.o... .         |
    |=Eoo+.=.         |
    |X+O*=O*+.        |
    +----[SHA256]-----+
    [student@ControlHost ansible-files]$
    ```

{{<alert type="note">}}
> De default locatie waar het key-pair opgeslagen zal worden is de map `.ssh` van je home-folder.
{{</alert>}}

{{<alert type="tip">}}
Je kan in dit geval best geen wachtwoord op je private key plaatsen anders verlies je het voordeel van password-less authentication. Je zou dan immers alsnog telkens een wachtwoord moeten ingeven bij het gebruik van je private key (deze wordt wel niet over het netwerk gestuurd). Wil je dit toch veilig met een extra wachtwoord, dan kan je ook met een `ssh-agent` werken.
{{</alert>}}

- Controleer of het key-pair aangemaakt werd in je `.ssh`-folder.

    ```bash
    [student@ControlHost ansible-files]$ ls -al ~/.ssh
    ```

    Je zou er zowel de private key (`id_rsa`) en public key (`id_rsa.pub`) moeten zien staan.

### Stap 2 - Public key distribueren

Opdat je via een SSH key-pair zou kunnen aanmelden moet de public aanwezig zijn op de remote hosts en dat bij de correcte `user` waar je mee wil aanmelden.
Dit betekent dat de public key aanwezig moet zijn in het bestand `authorized_keys` in de `.ssh` map van de remote user.
Je zou kunnen manueel je public key kopiëren in het bestand `authorized_keys` maar het kan makkelijker met de ssh-tool `ssh-copy-id`.

- Kopieer de public key naar de remote host:

    ```bash
    [student@ControlHost ansible-files]$ ssh-copy-id -i ~/.ssh/id_rsa.pub ubuntu@X.X.X.X

    /usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/student/.ssh/id_rsa.pub"
    /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
    /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- when you are prompted now it is to install the new keys
    ubuntu@10.129.36.30 password:

    Number of key(s) added: 1

    Now try logging into the machine, with:   "ssh ubuntu@X.X.X.X"
    and check to make sure that only the key(s) you wanted were added.

    [student@ControlHost ansible-files]$
    ```

    Met de parameter `-i` refereer je naar de correcte public_key. De node én gebruiker waar je de public key wenst naartoe te kopiëren geef je aan met `user@ip_remote_host`.

- Doe dit nu ook nog voor de 2 andere nodes.

### Stap 3 - Controle

- Controleer nu of je vanop je Ansible Control Host een ssh-connectie kan maken naar de remote host. Dit zou zonder wachtwoord moeten lukken:

    ```bash
    [student@ControlHost ansible-files]$ ssh ubuntu@X.X.X.X
    ```

- Verifieer ook eens de inhoud van `authorized_keys` op de remote host. Daar zou dezelfde inhoud moeten staan als je `id_rsa.pub` op je eigen Ansible Control Host.

### Stap 4 - Ansible aanpassen voor passwordless authentication

Tot op heden werd de authenticatie voor Ansible gedaan via de SSH gebruikersnaam en wachtwoord dat opgegeven werd in de `inventory`. Dit willen we nu aanpassen zodat er gebruik gemaakt wordt van het public/private key-pair.

Dit kan u eenvoudig door de variabele `ansible_password` te verwijderen. Ansible zal bij het authenticeren automatisch proberen aan te melden met een public/private key-pair. Uiteraard moet de gebruiker wel nog steeds bepaald worden via `ansible_user`.

- Verwijder (of zet in commentaar) de variabele `ansible_password` in de sectie `vars` van de host-groep `web`:

  ```yml
  vars:
    ansible_port: 22
    ansible_user: ubuntu
    #ansible_password: Azerty123
    ansible_connection: ssh
    ansible_become_pass: Azerty123
  ```

- Probeer je playbook van de vorige workshop opnieuw uit en kijk of deze nog steeds kan uitgevoerd worden:

    ```bash
    [student@ControlHost ansible-files]$ ansible-playbook test_apache_role.yml
    ```

© Deze workshop werd gebaseerd op de informatie van [Red Hat Ansible Automation Platform](https://ansible.github.io/workshops/)
