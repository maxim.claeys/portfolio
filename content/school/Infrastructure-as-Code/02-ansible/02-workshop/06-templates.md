---
title : Templates - jinja2
page_title: "WORKSHOP 6: Templates"
draft: false
weight: 29
---

In deze oefening behandelen we **`Jinja2`** templates. Ansible gebruikt deze `Jinja2` templating om bestanden aan te passen "vooraleer" ze naar de beheerde hosts doorgestuurd worden.
`Jinja2` is een van de meest gebruikte `template engines` voor oa. Python (zie [Jinja2 templates](https://jinja.palletsprojects.com/en/3.1.x/templates/))

## Doelstelling

In deze workshop leer je:

- Jinja2 als templating instrument te gebruiken.
- templates gebruiken om aangepaste bestanden door te geven met Ansible.

## Workshop

### Stap 1 - Templates gebruiken in Playbooks

Wanneer een template voor een bepaald bestand werd aangemaakt, dan kan het resultaat uitgerold worden naar de beheerde hosts met de Ansible module `template`. Via deze module kunnen lokale bestanden dus aangepast én gekopieerd worden naar de beheerde hosts.

Als voorbleeld gaan we de inhoud van het `motd`-bestand aanpassen met telkens **"host-specifieke"** data.

- Creëer een nieuwe "map" met de naam `templates` in de projectmap:
  
  ```bash
  [student@ControlHost ansible-files]$ mkdir templates
  ```

- Maak in deze subfolder een template-bestand aan met de naam `motd-facts.j2`:

    ```txt
    Welcome to {{ ansible_hostname }}.
    Distributie: {{ ansible_distribution }} {{ ansible_distribution_version}}
    Deployed on {{ ansible_architecture }} architecture.
    ```

    Bovenstaande template maakt gebruik van de `Ansible facts` (verzameld via de eerste impliciete taak in een playbook `Gathering Facts`).
    De specifieke waardes van deze variabelen worden dan per host vervangen en de file naar de host gestuurd.

- Maak een `Playbook` aan met de naam `motd-facts.yml` die de module template gebruikt:

    ```yml
    ---
    - name: Fill motd file with host data
      hosts: node1
      become: true
      tasks:
        - template:
            src: motd-facts.j2
            dest: /etc/motd
            owner: root
            group: root
            mode: 0644
    ```

- Voer de playbook uit en controleer of, na het inloggen via SSH op `node1` de `motd` aangepast werd.

### Stap 2 - Uitbreiding

- Voeg een lijn toe die de huidige kernel versie toevoegt aan de
    template en dus aan de `motd`.

{{<alert type="tip">}}
Zoek de correcte `Ansible Fact` op die je kan gebruiken in je template
{{</alert>}}

{{<alert type="danger">}}
Spoiler Alert: Oplossing hieronder....
{{</alert>}}

- Zoek de `Ansible Fact`-variabele:

    ```bash
    [student@ControlHost ansible-files]$ ansible node1 -m setup | grep -i kernel

            "ansible_kernel": "5.15.0-27-generic",
            "ansible_kernel_version": "#28-Ubuntu SMP Thu Apr 14 04:55:28 UTC 2022",
    ```

- Pas de template `motd-facts.j2` aan:

    ```txt
    Welcome to {{ ansible_hostname }}.
    Distributie: {{ ansible_distribution }} {{ ansible_distribution_version}}
    Deployed on {{ ansible_architecture }} architecture.
    Running kernel {{ ansible_kernel }}.
    ```

- Laat je playbook nog eens lopen en controleer het resultaat via SSH:

    ```bash
    [student@ControlHost ansible-files]$ ssh ubuntu@X.X.X.X
    ubuntu@X.X.X.Xs password:
    Welcome to Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-70-generic x86_64)
    Welcome to iac-Managed-Host-1.
    Distributie: Ubuntu 22.04
    Deployed on x86_64 architecture.
    Running kernel 5.15.0-27-generic.
    Last login: Sun May  1 16:54:57 2022 from A.A.A.A

    ubuntu@IaC-Managed-Host-1:~$
    ```

© Deze workshop werd gebaseerd op de informatie van [Red Hat Ansible Automation Platform](https://ansible.github.io/workshops/)
