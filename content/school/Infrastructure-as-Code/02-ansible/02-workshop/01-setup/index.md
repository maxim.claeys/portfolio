---
title : Ansible setup
page_title: "WORKSHOP 1: Ansible Setup"
draft: false
weight: 24
---

Deze workshop laat je vooral kennis maken met de omgeving waarbinnen we Ansible gaan gebruiken.
Er wordt ook gezorgd dat de nodige software aanwezig is en de verschillende machines (VM's) toegankelijk zijn.

## 1. Doelstelling

- Weten hoe je de labo-omgeving op de private cloud van onze opleiding kan gebruiken.

- Ansible installeren op de `Ansible Control Host`

- Eenvoudige authenticatie voor SSH gebruiken op de verschillende machines.

## 2. Workshop

### De labo-omgeving

Voor deze Ansible workshop werden enkele Linux VM's voorzien op de private cloudomgeving van onze opleiding.
Het portaal voor deze omgeving kan je vinden op [https://cloud.ikdoeict.gent](https://cloud.ikdoeict.gent).
Aanmelden kan met je **Odisee-account**.

### Stap 1 - De omgeving deployen {#_stap_1_de_omgeving_deployen}

- Eenmaal aangemeld op het portaal kan je in de "Catalog" je deployment `OPO_Infrastructure_as_Code_Workshop` aanvragen en uitrollen.  
Je gebruikt er als deploymentnaam: `IaC-Ansible-WS-Familienaam Voornaam`  
  
  ![test](catalogus.jpg)

- Als alle VM's opgestart zijn kan je best eerst de connecties voorzien met deze VM's (SSH via MobaXterm,...)  
De ip-adressen noteer je hieronder.  

| Virtual Machine      | OS           | IP-adres | credentials      |
|----------------------|--------------|----------|------------------|
| Ansible Control Host | Rocky Linux  |          | student/student  |
| Managed Host 1       | Ubuntu |          | ubuntu/Azerty123 |
| Managed Host 2       | Ubuntu |          | ubuntu/Azerty123 |
| Managed Host 3       | Ubuntu |          | ubuntu/Azerty123 |

{{<alert type="note">}}

De gebruikers in bovenstaande tabel kregen `sudo`-rechten zodat taken die root privileges nodig hebben kunnen uitgevoerd worden.

{{</alert>}}

- Neem van elke virtuele machine een `snapshot` zodat je op elk moment, indien gewenst, kan terugkeren naar een `clean install`.

### Stap 2 - Ansible installeren {#_stap_2_ansible_installeren}

De `Ansible Control Host` zal de machine worden van waaruit alle Ansible taken (playbooks, roles,...) uitgevoerd zullen worden.

- Ansible is niet geïnstalleerd op de Control host en moet dus eerst voorzien worden.  
    De "Ansible-package" is standaard niet aanwezig in de package-repository voor Rocky Linux / CentOS.  
    Er zijn 2 mogelijkheden om de Ansible-package te voorzien ([info](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-rhel-centos-or-fedora)):

  - de rechtstreekse repository van de Ansible ontwikkelaars toevoegen aan je CentOS. Hiervoor moet je wel een subscriptie hebben bij Red Hat (RHEL).

  - ofwel de EPEL-release repository ([Extra Packages for Enterprise Linux](https://www.redhat.com/en/blog/whats-epel-and-how-do-i-use-it)) met pakketten afkomstig van Fedora toevoegen aan je CentOS.

    ``` bash
    [student@ControlHost ~]$ sudo dnf upgrade
    [student@ControlHost ~]$ sudo dnf install epel-release
    [student@ControlHost ~]$ sudo dnf install ansible
    ```

- Controle of Ansible correct geïnstalleerd werd:

    ```bash
    [student@ControlHost ~]$ ansible --version
    ansible 2.9.18
    [...]
    ```

{{<alert type="tip">}}
Ansible houdt Configuration Mangement eenvoudig. Ansible heeft geen databank nodig en ook geen continu draaiende "service".  
Ook op de hosts die je met Ansible wil beheren is er geen software of agent nodig.
{{</alert>}}

{{<alert type="danger" text="warning">}}
Sinds Ansible 2.10 is er een andere benadering. In versies voor 2.10 werden heel veel modules samen aangeleverd bij installatie van Ansible (ca. 4000+).
Deze werden allemaal samen in een repository beheerd door de ontwikkelaars van Ansible, ook de modules die ontwikkeld werden dooe de community.
Omdat uiteindelijk heel veel verschillende partijen in dezelfde repo aan al deze modules moesten meewerken werd dit heel complex. Vanaf versie 2.10 werd alles opgedeeld in verschillende collecties. Op die manier kan er gewerkt worden aan beperktere sets van modules met minder partijen.  
Het gevolg op het gebruik in je Playbooks en Roles is miniem, maar er wordt geadviseerd om de modulenamen `fully qualified collection name` of `#FQCN#` te noteren in je Playbooks. vb. ipv de module `file` gebruik je `ansible.buitlin.file`.  
Aangezien bij installatie niet alle modules meegeleverd worden zal je mogelijks achteraf extra modules moeten installeren.
Dat doe je door extra `collections` toe te voegen. Collections toevoegen kan je doen met `ansible-galaxy collection install <collection.name>`.
{{</alert>}}

### Stap 3: IaC onder versiecontrole

Infrastructure as Code valt bij voorkeur ook onder versiecontrole.
Ook in deze workshop is het de bedoeling om stelselmatig je werk in een repo op te slaan (commit) en te pushen naar een centrale locatie (gitlab.com).

- Maak op gitlab.com/ikdoeict een repo aan voor deze workshop met de naam `2122-IaC_Ansible_workshop_voornaam.familienaam`.
- Zorg voor een "clone" van deze repo op je Ansible Control Host (bij voorkeur in je home-folder)
- Werk nu de workshop verder af binnen deze git-folder. Vergeet uiteraard niet héél regelmatig een commit door te voeren en te pushen naar gitlab.com

{{<alert type="note">}}
Wellicht is `git` nog niet geïnstalleerd op je Ansible Control Host.
Dat is snel gebeurd met:

```bash
sudo dnf install git
```
{{</alert>}}

© Deze workshop werd gebaseerd op de informatie van [Red Hat Ansible
Automation Platform](https://ansible.github.io/workshops/)
