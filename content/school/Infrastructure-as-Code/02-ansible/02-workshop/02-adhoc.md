---
title : Ad Hoc commands
page_title: "WORKSHOP 2: Ad Hoc Commands"
draft: false
weight: 25
---

In deze workshop gaan we Ansible op de meest eenvoudige manier gebruiken via `Ad-Hoc commands`. Dit is zeker niet de meest efficiënte manier maar geeft je inzicht in hoe Ansible taken uitvoert op `Remote Hosts` zonder een playbook (zie later) te gebruiken.

## 1. Doelstelling

In deze workshop leer je:

* het Ansible configuratie-bestand (ansible.cfg) begrijpen
* een `inventory` aanmaken en begrijpen in `INI`- en `YAML` stijl
* `Ad-Hoc commands` uitvoeren met Ansible

## 2. Workshop

### Stap 1 - Werken met een "Inventory"

Om Ansible commands te kunnen gebruiken is er nood aan een `inventory`. Deze inventory bepaalt welke hosts (desktops, servers, firewalls, routers,...) beheerd kunnen worden door de Ansible Control Host. Dergelijke inventory kan opgegeven worden in `INI- of YAML-stijl` en bevat naast de `hosts` eventueel ook `groepen` waartoe deze hosts behoren en mogelijks ook `variabelen` die we later kunnen nodig hebben. ([Meer info](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html))

```ini title="Inventory in INI-stijl"
dummyhost ansible_host=1.2.3.4

[all:vars]
ansible_port=22

[web]
node1 ansible_host=X.X.X.X
node2 ansible_host=Y.Y.Y.Y
node3 ansible_host=Z.Z.Z.Z

[web:vars]
ansible_user=ubuntu
ansible_password=PASSWORD
ansible_connection=ssh

[control]
ansible-1 ansible_host=A.A.A.A

[control:vars]
ansible_user=student
ansible_password=PASSWORD
ansible_connection=local
```

Hosts kunnen allemaal gezamelijk op 1 niveau  (vb dummyhost) of onderverdeeld worden in groepen volgens bv functie (webservers, container-nodes), geografie (Gent, Brussel,...) of andere verdelingen (vb "web" en "control" in het fragment hierboven). Op elk van deze niveaus kunnen dan ook variabelen meegegeven worden (vb all:vars of web:vars). Later zien we nog andere mogelijkheden om variabelen te definiëren.

* Bekijk bovenstaande inventory goed en probeer er de onderverdeling op het hoogste niveau én de onderliggende groepen met hun bijhorende variabelen te begrijpen.

```yaml title="Inventory in Yaml-stijl"
--- 
  all:
    hosts:
      dummyhost:
        ansible_host: 1.2.3.4
    children:
      web:
        hosts:
          node1:
            ansible_host: X.X.X.X
          node2:
            ansible_host: Y.Y.Y.Y
          node3:
            ansible_host: Z.Z.Z.Z
        vars:
          ansible_user: ubuntu
          ansible_password: PASSWORD
          ansible_connection: ssh
      control:
        hosts:
          ansible-1:
            ansible_host: A.A.A.A
        vars:
          ansible_user: student
          ansible_password: PASSWORD
          ansible_connection: local
    vars:
      ansible_port: 22
```

{{<alert type="caution">}}
YAML-files starten steeds met 3 streepjes => "`---`".
De hiërarchie in YAML wordt bepaald door de insprong.
Insprongen mogen niet met "tabs" gemaakt worden maar *moeten* met **`spaties`**!!
{{</alert>}}

Bij YAML-inventories moet er altijd gebruik gemaakt worden van de keywords `all:`, `hosts:` en eventueel ook van `children:` en `vars:`

* Bekijk bovenstaande YAML-inventory goed en probeer net zoals in het vorig voorbeeld de onderverdeling op het hoogste niveau én de onderliggende groepen met hun bijhorende variabelen te begrijpen.

{{<alert type="note">}}
Hosts kunnen in meerdere groepen voorkomen.
{{</alert>}}

{{<alert type="note">}}
Variabelen kunnen, zoals je in de voorbeelden ziet, op verschillende niveaus gedefinieerd worden. Het is immers mogelijk om op het hoogste niveau of per groep of zelfs per host variabelen (of parameters) mee te geven.
{{</alert>}}

Dergelijke  (de ini- of yaml-file) kan op verschillende plaatsen opgeslagen worden. Bij installatie van Ansible werd een default inventory met de naam `hosts` voorzien in `/etc/ansible/hosts`. Als je, bij het uitvoeren van het Ansible-commando, geen inventory opgeeft, dan wordt er teruggevallen op dit exemplaar. Idealiter maak je dus een inventory aan in de locatie van je actieve Ansible-project. Inventories kunnen ook dynamisch opgebouwd worden, bijvoorbeeld met real-time info uit een databank, maar dit valt buiten de scope van deze workshops.

* Maak op je Ansible Control Host een nieuwe map aan onder je home-folder met de naam `ansible-workshop`.

```bash
[student@ControlHost ~]$ mkdir ansible-workshop
[student@ControlHost ~]$ cd ansible-workshop
[student@ControlHost ansible-workshop]$
```

* Maak in deze map een `inventory`-bestand aan voor zowel `Yaml` (*hostsyaml*) als `Ini` (*hostsini*) en voeg onderstaande code toe.
  {{< details "hostini" open >}}
  ```ini
  dummyhost ansible_host=1.2.3.4

  [all:vars]
  ansible_port=22

  [web]
  node1 ansible_host=<X.X.X.X>
  node2 ansible_host=<Y.Y.Y.Y>
  node3 ansible_host=<Z.Z.Z.Z>

  [web:vars]
  ansible_user=ubuntu
  ansible_password=PASSWORD
  ansible_connection=ssh

  [control]
  ansible-1 ansible_host=<A.A.A.A>

  [control:vars]
  ansible_user=student
  ansible_password=PASSWORD
  ansible_connection=local
  ```
  {{< /details >}}
  {{< details "hostsyml" open >}}
  ```yml
  --- 
  all:
    hosts:
      dummyhost:
        ansible_host: 1.2.3.4
    children:
      web:
        hosts:
          node1:
            ansible_host: <X.X.X.X>
          node2:
            ansible_host: <Y.Y.Y.Y>
          node3:
            ansible_host: <Z.Z.Z.Z>
        vars:
          ansible_user: ubuntu
          ansible_password: PASSWORD
          ansible_connection: ssh
      control:
        hosts:
          ansible-1:
            ansible_host: <A.A.A.A>
        vars:
          ansible_user: student
          ansible_password: PASSWORD
          ansible_connection: local
    vars:
      ansible_port: 22

  ```
  {{< /details >}}
**Je past de ip-adressen van je Ansible Control Host en de Nodes aan met je eigen ip-adressen. Ook de wachtwoorden vul je correct aan voor beide type hosts**
De dummyhost mag je negeren (laten staan).

```bash
[student@ControlHost ansible-workshop]$ nano hostsyaml
```

{{<alert type="tip">}}
Je kan eventueel ook gebruik maken van een geavanceerdere editor zoals `Visual Studio Code`. Via een `Remote Window` (Remote-SSH plugin) kan je dan rechtstreeks de bestanden in je Ansible Control Host bewerken. Het grote voordeel van dergelijke editor is dat deze met verschillende extenties kan uitgebreid worden die ondersteuning geven voor het schrijven van Ansible playbooks, YAML-bestanden (via linting), etc...
{{</alert>}}

Je kan je inventory-bestand gaan bevragen en controleren via het command `ansible` en parameter `--list-hosts`.

{{<alert type="tip">}}
Als we in onderstaand command de inventory niet definiëren met `-i`, dan wordt er teruggevallen op de default inventory `hosts` in de map. /etc/ansible. Deze inventory is uiteraard leeg.
{{</alert>}}

* Probeer onderstaande commands uit met zowel je `INI-` als je `YAML`-inventory

```bash
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml web --list-hosts
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml web,control --list-hosts
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml 'node*' --list-hosts
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml all --list-hosts
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml ungrouped --list-hosts
```

```bash
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml web,control --list-hosts
  hosts (4):
    node1
    node2
    node3
    ansible-1
[student@ControlHost ansible-workshop]$
```

### Stap 2 - Ad Hoc Commands

Met een inventory die klaar staat kunnen we nu de eerste Ansible-opdrachten uitvoeren. Dergelijke Ansible-opdrachten worden uitgevoerd via `modules` (later volgt meer info). Een zeer eenvoudige module is de `ping-module`.

{{<alert type="caution">}}
Ansible maakt voor het uitvoeren van zijn opdrachten telkens een connectie (vb via SSH) om daar een script uit te voeren (meestal python-script). Dat script voert dan de gewenste taak uit. Een "ping" in de beschreven module werkt dus niet zoals een klassieke "netwerk-ping" maar voert gewoon een klein scriptje uit op de doelhost. Dat is voldoende om te controleren of de doelhost "bereikbaar" is (resultaat van de ping).
{{</alert>}}

Het Ansible-command kan via de parameter `-m` weten welke module het moet uitvoeren. Opties (parameters) die je wil meegeven met een module doe je via `-a` (zie verder).

* Voer onderstaande `ad-hoc command` uit.

```bash
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml web -m ping

node2 | *SUCCESS* => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    // highlight-next-line
    "ping": "pong"
}
node3 | *SUCCESS* => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    // highlight-next-line
    "ping": "pong"
}
node1 | *SUCCESS* => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    // highlight-next-line
    "ping": "pong"
}

[student@ControlHost ansible-workshop]$
```

### Stap 3 - Ansible Configuration File

Het standaard gedrag van Ansible kan aangepast worden in een `Ansible Configuration bestand`. Dit ansible configuratie-bestand kan dan specifieke instellingen (bv. connectie-settings, locatie van de inventory,...) bevatten die je niet meer hoeft te definiëren in je inventory of playbook. Er is bij de installatie van ansible op het systeem een default configuratie-bestand `ansible.cfg` aangemaakt in de map /etc/ansible.

{{<alert type="note">}}
Het is best practice om een `.ansible.cfg` bestand aan te maken in de map waar je de Ansible commands (ad-hoc, playbooks, roles,..) zal uitvoeren met je specifieke instellingen voor dat project (tenzij de default-waarden voldoende zijn uiteraard)
{{</alert>}}

* bekijk even de inhoud van het configuratie-bestand `ansible.cfg` in de map "/etc/ansible". Je zal er wellicht vaststellen dat alles in commentaar staat. Dit maakt het dus makkelijk om hier regels uit commentaar te halen of je eigen `ansible.cfg`-bestand aan te maken in je projectmap.

```bash
[student@ControlHost ansible-workshop]$ cat /etc/ansible/ansible.cfg

# config file for ansible -- https://ansible.com/
# ===============================================

# nearly all parameters can be overridden in ansible-playbook
# or with command line flags. ansible will read ANSIBLE_CONFIG,
# ansible.cfg in the current working directory, .ansible.cfg in
# the home directory or /etc/ansible/ansible.cfg, whichever it
# finds first

[defaults]

# some basic default values...

#inventory      = /etc/ansible/hosts
#library        = /usr/share/my_modules/
#module_utils   = /usr/share/my_module_utils/
#remote_tmp     = ~/.ansible/tmp
#local_tmp      = ~/.ansible/tmp
#plugin_filters_cfg = /etc/ansible/plugin_filters.yml
#forks          = 5
#poll_interval  = 15
#sudo_user      = root
#ask_sudo_pass = True
#ask_pass      = True
#transport      = smart
...
```

### Stap 4 - Modules en Help

In de default installatie van Ansible werden onmiddellijk heel veel modules voorzien. Maar er kunnen nog steeds achteraf extra modules/plugins toegevoegd worden. Er komen immers steeds producten bij en bepaalde vendors voorzien eigen plugins. Je zou zo ook in principe je eigen plugin kunnen schrijven.

* Om alle onmiddellijk bruikbare modules te zien gebruik je volgend command:

```bash
[student@ControlHost ansible-workshop]$ ansible-doc -l
```

* Probeer even enkele modules van belangrijke spelers te bekijken (ios [cisco], ec2 [amazon], azure, vmware...).

Met bovenstaande overzicht begrijp je onmiddellijk dat Ansible kan ingezet worden voor zowat elk toestel van elke vendor of elk besturingssysteem.

{{<alert type="tip">}}
In de online documentatie van Ansible kan je een beperkt deel van de modules ook terugvinden met bijhorende uitleg over gebruik, parameters, etc... [zie Ansible documentation](https://docs.ansible.com/ansible/latest/collections/index.html) en  [belangrijker](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/) (de Ansible specifieke modules).
{{</alert>}}

* Bekijk eens hoe je de module `user` kan gebruiken (opties, parameters, voorbeelden...)
Parameters voorafgegaan door een `"="` zijn verplicht.

```bash
[student@ControlHost ansible-workshop]$ ansible-doc user
```

### Stap 5 - de module `command`

Een van de modules waarmee we "klassieke" Linux-commando's via Ansible kunnen uitvoeren is de `command`-module.
Dit voert simpelweg het opgegeven (bash-)commando uit op de remote host.

* Voer onderstaande commands eens uit t.o.v. één of meerdere hosts. Op deze manier kan je makkelijk custom commands uitvoeren op je hosts die je misschien niet kan uitvoeren met een andere module.

```bash title="ip-adres info opvragen"
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml node1 -m command -a "ip a"
```

```bash title="kernel-versie opvragen"
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml web,control -m command -a "uname -r"
```

### Stap 6 - de module `copy` en bijhorende "permissies"

In deze stap is het de bedoeling om met de module `copy` de inhoud van de `motd` (in /etc/motd) aanpassen.
`motd` is de "message of the day" wat elke gebruiker te zien krijgt bij een Linux-host bij het aanmelden.

* Bekijk het gebruik (de parameters) van deze module in de [online documentatie](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html#ansible-collections-ansible-builtin-copy-module).

* Voer volgend ad hoc commando uit. Verwacht je wel aan een fout...

```bash
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml node1 -m copy -a 'content="Host managed by Ansible@ikdoeict\n" dest=/etc/motd'
```

De output van bovenstaand commando geeft duidelijk aan dat er een fout is opgetreden. Waarom?
De gebruiker waarmee je dit commando remote uitvoert (in dit geval de user "ubuntu") heeft niet de rechten om te schrijven naar de `motd`-file. Daarvoor moet je verhoogde privileges hebben (sudo-rechten).

```bash
// highlight-next-line
node1 | *FAILED*! => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "checksum": "f23c13d7b4d7ed4b057bb88bea89d7038bac9e4e",
    // highlight-next-line
    "msg": "Destination /etc not writable"
}
```

Manueel zou je in Linux nu het command laten voorafgaan door `sudo`. Met Ansible doe je dit door de optie `-b` of `--become` mee te geven. Daarnaast moet je het wachtwoord meegeven om die sudo-rechten te verkrijgen met `--ask-become-pass` (zie [documentatie](https://docs.ansible.com/ansible/latest/user_guide/become.html)).

* Voer volgend aangepast command uit:

```bash
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml node1 -m copy -a 'content="Host managed by Ansible@ikdoeict\n" dest=/etc/motd' --become --ask-become-pass

BECOME password:
// highlight-next-line
node1 | *CHANGED* => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    // highlight-next-line
    "changed": true,
    "checksum": "4458b979ede3c332f8f2128385df4ba305e58c27",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "md5sum": "65a4290ee5559756ad04e558b0e0c4e3",
    "mode": "0644",
    "owner": "root",
    "size": 19,
    "src": "/home/ubuntu/.ansible/tmp/ansible-tmp-1617217187.197986-8098-265930493570302/source",
    "state": "file",
    "uid": 0
}
```

Om te vermijden dat je telkens het `sudo-wachtwoord` moet opgeven zou je ook kunnen overwegen om dit mee te geven met de inventory.

```yaml
--- 
  all:
    hosts:
      dummyhost:
        ansible_host: 1.2.3.4
    children:
      web:
        hosts:
          node1:
            ansible_host: X.X.X.X
          node2:
            ansible_host: Y.Y.Y.Y
          node3:
            ansible_host: Z.Z.Z.Z
        vars:
          ansible_user: ubuntu
          ansible_password: PASSWORD
          ansible_connection: ssh
          // highlight-next-line
          ansible_become_pass: Azerty123
      control:
        hosts:
          ansible-1:
            ansible_host: A.A.A.A
        vars:
          ansible_user: student
          ansible_password: PASSWORD
          ansible_connection: local
          // highlight-next-line
          ansible_become_pass: student
    vars:
      ansible_port: 22
```

{{<alert type="note">}}
Uiteraard is het geen goed idee om wachtwoorden in clear-text op te nemen in je inventory of playbook. Later zal je zien hoe we dit kunnen veilig oplossen met een ansible `Vault`.
{{</alert>}}

* Geef het vorige command nogmaals maar nu zonder `--ask-become-pass` en gebruik makend van de aangepaste inventory.

```bash
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml node1 -m copy -a 'content="Host managed by Ansible@ikdoeict\n" dest=/etc/motd' --become

// highlight-next-line
node1 | *SUCCESS* => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    // highlight-next-line
    "changed": false,
    "checksum": "4458b979ede3c332f8f2128385df4ba305e58c27",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "mode": "0644",
    "owner": "root",
    "path": "/etc/motd",
    "size": 19,
    "state": "file",
    "uid": 0
}
```

{{<alert type="note">}}
Op eenzelfde manier is het ook mogelijk om de `--become` parameter te definiëren in de inventory (of in een ansible.cfg bestand)
{{</alert>}}

* Controleer met de generieke `command` module de inhoud van het ´motd`-bestand.

```bash
[student@ControlHost ansible-workshop]$ ansible -i hostsyaml node1 -m command -a "cat /etc/motd"
```

## Uitbreiding: Modules

* Ga met het commando `ansible-doc` aan de slag om:
  * een module te zoeken die gebruik kan maken van de package manager `apt`
  * te leren hoe je pakketten (software) kan installeren via deze module

* Voer een ad-hoc commando uit die de laatste versie van `squid` installeert op node1

```bash
[student@ControlHost ansible-workshop]$ ansible-doc ....
[student@ControlHost ansible-workshop]$ ansible-doc ....
[student@ControlHost ansible-workshop]$ ansible -i ....
```

{{<alert type="danger">}}
Spoiler Alert: Oplossing hieronder....
{{</alert>}}

```bash
[student@ControlHost ansible-workshop]$ ansible-doc -l | grep apt
[student@ControlHost ansible-workshop]$ ansible-doc apt
[student@ControlHost ansible-workshop]$ ansible node1 -i hostsyaml -m apt -a "name=squid state=latest" --become
```

(C) Deze workshop werd gebaseerd op de informatie van [Red Hat Ansible Automation Platform](https://ansible.github.io/workshops/)
