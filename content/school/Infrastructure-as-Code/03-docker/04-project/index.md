---
title: project
page_title: project Docker
draft: false
weight: 46
---

## Doelstelling
In dit project:

* Toon je aan dat je verschillende systeembeheertaken kan uitvoeren met Docker
* Pas je zo veel mogelijk de `best practices` toe om Docker-taken uit te voeren.

## Opgave

Voor deze opgave gaan we twee docker webservers bouwen met daarvoor een (https) load balancer. Om het aantal servers te beperken, zal de load-balancer actief zijn op je mgmt-machine.
Het einddoel zie je in onderstaande afbeelding:

![](stap3.png)

### Voorbereiding: Testomgeving

Deploy een nieuwe instantie "OPO_Infra_As_Code_Docker_Project" op de private cloud. Gebruik een gestructureerde naam: 2122-IaC_voornaam_naam-Docker_Project

Maak een git repository, waarin je alle bestanden bewaart die nodig zijn als oplossing voor deze oefening. Alle Dockerfiles, Ansible files, host-files, ... plaats je dus in deze repo. In principe zou je dus met een lege server moeten kunnen starten én de repository om alles snel terug te kunnen opzetten.

De eerste experimenten met Docker gaan we uitvoeren op onze management-machine. (Rocky Linux 8.x)
Later kan je de werkende containers implementeren op de test- en productie-machines. (Ubuntu 22.04)

Vergewis je ervan dat de laatste updates op je MGMT machine staan geïnstalleerd.

Op je Rocky Linux machine (MGMT) kan je kiezen om ofwel Podman, ofwel Docker-ce te gebruiken. Raadpleeg de informatie in de workshops over hoe je beide opties kan installeren.

{{<alert type="danger" text="warning">}} 
het Docker eco-systeem evolueert erg snel, waarbij releases elkaar in een recordtempo opvolgen.
Online zijn er dus heel vaak voorbeelden te vinden die reeds lang voorbijgestreefd zijn. (je zal vermoedelijk al gemerkt hebben dat tutorials voor CentOS7 niet meer werken op CentOS8, en zelfs in subversies (vb CentOS 8.3 vs CentOS 8.2) zijn er significante verschillen...)
{{</alert>}}

De officiële documentatie is daarom het beste vertrekpunt als je naar informatie zoekt.

### Stap1:

In deze stap zal je een load balancer opzetten én een webserver op één host. Dit mag je 'manueel' doen, zonder gebruik van Ansible. 

![](stap1.png)

* Bouw een Dockerfile die in staat is om een eenvoudige Flask-website te hosten (via http). Je kan je baseren op een container met ondersteuning voor Flask (Python) die reeds bestaat. Gebruik 'flask' als tag bij het builden.
Deze Dockerfile kan erg kort zijn, maar zorg minstens voor een eigen pagina die in staat is om een omgevingsvariabele weer te geven op het scherm. Later zullen we die gebruiken om de twee nodes van elkaar te kunnen onderscheiden.
Start deze server op poort 8001 op je MGMT host om deze te testen. Geef bij het starten de omgevingsvariabele mee die je wil tonen. 

{{<alert type="tip">}}
Flask is een framework om websites te maken met Python code. Voor deze opdracht zal je met enkele regeltjes code voldoende hebben, maar ook complexe projecten kunnen hiermee gebouwd worden...
{{</alert>}}

{{<alert type="tip">}}
Linux OS'en uit de RHEL-familie hebben met SElinux een extra beveiligingslaag die constant zal zoeken naar mogelijks risicovolle acties van software. Het mounten van een bestand of map vanuit een container wordt ook als risicovol aanzien. Met de ':z' suffix bij een mount kan je aangeven dat je dit expliciet wil toelaten. 
{{</alert>}}

{{<alert type="danger" text="warning">}} 
indien je podman gebruikt zullen je containers by default niet onderling kunnen communiceren als je de containers niet als root start. Dat is een gevolg van het feit dat podman enkele extra veiligheden heeft ingebouwd. Wens je dat toch te kunnen (om van je load balancer met je node te kunnen communiceren op de zezelfde host bijvoorbeeld), dan kan je de containers toch als root starten (met sudo), of bijvoorbeeld bij het podman run commando met `--network host` aangeven dat je toegang wil tot het hostnetwerk.
{{</alert>}}

* Bouw een load balancer container die https voorziet (als proxy) en die je request doorstuurt naar de onbeveiligde container die je eerder maakte. Dat mag met een self-signed certificaat gebeuren. (aan te maken met openssl) Zorg ervoor dat het certificaat niet in de container zelf zit, maar er aan gelinkt wordt met een volume. Ook de andere configuratie geef je zo mee. 
Suggestie: vertrek bijvoorbeeld van een HAproxy container, of Nginx

{{<alert type="tip">}}
configuratie kan je makkelijk meegeven aan een container als omgevingsvariabele (of door het mounten van een config-file). Zo hoef je de container niet telkens specifiek te builden én vermijd je dat gevoelige info terecht komt op docker hub.
Ook gevoelige bestanden zoals certificaten kan je in runtime koppelen.
{{</alert>}}

### Stap 2a:

Zorg ervoor dat je net gemaakte Flask container 'gepusht' wordt naar je private registry op gitlab.com Daarvoor zal je enkele stappen moeten doorlopen, die je vindt bij je project op gitlab.com via het menu packages & registries -> container registry.

```bash
# aanmelden op docker hub met je odisee account of een aangemaakt token
docker login registry.gitlab.com

# ofwel: je lokale image taggen met je docker hub username
docker tag <imagename>:<tag> docker.io/<username>/<imagename>:<tag>
# ofwel: nieuwe build maken met juiste tag:
docker build -t registry.gitlab.com/(pad) .

# je image pushen naar Gitlab.
docker push registry.gitlab.com/ikdoeict/(pad)

# daarna kan je de container gebruiken op je andere hosts met:
docker run registry.gitlab.com/(pad):<tag>

```

{{<alert type="danger" text="warning">}}
bij een private container repository zal ook authenticatie nodig zijn, ook straks als je deze container binnen wil halen.. Vermijd uiteraard dat je je credentials ergens in cleartext moet achterlaten in de config!
{{</alert>}}

### Stap 2b:

![](stap2.png)

* Maak een Ansible role of playbook zodat de gebouwde web-container automatisch gestart wordt op jouw twee testservers. Je zal daar uiteraard onderliggend ook enkele extra pakketten voor moeten installeren op die machines, in de eerste plaats Docker.
* Geef in die Ansible role of playbook ook de omgevingsvariabele mee die je gaat afdrukken bij het surfen naar de container. Definieer die in je inventory (goed), hostvars files (beter) of vault (best).
* Zorg ervoor dat je load balancer de load effectief kan gaan spreiden over je verschillende containers.

### Stap 3:

![](stap3.png)

* Deploy je twee webservers aan de hand van je playbook én je containers op de repository op je productieservers.

Test uitvoerig: wordt de load wel degelijk verdeeld over je twee servers?

Demonstreer je oplossing aan een docent.

Zorg ervoor dat alle gebruikte playbooks, Dockerfiles, scripts en andere configuratie terug te vinden zijn in je git-repository.

## Uitdaging

Om de oefening helemaal op punt te zetten kan je ook het eerste deel helemaal met Ansible uitvoeren, zodat de config van je load balancer automatisch aangepast wordt naarmate je een deploy doet naar test of productie. 
