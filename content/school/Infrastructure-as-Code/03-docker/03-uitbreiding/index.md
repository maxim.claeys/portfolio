---
title: "Docker: uitbreiding"
draft: false
weight: 45
---

Voor de uitbreiding vragen we je een **efficiënt** gebouwde container te maken om jullie git-project (de workshop voor het secundair die jullie met enkele teamgenoten bouwen) te 'serven'.

Daarvoor krijg je toegang tot de broncode van deze cursus. 

Onze vraag is om een Docker container te bouwen, die niks anders doet dan deze code weer te geven. We streven daarbij naar een zo klein mogelijke image, zonder overbodige tools.

{{<alert type="tip" text="important">}}
Om dat mogelijk te maken, moeten jullie gebruik maken van een `multi-stage` Dockerfile. Op die manier kan je de tools die nodig zijn om de statische site te bouwen weglaten in het eindresultaat.
{{</alert>}}

Benieuwd hoe compact jullie de container krijgen!

Deze uitbreiding wordt uiteraard individueel afgewerkt...
