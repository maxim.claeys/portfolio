---
title: Modus Operandi
page_title: Inleiding
draft: false
weight: 36
---


Deze workshop laat je zien hoe je een container-engine kan installeren in een Linux opgeving.


## Voorbereiding


Voor dit onderdeel is geen specifieke voorbereiding nodig. 
De slides van de theorie zijn terug te vinden op (Gitlab pages)[https://ikdoeict.gitlab.io/docenten/2122_infrastructure_as_code/iac-slides/]

## Doelstelling


De concepten en uitvoering van de basisconcepten rond software containers begrijpen en kunnen toepassen

## Leerdoelen


* Een container-engine installeren op een Linux-omgeving
* Eenvoudige authenticatie voor SSH gebruiken op de verschillende machines. 
* De commando's leren gebruiken om container images binnen te halen
* De commando's leren en toepassen om containers te starten.
* Leren interageren met containers die gestart reeds zijn.
* Zelf docker images beschrijven en samenstellen (Dockerfile)
* De basis commando's van docker toepassen in kleine, begeleide oefeningen

## Opgave 

De opgaves binnen de workshop kunnen worden uitgevoerd op een standaard Linux machine (bijvoorbeeld in de cloud), of op je eigen laptop.
Als alternatief kunnen de opgaves ook integraal online uitgevoerd worden in een simulatie-omgeving.

{{<alert type="info">}} 
Deze opgave wordt individueel afgewerkt... 
{{</alert>}}



## Evaluatie

Bij de verdediging van deze opdracht zal de score opgesplitst worden:

* De praktische implementatie (70%)
* De theoretische achtergrondkennis (10%)
* Uitvoering van extra functionaliteit (20%)

Zorg er dus zeker voor dat je op het moment van de verdediging de theorie nog eens grondig doornam.


