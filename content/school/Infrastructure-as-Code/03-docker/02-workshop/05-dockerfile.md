---
title: Dockerfiles
draft: false
weight: 41
---

Dockerfiles zijn cruciale bouwstenen om te werken met containers. Het zijn tekstbestanden die beschrijven hoe de container opgebouwd wordt. 
Elke lijn die daarin voorkomt creëert een extra laag in de container. Die lagen kwamen al eerder aan bod toen we `docker inspect image` commando uitvoerden.

```Dockerfile
FROM ubuntu 
RUN apt-get update 
RUN apt-get install –y apache2 
RUN apt-get install –y apache2-utils 
RUN apt-get clean 
EXPOSE 80 
CMD [“apache2ctl”, “-D”, “FOREGROUND”]
```

Bovenstaand voorbeeld toont een Dockerfile waarbij een container wordt aangemaakt op basis van Ubuntu. Met `FROM` geef je aan welke base image je wil gebruiken. Hier wordt Ubuntu gekozen, maar als je mikt op een extreem kleine en snelle image, kan je ook opteren voor bijvoorbeeld Alpine.
met `RUN` worden daarna een aantal acties uitgevoerd op de container. Let wel, zoals eerder vermeld zal elke lijn in het bestand een extra laag aanmaken in de container. Het zou dus efficiënter zijn om de verschillende apt-get install commando's te combineren.
Met `EXPOSE` geef je aan welke poort(en) moeten zichtbaar zijn voor de buitenwereld.
Tenslotte wordt `CMD` gebruikt om aan te geven welk proces moet gestart worden. 

De mogelijkheden voor zo'n bestand zijn uiteraard veel ruimer dan wat hierboven beschreven werd. Daarvoor verwijzen we naar enkele externe bronnen:

* [What is Dockerfile](https://dockerlabs.collabnix.com/beginners/dockerfile/Writing-dockerfile.html#what-is-a-dockerfile)
* [Understanding Layering Concept with Dockerfile](https://dockerlabs.collabnix.com/beginners/dockerfile/Layering-Dockerfile.html)

## Best practices

Enkele best practices bij het maken van Dockerfiles

* Combineer gelijkaardige commando's in één regel om het aantal lagen beperkt te houden (bijvoorbeeld de installatie van verschillende pakketten in één regel in plaats van meerdere)
* Zet commando's die vaak veranderen (vb source files kopiëren) relatief onderaan je Dockerfile, dat komt de snelheid bij het builden ten goede omdat cache dan optimaal kan gebruikt worden.
* Vermeld steeds de tags van base images zodat je zeker bent dat je image ook op lange termijn stabiel blijft. (vb `FROM ubuntu:22.04` ipv `FROM ubuntu`)

## De container bouwen

Eens de Dockerfile klaar is, kan daarvan een image gemaakt worden met `docker build`

```shell
# bouw een image op basis van de Dockerfile in de huidige map
docker build .

# bouw een image met meteen een tag aan gekoppeld
docker build . -t roelvs/dockie
```

## Bijhorende labs

Maak een container op basis van Alpine waarin je Git installeert:
 * [Lab #1: Installing GIT](https://dockerlabs.collabnix.com/beginners/dockerfile/lab1_dockerfile_git.html)

De verschillende instructies gedemonstreerd:
* [Lab #2: ADD instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/Lab-2-Create-an-image-with-ADD-instruction.html)
* [Lab #3: COPY instruction](https://dockerlabs.collabnix.com//beginners/dockerfile/lab4_dockerfile_copy.html)
* [Lab #4: CMD instruction](https://dockerlabs.collabnix.com//beginners/dockerfile/lab4_cmd.html)
* [Lab #5: ENTRYPOINT instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/Dockerfile-ENTRYPOINT.html)
* [Lab #6: WORKDIR instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/WORKDIR_instruction.html)
* [Lab #7: RUN instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/Lab-7-Create-an-image-with-EXPOSE-instruction.html)
* [Lab #8: ARG instruction](https://dockerlabs.collabnix.com//beginners/dockerfile/arg.html)
* [Lab #9: ENV instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/Lab_%239_ENV_instruction.html)
* [Lab #10: VOLUME instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/Lab%2310_VOLUME_instruction.html)
* [Lab #11: EXPOSE instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/Lab%2311_EXPOSE_instruction.html)
* [Lab #12: LABEL instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/Label_instruction.html)
* [Lab #16: Entrypoint Vs RUN](https://dockerlabs.collabnix.com/beginners/dockerfile/entrypoint-vs-run.html)
* [Writing Dockerfile with Hello Python Script Added](https://dockerlabs.collabnix.com/beginners/dockerfile/lab_dockerfile_python.html)

## Uitbreidingslabs

 * [Lab #13: ONBUILD instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/onbuild.html)
 * [Lab #14: HEALTHCHECK instruction](https://dockerlabs.collabnix.com/beginners/dockerfile/healthcheck.html)