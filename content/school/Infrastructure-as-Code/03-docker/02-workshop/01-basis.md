---
title: Inleiding
draft: false
weight: 37
---


# Inleiding

Aangezien Docker (containertechnologie in het algemeen) ondertussen een heel breed toepassingsgebied heeft en dus door een heel grote community gebruikt wordt, bestaat er ook heel veel materiaal om Docker aan te leren.
Wij kozen er voor om jullie Docker gedeeltelijk aan te leren via bestaand lesmateriaal. Dit zal aangevuld worden met extra oefeningen en een individueel project om Docker in de vingers te krijgen.

## Verloop

Doorloop onderstaande hoofdstukken volledig en probeer heel goed in detail de verschillende commando's en hun werking te begrijpen.

In het lesmateriaal dat volgt kan je een *geïntegreerde terminal omgeving* (gebaseerd op ubuntu) gebruiken [op deze site](https://labs.play-with-docker.com/) maar je kan evengoed *je eigen VM* uit de vorige workshop gebruiken. De keuze is aan jou.... Maar in functie van toekomstige oefeningen kan het interessant zijn om onmiddellijk je eigen omgeving te testen.

{{<alert type="tip">}}

Indien je gebruikt maakt van je eigen VM via een Remote-SSH connectie in Visual Studio Code (VSC), dan zal je wellicht de rechten van de gebruiker "ubuntu" moeten verhogen om de `docker-extensie` te kunnen gebruiken. VCS maakt connectie als `non-root` user en Docker commands uitvoeren kan enkel als `root`. Dit kan je oplossen door bv. je gebruiker extra (sudo) rechten te geven (zie [documentatie Msft](https://github.com/microsoft/vscode-docker/wiki/Troubleshooting) en [docker](https://docs.docker.com/engine/install/linux-postinstall) )

{{</alert>}}

#### Prerequisite

Voor het verloop van de workshop wordt verwacht om een account aan te maken op `DockerHub` (link [https://hub.docker.com]). Dit is echter één van de mogelijk bruikbare registries waar je container images kan halen. We zullen later gebruik maken van een eigen `image repostory` in `gitlab`.
