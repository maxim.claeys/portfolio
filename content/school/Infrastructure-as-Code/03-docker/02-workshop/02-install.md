---
title: Installatie
draft: false
weight: 38
---

# Installatie van Docker

## Inleiding

De meeste gekende containerapplicatie is zonder meer *Docker*. Docker ligt dan wel aan de basis van de popularisering van containers, toch is het niet meer de enige speler.  

Waar Docker als een monolitisch geheel zowel zorgt voor de `container runtime/engine`, de `container distribution` en de `container images`, zijn er verschillende andere tools die deze functies apart uitvoeren.

Aangezien de containers en hun images zelf moeten voldoen aan de OCI (Open Container Initiative) kunnen deze eigenlijk op verschillende container runtime omgevingen draaien.

Een voorbeeld als alternatief voor Docker:

* *Podman*: voor het runnen van containers (alsook "pods" )
* *Buildah*: voor het "builden" van images
* *Skopeo*: voor de image distributie 

Ook onderliggend kan de werking en aanpak van een container-engine verschillend zijn. Een belangrijk verschil is bijvoorbeeld dat Docker steeds als `"deamon"` draait. Daarnaast zal Docker ook altijd moeten beschikken over `"root privileges"`. Dit is op het gebied van beveiliging niet het meest ideale scenario.

In de praktijk hoeft het geen grote aanpassing te vergen om met andere tools dan Docker aan de slag te gaan. Tools zoals `Podman` en `Buildah` voorzien complete compatibiliteit met de native Docker Commands (m.b.v. aliassen).

In de informatie hierboven spreken we uiteraard over container-engines in een Linux omgeving. Ondertussen is het echter ook al een tijdje mogelijk om in een Windows-omgeving containers te draaien. Afhankelijk van welke Windows-versie (Windows 10/11 - Windows Server) en de aanpak kan je enkel Windows-containers of zowel Windows- en Linux containers draaien.

## Installatie van Docker

In deze workshops zullen alle voorbeelden en oefeningen de standaard Docker commands gebruiken. In principe doet het er dan niet toe welke onderliggende container engine gebruikt wordt.

Hieronder geven we mee hoe je in Centos en Ubuntu Docker kan installeren. Voor meer gedetailleerde uitleg en installatie op andere distributies kan je een kijkje nemen in de [documentatie van Docker](https://docs.docker.com/engine/install/).

### Docker-ce op Ubuntu

Voor het vervolg van de workshop kan je zelf een VM deployen in onze [private cloud](https://cloud.ikdoeict.gent). Deze installatie-procedure voor CentOS kan je ook bekijken in [de documentatie](https://docs.docker.com/engine/install/centos/)

* Deploy op onze private cloud de template `OPO_Infra_As_Code_Docker` en geef die als naam `IaC_Docker_Voornaam.familienaam`.
* Zorg dat je een SSH-connectie legt met deze Ubuntu VM. +
(username: *student*, wachtwoord: *student*) +

{{<alert type="tip">}}
Het is aan te raden om ook hier via een IDE zoals Visual Studio Code de SSH-connectie te maken met de VM. In een latere fase wordt het dan opnieuw makkelijk om de bestanden (Dockerfiles, etc...) te bewerken en ondersteuning te krijgen van extenties voor syntax, linting,....
{{</alert>}}

* Eventuele oude versies van docker of container runtimes verwijderen:

```shell

ubuntu@IaC-Docker-Host:~$ sudo apt-get remove docker 
docker-engine docker.io containerd runc

```

Om de effectieve installatie uit te voeren zijn er verschillende manieren. Het makkelijkste is gebruik maken van de `Docker package repository`. Deze wordt uiteraard standaard niet gebruikt door de package-manager `apt` en moet dus eerst nog toegevoegd worden als een nieuwe installatiebron.

* Voorzie de extra installatie repository: 
  * eerst installatie van extra packages (prerequisites)

```shell
ubuntu@IaC-Docker-Host:~$  sudo apt-get update
ubuntu@IaC-Docker-Host:~$  sudo apt-get install \
                                apt-transport-https \
                                ca-certificates \
                                curl \
                                gnupg \
                                lsb-release
```


  * Opnemen van de GPG key (voor beveiligde communicatie met de repository)

```
ubuntu@IaC-Docker-Host:~$  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```


  * Toevoegen van de link naar de installatiebron


```
ubuntu@IaC-Docker-Host:~$  echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

* Effectieve installatie Docker Engine:

In de volgende stap wordt de Docker Engine samen met de Docker Runtime en de Docker CLI geïnstalleerd. De officiële naam van Docker die wij gebruiken is de `Community Edition (ce)`, vandaar `docker-ce` bij het installeren. Er bestaat ook een `Enterprise Edition`.


```
ubuntu@IaC-Docker-Host:~$  sudo apt-get update
ubuntu@IaC-Docker-Host:~$  sudo apt-get install docker-ce docker-ce-cli containerd.io
```

* Controle:

Een snelle controle om te zien of Docker goed geïnstalleerd werd (en dus ook remote images kan downloaden) is een `hello-world test` uitvoeren:


```
ubuntu@IaC-Docker-Host:~$  sudo docker run hello-world

Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
b8dfde127a29: Pull complete 
Digest: sha256:f2266cbfc127c960fd30e76b7c792dc23b588c0db76233517e1891a4e357d519
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

### Containers op Rocky Linux 8

Voor het verder verloop van de workshop hoef je onderstaande niet uit te voeren. Je hebt de keuze voor Docker of Podman als je containers wil gaan gebruiken op RHEL, Centos, Rocky Linux of Almalinux.

Hieronder worden beide installatiemethodes gedemonstreerd.

#### Docker-ce op Rocky Lnux 8
Mocht je gebruik willen maken van Docker-ce op de Rocky Linux 8 VM die wij in onze private cloud voorzien dan moet je voorafgaand even het volgende uitvoeren:

* Aangezien op de voorziene image van Rocky Linux 8 door Red Hat al enkele onderdelen van `podman` en `buildah` geïnstalleerd werden, moeten deze allereerst verwijderd worden.

```
student@IaC-Docker-Host:~$ sudo yum remove buildah docker podman
```

Na het uitvoeren van bovenstaande installatie kan je gelijkaardig aan de docker-commands, images binnenhalen, containers starten/stoppen, etc... met `podman image pull`, `podman container start`, `podman container run`, enz...

* Voor het verdere verloop volg je de installatie-procedure van (https://docs.docker.com/engine/install/centos/)[de documentatie].

#### Podman op Rocky Linux 8

Uiteraard is het ook mogelijk om te werken met de container tools die Red Hat bij zijn distributies voorziet, nl `Podman` en `Buildah`. Dit brengt zeker enkele voordelen (deamon-less, root-less, pods mogelijk,...) met zich mee die hier niet verder uitgelegd worden.

* Wil je de native containeroplossing van Red Hat installeren dan kan dat via:

```shell
student@IaC-Docker-Host:~$ sudo yum install podman
```

{{<alert type="tip">}} 
Wil je met excact dezelfde `commands` werken zoals bij `docker-ce`, dan kan je de package `podman-docker` installeren. Deze zorgt voor een naadloze aliasing tussen de docker- en podman commands.
{{</alert>}}