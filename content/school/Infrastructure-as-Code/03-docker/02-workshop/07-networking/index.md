---
title: Networking
draft: false
weight: 43
---

## Types netwerken in Docker
Een aspect dat vroeg of laat komt bovendrijven als je werkt met docker zijn de netwerken.
Het is daarbij goed om weten dat er verschillende mogelijkheden zijn om je container te verbinden met een netwerk.

Zonder extra configuratie zal daarvoor een bridge (met ook als naam 'bridge') gebruikt worden, die via een gateway (en een nat-translatie) met het netwerk van je computer verbonden is.

Dat betekent dat alle containers op dat interne netwerk met elkaar kunnen communiceren.

Schematisch ziet dat er zo uit:

![docker networking](dockernetworking.png)

Je kan die informatie ook opvragen door gebruik te maken van `docker network inspect bridge`. Daarmee krijg je de configuratie van het netwerk in json-formaat terug. Je vindt er oa het subnet dat gebruikt wordt op de interne bridge.

```json
roelvs@comp01:~$ docker network inspect bridge
[
    {
        "Name": "bridge",
        "Id": "c61b5c81a64da46c00f71cd40de6cacfb5a81d0d006f27f14016ba33f1fcddd2",
        "Created": "2022-05-11T05:58:34.173773078Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "8df725f0d9f38942d703fcbcc80a2dc90dfd0f1c5e42b8776756b52072a14b2c": {
                "Name": "suspicious_albattani",
                "EndpointID": "15b49224a7226abe8fc8588f5868d2b99a95d6fd30e374063c082a407ef5ab6c",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
```

Er zijn uiteraard ook nog een aantal andere mogelijkheden (naast briding) om networking te configureren:
* **host networking**: daarmee bevindt je host zich op hetzelfde netwerk als de containers
* **overlay**: met dit netwerktype kunnen containers met elkaar communiceren over hetzelfde netwerk, zelfs als ze zicht op een andere host bevinden. Dit is uiteraard enkel relevant in opstellingen met meerdere hosts.
* ... (er zijn nog enkele andere mogelijkheden, maar die vallen buiten de scope van deze workshop.)

## Aanmaken van nieuwe netwerken

Je kan zelf ook netwerken toevoegen aan je docker-setup, en daar dan containers aan koppelen.

```shell
$ docker network create -d bridge my-bridge-network
```

Na het aanmaken kan je uiteraard ook een lijst van de actuele netwerken opvragen:

```shell
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
1befe23acd58        bridge              bridge              local
726ead8f4e6b        host                host                local
ef4896538cc7        none                null                local
```

## Port mapping

Erg vaak zal je remote toegang willen geven tot specifieke poorten op je container. Denk bijvoorbeeld aan een webserver waar je remote toegang nodig hebt.

Dat kan met een port mapping. Op de achtergrond maak je daarmee eigenlijk een NAT/PAT-vertaling aan waardoor een externe poort gelinkt wordt aan een interne.

Een voorbeeld maakt veel duidelijk:

```shell
$ docker run -d -p 8081:80 nginx
```

Hiermee ga je de poort 8081 op de host koppelen aan poort 80 op de container. 

## DNS

Erg interessant is dat docker op de achtergrond ook een DNS-systeem in stand houdt. Als je bijvoorbeeld een container **web** en een container **mysql** hebt, dan kan je die namen ook gewoon gebruiken om ze onderling te laten communiceren.

{{<alert type="danger" text="warning">}}
Gebruik de interne adressen niet om tussen containers te communiceren. Die zijn gedoemd om te veranderen. Vertrouw dus steeds op DNS...
{{</alert>}}

## Bijhorende labs

* [Lab #4: Docker Bridge Networking](http://dockerlabs.collabnix.com/networking/A2-bridge-networking.html)
