---
title: Volumes
draft: false
weight: 42
---

Bij het starten van je container is er de mogelijkheid om externe volumes te koppelen. Je zou bijvoorbeeld een volume kunnen bouwen en dat koppelen aan de /upload folder van je webapplicatie. Op die manier komen de uploads in je webapplicatie niet in je originele image, maar wel in een apart volume. Dat geeft zeker z'n voordelen: je kan dit volume delen met meerdere containers, of je container integraal vervangen door een nieuwere en toch de uploads behouden.

Commando's voor het aanmaken en beheren van volumes:

```shell

# Creating Volume called demo
docker volume create demo

#Listing Docker Volumes
docker volume ls

#Inspecting “demo” Docker Volume
docker inspect demo

#Removing the “demo” Docker Volume
docker volume rm demo

```

Eens het volume aangemaakt is moet het uiteraard ook gekoppeld worden. Dat wordt gedemonstreerd in onderstaand commando

```shell
docker run -v my-volume:/var/www/html --name my-container nginx
```

In bovenstaand commando wordt de volume `my-volume` gemount in de map `/data` in de nginx-container.

Bijhorend lab:
* [Creating Volume Mount from **docker run** command & sharing same Volume Mounts among multiple containers](https://collabnix.github.io/dockerlabs/beginners/volume/creating-volume-mount-from-dockercli.html)