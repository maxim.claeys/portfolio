---
title: Even oefenen
draft: false
weight: 44
---

Om de belangrijkste principes in de vingers te krijgen kunnen onderstaande externe labo's nog helpen.

Volg onderstaande courses als volgt:

1. [Deploy Static HTML Website as Container](https://www.katacoda.com/courses/docker/create-nginx-static-web-server)
2. [Building Container Images](https://www.katacoda.com/courses/docker/2)
3. [Create Data Containers](https://www.katacoda.com/courses/docker/data-containers)
4. [Persisting Data Using Volumes](https://www.katacoda.com/courses/docker/persisting-data-using-volumes)