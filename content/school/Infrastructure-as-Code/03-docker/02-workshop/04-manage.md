---
title: Containers beheren
draft: false
weight: 40
---

## Starten en stoppen van containers
Uiteraard zullen we in het dagelijks beheer van containers deze vaak moeten starten en stoppen. Het spreekt dus voor zich dat deze commando's goed moeten gekend zijn. Oefen onderstaande commando's zeker op je eigen systeem, bij het project zal je deze ongetwijfeld vaak nodig hebben:

```shell
# starten van de nginx container
docker run nginx

# starten van de nginx container als daemon (=achtergrondproces)
docker run -d nginx

# toekennen van een naam aan een container
docker run --name mijncontainer nginx

# lijst opvragen van alle containers op het huidige systeem
docker ps -a

```

* [Managing Docker Containers](http://dockerlabs.collabnix.com/beginners/managing-containers.html)

## Interageren met containers

Initieel kan het wat lastig lijken om te troubleshooten op containers. Als iets fout gaat kan je iets minder terugvallen op de reflexen die je de voorbije jaren aanleerde. 
Toch hoeft dat geen probleem te zijn, je kan immers ook steeds de motorkap van de container opheffen om problemen te zoeken.

### De output volgen

Soms is het genoeg om de output te zien die normaalgezien zichtbaar zou zijn mocht de container een volwaarde machine zijn. Om dat te kunnen doen is er het `docker attach` commando. Je zal daarmee de standaard output zien die je ook zou zijn mocht je rechtstreeks de `stdout`, `stderr` en `stdin` kunnen zien.

```
docker attach mijncontainer
```

### Commando's uitvoeren

Met `docker exec` kan je een commando uitvoeren binnenin je container. Uiteraard moet het dan om een commando gaan dat bestaat in de container. Dat laatste kan een uitdaging zijn op een sterk gestripte basisimage.

```shell

# start het ls commando in de container
docker exec mijncontainer ls

# start een interactieve terminal in de container
docker exec -it mijncontainer /bin/bash

```

de parameters `-it` zijn daarbij cruciaal om een 'echte' interactieve shell te krijgen op de container. 

* `-i`: hiermee verkrijg je een interactieve sessie. Je keyboard-aanslagen worden dus naar de container gestuurd.
* `-t`: hiermee verkrijg je een pseudo-terminal


Bijhorende labs:

* [Accessing the Container Shell](http://dockerlabs.collabnix.com/beginners/accessing-the-container.html)
* [Running a Command inside running Container](http://dockerlabs.collabnix.com/beginners/running-command-inside-running-container.html)
