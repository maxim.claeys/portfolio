---
title : "project"
page_title: "Project: workshops voor het secundair"

draft: false
weight: 21
---

## Doelstelling

De beste manier om Git goed in de vingers te krijgen, is er mee aan de slag gaan.

Dat zullen we de komende weken doen aan de hand van een project waar jullie samen aan werken.

Doorheen de volgende zeven weken worden een aantal workshops gemaakt. Die workshops worden op zo’n niveau geschreven zodat iemand uit het secundair die zou kunnen doorlopen. Bedoeling is om die workshops nadien ook effectief aan te bieden aan geïnteresseerde leerlingen en hun scholen. Zo kunnen ze zich thuis uitleven met technologieën die ze in hun latere studies misschien nog zullen kunnen gebruiken.

Om zo’n workshop te maken, gebruiken we Docusaurus. Dat is een statische site-generator op basis van Markdown. Zonder het misschien te weten ken je dit al: deze opgave werd namelijk ook gemaakt met Docusaurus.

Hoewel we de kwaliteit van de workshop ook opnemen in de evaluatie, ligt de focus op het correct gebruik van Git.

## Werkwijze

Jullie krijgen de kans om zelf een projectgroep te vormen met maximum vier leden.
Voor sommige workshops verzonnen we reeds een titel, maar jullie kunnen er zeker zelf ook nog suggereren.

De repositories voorzien we ook voor jullie. Eens de teams verdeeld zijn, voegen we jullie toe als eigenaar aan jullie repository op gitlab.com. Die repositories zijn nog leeg. Bedoeling is een hierin een "clone" te voorzien met een lege Docusaurus site, zodat je vlot kan starten zonder je te moeten verdiepen in de werking van Docusaurus. De handleiding voor een lege Docusaurus website kan je [hier](https://docusaurus.io/docs/installation) vinden.

Om de workshops te stroomlijnen, moet je workshop uitvoerbaar zijn op een Ubuntu 22.04 VM. De workshop moet dus daarop getest worden.

Deze VM is opgebouwd door:

* Ubuntu 22.04 DESKTOP met laatste updates geïnstalleerd (minimal install)
* username ubuntu, wachtwoord Azerty123
* keyboard BE
* visual studio code pre-installed
* RDP enabled

{{<alert type="tip">}}
Als je de workshop wil gaan testen, zal je zien dat er hiervoor een nieuwe template werd aangemaakt op de cloud-omgeving. Die is geconfigureerd zoals hierboven beschreven..
{{</alert>}}

Om de workshops ook inhoudelijk en qua diepgang in lijn te brengen, is het belangrijk om enkele gezamenlijke veronderstellingen de definiëren:

* Geschatte tijdsduur om de workshop te doorlopen: 1,5u à 2u. Eventueel kan dit uitgebreid worden met optionele oefeningen om tot max 4u workshop te komen.
* Doelpubliek: leerlingen uit het secundair met ‘iets’ van informatica-achtergrond. Ga er dus niet van uit dat ze Linux of een programmeertaal kennen, maar wel dat ze in staat zijn om zonder veel uitleg bijvoorbeeld een console kunnen openen op die virtuele machine.
* We werken enkel met publiek beschikbare tools. Op die manier kan de workshop overal georganiseerd worden zonder licentiekosten.

## Planning

Hieronder vind je de algemene planning. De precieze deadlines (datum en uur) vind je in het planningsdocument.

### WEEK1

* De opgave wordt toegelicht, studenten krijgen de kans om zich te groeperen en een onderwerp te kiezen.
* Er worden groepsjes gevormd van maximaal 4 studenten (afwijking kan uitzonderlijk met toestemming van de docenten).
* Nadat de teams gevormd zijn, worden de repositories door de docenten aangemaakt.

### BEGIN WEEK2

* Er wordt een repository in gebruik genomen per team. De teamleden hebben daarbij alle rechten, maar niet-teamleden hebben enkel leesrechten.
* De voorbeeld template (docusaurus) neem je over in je eigen repository.
* Op het hoogste niveau in de repository voorzie je een “readme.md” document waarin je kort de namen van je teamleden opneemt en beschrijft wat er in deze repo te vinden is, waarom je dit onderwerp gekozen hebt, etc…
Dit document staat eigelijk los van de rest van de workshop die je gaat uitwerken.

* Je werkt tijdens deze week ook de `inleiding` af voor de workshop.  Dit is algemene info die opgenomen zal worden in de Docusaurus cursus. Dit zal dus zichtbaar zijn voor derden (leerkrachten, docenten, andere studenten die de workshop volgen). In deze inleiding beschrijf je voor je doelpubliek wat je workshop precies zal inhouden en hoe er te werk zal gegaan worden.

* Het inhoudelijk werk aan de workshop wordt aangevat…

### WEEK3

* Je werkt inhoudelijk verder aan de workshop

### EIND WEEK4

* De workshop moet in een fase zijn dat de main branch te builden valt in docusaurus (met het op dat moment beschikbare materiaal)
* De link naar de site op Gitlab pages werd toegevoegd in het document met de teamverdeling.

### EIND WEEK5

* Inhoudelijk moet de workshop al te testen vallen door externen. (Er moet dus al voldoende materiaal aanwezig zijn om iets uit te voeren.)
Dat niet alle details uitvoerig getest zijn, en dat er tekstueel nog verbeteringen mogelijk zijn, zien we nog even door de vingers.

* Er wordt daarnaast een specifiek document aangemaakt voor de leerkrachten. Dit bevat enkele extra tips of codefragmenten om de opgave op te lossen. Ook eventuele tips voor het opzetten van deze workshop (vb accounts aanmaken, …) kunnen hierin komen.

### WEEK6

* Iedereen test individueel minstens één workshop van een ander team. Minstens één voorgestelde wijziging wordt via een pull/merge request doorgestuurd naar de originele auteurs. Problemen of suggesties die niet meteen kunnen opgelost worden via een pull request worden aan het oorspronkelijke team gemeld aan de hand van een `issue`. (tegen vrijdagmiddag 03/06 13u)

### WEEK7

* Voor de deadline (9 juni 20u) moeten alle pull requests gemerged zijn. Alles moet nu dus af zijn. Alle verbeteringen/wijzigingen moeten doorgevoerd zijn. Er kunnen hierna geen wijzigingen meer aangebracht worden.

### WEEK8: evaluatie

* In deze week worden de groepjes uitgenodigd door de docenten voor evaluatie

Er wordt gequoteerd op:

* Best practices gebruikt voor commit messages
* Altijd gewerkt met feature branches
* Kwaliteit van de wijzigingen
* Kwaliteit workshop (door peer-evaluatie)
* Aanmaken pull requests
* Gebruik van de Gitlab-features zoals issues, borden, ...

## Git strategie

Uiteraard ligt de focus in deze opgave op het correct gebruik van GIT.

Daarom volgende afspraken:

* De main branch bevat steeds de release versie. Het zal deze versie zijn die gebruikt wordt om de cursus te bouwen tot een website. **In deze branch moet dus op elk moment een “productie-versie” zitten**: zonder syntax-fouten, onafgewerkte pagina’s, ...

* Je maakt een develop branch waarop je actief gaat werken. Wil je zelf wijzigingen doen, dan maak je een feature branch van de develop branch. Geef die branches altijd een naam die start met `feat-`

* Als op een bepaald moment de develop branch productie-waardig is, dan kan je die mergen met main.

* Gebruik de issue-functionaliteit én het kanban-bord dat ingebouwd zit in Gitlab om samen te werken.

* Dit zou dus een voorbeeld kunnen zijn van een degelijke git-tree: (van onder naar boven lezen)
![Git tree](git_tree.png)
