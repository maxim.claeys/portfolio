---
title : "Modus Operandi"
page_title: "Inleiding"
draft: false
weight: 2
---


## Voorbereiding

Een specifieke voorbereiding voor deze workshop is niet nodig.


## Leerdoelen


* Begrip van het concept versie controle
* Werken met git via de command line
* Het gebruiken van branching
* Met meerdere personen samenwerken via versie controle

## Opgave 

De workshop wordt individueel afgewerkt... <br/>
Het bijhorende project is wel teamwork.



## Evaluatie

Bij de verdediging van deze opdracht zal de score opgesplitst worden:

* Het resultaat van het project:
  * bruikbaarheid van de gemaakte workshop
  * consequent gebruik van branches tijdens het project
* Peer-evaluatie

Zorg er dus zeker voor dat je op het moment van de verdediging de theorie nog grondig herhaalde.


