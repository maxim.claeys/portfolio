---
title : "Git"
page_title: Git, Gitlab en GitHub
draft: false
weight: 4
---

GitHub en Gitlab zijn zo alomtegenwoordig, dat de kans bestaat dat je de termen Git en GitHub misschien door elkaar gebruikt.

`Git` is het distributed version control systeem dat oorspronkelijk door Linus Torvalds werd ontwikkeld, met als primair doel om de Linux kernel te ontwikkelen.

Andere version control systemen waar je mogelijks al over hoorde zijn Subversion, Mercurial, CVS.

`Gitlab, Github en Bitbucket` daarentegen zijn enkele bekende platformen die toelaten om GIT-repositories te gaan beheren. Die bieden naast de pure Git-functionaliteit ook nog een pak andere tools die een totaalbeleving bieden aan de programmeur: wiki, rechtenbeheer, ci/cd, kanban-borden, ...

Op de campus werken we met Gitlab (https://gitlab.com) als interface om GIT repositories te beheren, maar je zal merken of gemerkt hebben dat de gelijkenissen met GitHub treffend zijn.

## Wegwijs in Gitlab

In Gitlab wordt alles georganiseerd in projecten. De belangrijkste component van zo'n project is ongetwijfeld de repository, maar een project bevat (optioneel) ook een wiki, issues, CI/CD tools etc. 

Projecten kunnen ook gegroepeerd worden in 'groepen' (groups), een extra beheersniveau dat het makkelijk kan maken om instellingen meteen voor een aantal projecten in te stellen. 

{{<alert type="danger" text="warning">}}
Je kan op eender wel moment een project toevoegen aan een groep, maar wees je er wel van bewust dat dat ook zal betekenen dat sommige links zullen veranderen.
{{</alert>}}

Om het studenten makkelijk te maken om alle relevante projecten voor een bepaald vak te vinden, worden deze bijvoorbeeld vaak gegroepeerd in zo'n groep.

![Gitlab projecten en groepen](gitlabprojectsandgroups.png)

Aanmelden doe je op gitlab.com zoals beschreven op de helpdesk, en zowel op gitlab.com en github.com kan je zelf ook accounts aanmaken. Voor persoonlijk gebruik is dat meestal gratis, voor extra beheersfuncties die vooral binnen bedrijven nodig zijn wordt wel een prijs aangerekend.

