---
title: "Project"
page_title: "Toepassing Project NSX-T - uitwerking scenario"
draft: false
weight: 81
---

## Situering

Om de krachtige toepassing van netwerkvirtualisatie duidelijk te maken, volgen we een fictief scenario. We nemen de rol op van kleine cloud provider die z'n cloud wil delen met meerdere klanten die elk een tenant zullen krijgen. In principe kunnen deze verschillende klanten (tenants) identieke, overlappende ip-ranges gebruiken. Door gebruik te maken van VRF's of een extra NAT-laag kan dit mogelijk gemaakt worden. In onderstaand scenario kiezen we echter wel een duidelijk verschillende range per tenant.

In de voorgaande stappen (`hoofdstuk Private Cloud: vSphere` > `NSX-T configratie basiscomponenten`) hebben jullie reeds een aanzet gekregen hoe een T0 en een T1 gateway kunnen geconfigureerd worden. 

{{<alert type="info">}}
Deze **bestaande T0** zal **als Provider gateway gebruikt** worden. Voor het verder verloop zullen nieuwe tenants moeten voorzien worden en dus 2 nieuwe T1 gateways.
{{</alert>}}


Om dit scenario onmiddellijk een nuttige wending te geven zullen de virtuele machines die opgezet worden onze eigen vsphere omgeving gaan `monitoren` en `via dashboards visualiseren`.

{{<alert type="danger" text="warning">}}
Het is belangrijk om te weten dat het eerste deeltje waarbij je nog fungeert als "*provider*"  samen met je teamgenoot moet uitgevoerd worden (acties en configs op de T0-gateway). Het stuk van de "*Tenants*", met T1-gateways, moet **elke student individueel** gaan opzetten.
Er zal dus 2 keer een gelijkaardige infrastructuur uitgerold worden (2 tenants) maar deze zouden niet mogen interferen met elkaar.
{{</alert>}}

Verder zal het belangrijk zijn om je goed te informeren en enkele goede bronnen er bij te halen om je te begeleiden bij keuzes en uitwerkingen van dit scenario.
We geven zelf enkele handvaten die voldoende zouden moeten zijn om alles te configureren zonder alles in detail te beschrijven.

## Opmerkingen

* Wees voorzichtig met de resources die je uitdeelt aan de verschillende componenten. CPU, memory en storage is er voldoende als je niet kwistig bent.

* Maak in je vcenter zowel bij `"Hosts and Cluster"` als bij `"VM's and Templates"` repectievelijk een `Resource Pools` en `folders` aan met de namen **"Provider Resources"**,**"Tenant1"** en **"Tenant2"**. Zo kan je alle routers, virtuele machines netjes gescheiden houden onder elkaar.

![folders](folders.jpg)

## Uitwerking

Om het scenario uit te leggen kan een schema veel verduidelijken.

![schema](schema.png)

{{<alert type="info">}}
Nietegenstaande we zouden kunnen kiezen voor identieke subnets in beide tenants doen we dat hier niet. Dat betekent dat volgende netwerken moeten gebruikt worden:  
* **Tenant 1:**
  * Web => 192.168.10.0/24
  * Db  => 192.168.20.0/24
* **Tenant 2:**
  * Web => 192.168.30.0/24
  * Db  => 192.168.40.0/24 
{{</alert>}}


### Vereisten Provider

We lijsten hieronder enkele vereisten op die voor het providergedeelte van belang zijn:

* Als provider voorzie je een centrale router die voor "North-South" communicatie verantwoordelijk is en dus alle verkeer van en naar je datacenter regelt. Hiervoor mag je **de reeds bestaande T0-gateway gebruiken** uit het eerste hoofdstuk.

* Deze router maakt gebruik van NAT om alle verkeer van de tenants (alle interne verkeer) naar buiten mogelijk te maken en waar nodig ook extern verkeer van het internet door te geven naar de tenants.
  * *Alle* uitgaande NAT verkeer, van beide tenants, gaat over het primaire ip-adres naar buiten (het eerste adres van je adres pool in vlan 27 dat nog beschikbaar is)
  * inkomend verkeer dat van de publieke kant verder zijn weg moet vinden naar de diensten in elke tenant maakt gebruik van secundaire ip-adressen (de volgende ip adressen uit je pool van vlan 27)

### Vereisten Tenant

{{<alert type="danger" text="warning">}}
Vanaf hier werkt elke teamgenoot het scenario apart uit. Zorg er voor dat de naamgeving van je resources duidelijk aangeeft van welke tenant ze zijn en plaats deze resources ook in de juiste `resource groups` en `folders`. 
{{</alert>}}

We lijsten ook hier enkele vereisten op voor het tenantgedeelte:

* Als klant die van de provider een tenant huurt, voorzie je een router (T1) die het verkeer tussen intern en de buitenwereld regelt.
  * Deze router zal ook als DHCP server fungeren voor het netwerk "Web" en "DB".

* Zorg ervoor dat net zoals in het voorbeeld dat eerder geconfigureerd werd dat de `segments` geadverteerd worden naat de T0-gateway.

### Vereisten **Services** in de Tenant



Elke tenant (en dus student) zal voorzien in een monitoringoplossing voor de vsphere-omgeving. Die monitoring moet naar de buitenwereld (=odisee netwerk) bereikbaar gemaakt worden.
Als eindresultaat zal je twee dashboards opzetten waarbij je live de load op je vSphere omgeving kan zien.

![backend](applicationbackend.png)

Om bovenstaande te realiseren worden vier services gebruikt:

**Telegraf**: deze service zal data adhv input plugins verzamelen en met output plugins wegschrijven. Wij gebruiken vcenter monitoring als input en influxdb als output

**Influxdb**: deze databank heeft een heel specifieke functie: het bewaren van time series. Wij gebruiken het om de monitoring parameters van vcenter te bewaren

**Grafana**: met deze visualisatietool kan je heel snel dashboarden maken voor monitoring. Wij gebruiken het om de verzamelde data in influxdb te visualiseren.

**Cronograf**: Alternatieve visualisatietool, specifiek ontwikkeld voor influxdb. 

{{<alert type="danger" text="warning">}}
Telegraph en InfluxDB kunnen best op 1 vm samen geconfigureerd worden om resources te besparen.
{{</alert>}}

Voor onze eindgebruikers zijn Grafana en Cronograf extern (via VPN) te bereiken:

* Grafana => **grafana_voornaam.teamxx.labci.eu**
* Cronograf => **cronograf_voornaam.teamxx.labci.eu* *

{{<alert type="info">}}
**Beide URN's moeten verwijzen naar hetzelfde ip-adres!!!** (uiteraard voor elke student een ander ip-adres)
Via Reverse Proxy mbv Load Balancing (zie verder) zal het verkeer dan bij de juiste server terecht komen.
{{</alert>}}

Jullie hoeven hiervoor zelf geen DNS-server voor resolving op te zetten. Je kan de benodigde records configureren in een gemeenschappelijk toegankelijke DNS-server. 
Op deze DNS-server kan je dan de nodige records toevoegen zodat bovenstaande URN's resolven naar een ip-adres op de "publieke" kant van je vsphere-omgeving.

{{<alert type="note">}}
Deze ip-adressen zullen uiteraard niet bruikbaar zijn om vanop het publieke internet te surfen naar jullie resources (de ip-adressen uit vlan 27 komen uit een private range). Met jullie VPN (Bij voorkeur Cisco Anyconnect) actief zou dit echter wel moeten werken. 
{{</alert>}}

De gegevens van de DNS-server, waar de DNS-zone voor "labci.eu geconfigureerd staat,  kan je bereiken via:

* RDP op **10.129.36.193**
* gebruikersnaam: .\labci
* wachtwoord: OdiseeGent!

{{<alert type="danger" text="warning">}}
Gedraag je op deze DNS-service als een 'goeie huisvader'...
{{</alert>}}


#### Opzetten van de servers

Hou bij het opzetten van de servers rekening met de beperkte resources op jullie servers. Kies daarom 2GB RAM en CentOS mini als OS.
**Zet Grafana, Telegraf & Cronograf op als container**. Voor influxdb hoef je dat niet te doen, deze service kan je met de package manager installeren.

Om de statusdata te halen uit vSphere zal je credentials moeten aanmaken in vCenter. Doe dat volgens de regels van de kunst: maak een account met net genoeg rechten om dit te doen.

#### Reverse proxy - Load Balancer

Om onze diensten beschikbaar te maken naar externe gebruikers zullen jullie moeten een reverse proxy functionaliteit opzetten. We hebben immers meerdere sites op hetzelfde domein die naar een verschillend ip moeten doorgestuurd worden. Deze dienst wordt ook aangeleverd door een NSX-component, nl de `Load Balancer`.

{{<alert type="tip">}}
Gebruik het scenario van een `Inline Load Balancer` die toegevoegd wordt aan de T1 gateway.  
Om de load balancer te laten verwijzen naar verschillende servers bij het ontvangen van verschillende URL's zal je moeten gebruik maken van `Load Balancer Rules` op de `Virtual Server`. Je zal meerbepaald de `HTTP Request Header` moeten bekijken bij de `Request forwarding` om op basis van de URL (host) die meegegeven werd het verkeer door te sturen naar de juiste `Server Pool`.
{{</alert>}}

 

#### Firewall

Beveilig je omgeving. Deze beveiliging realiseer je niet met de ingebouwde firewall-functionaliteit van het besturingssysteem maar met de *Distributed Firewall", een onderdeel van NSX.

Enkele regels waar zeker moet aan voldaan worden:

* De influx-DB mag enkel connecties ontvangen van de grafana/chronograph services. Dit realiseer je met de "distributed firewall" functie van NSX (en dus niet met de klassieke firewall regels in de Edge of DLR)
* de toegang tot het 192.168.(20,40).0/24 netwerk moet beperkt worden tot het strikt noodzakelijke.
* beveilig waar nodig je setup verder. (verantwoorden wat je al dan niet nog extra zal beveiligen)

{{<alert type="tip">}}
Je kan best even de documentatie er op nalezen hoe je precies een dergelijke distributed firewall (East-West en North-South) kan configureren.
{{</alert>}}