---
title : "Modus Operandi"
draft: false
weight: 80
---

## Voorbereiding

Labo Private Cloud moet volledig afgewerkt zijn.
Dit betekent dat ook het onderdeel waarbij de NSX-T appliance geïnstalleerd werd en waarbij de NSX-T basiscomponenten geconfigureerd werden (T1 en T0 gateways,...), moet functioneren.
Uiteraard verwachten we dat de leerstof over netwerk virtualisatie doorgenomen werd vooraleer aan dit project te beginnen.


## Leerdoelen

In deze opgave is het de bedoeling om zelfstandig een complexere toepassing uit te werken, gebaseerd op de basisfunctionaliteiten die in eerdere labo's uitgevoerd werden.
Via deze opgave is het de bedoeling om leerstof uit verschillende vakgebieden (netwerken , security, virtualisatie, Linux, containers, DNS,...) met elkaar te integreren in een scenario dat aanleunt bij de realiteit.

## Opgave 

De opgave wordt initieel gestart in een team van 2 studenten. 
Na het configureren van de basiscomponenten (Provider) werkt elke student individueel verder om een oplossing uit te werken.


## Evaluatie

De evaluatie zal gebeuren in Week 7.
Een concretere planning volgt nog via het planningsdocument ( en eventueel via Toledo).

