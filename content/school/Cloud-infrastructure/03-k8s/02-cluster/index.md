---
title : "Aanmaken cluster"
page_title: Aanmaken van de Kubernetes cluster
draft: false
weight: 73
---

Om de kosten onder controle te houden, gebruiken jullie zo meteen slechts één enkele pre-emptible node met beperkte resources. Dat is prima om te leren werken met de features van K8S, maar uiteraard geen configuratie die je kan inzetten in een productieomgeving. 

De aanmaak van de cluster kan gebeuren via de cloud console (grafisch), via de cloud shell, de command line (gcloud), maar uiteraard ook via Terraform, zoals jullie eerder al zagen.
Voor dit labo kan gebruik gemaakt worden van de cloud console. Op die manier krijg je een duidelijk zicht op de mogelijkheden bij het aanmaken van de cluster.

Sommige van de settings die je moet doen, kunnen niet meer gewijzigd worden tijdens volledige levensduur van de cluster. Vooral in productieomgevingen kan dat erg lastig zijn. Een foute instelling kan dan een gedwongen migratie tot gevolg hebben... Voor jullie betekent het goed opletten, maar de potentiële gevolgen zijn uiteraard minimaal.

Specifieke keuzes die nodig zijn bij het aanmaken van de cluster:
* Activeren van de Kubernetes Engine API
* We kiezen voor GKE standard. Op die manier hebben we de mogelijkheid om zelf de cluster te beheren. Met autopilot is dat niet zo, dan gebruik je kubernetes als een managed service, en betaal je per container.
* Cluster basics:
  * Plaats de **zonale** cluster in europe-west1-d (=Charleroi). Bemerk dat je bij een zonale cluster ook het datacenter moet kiezen. 
  * Gebruik het "regular" release channel. Je control plane wordt dan steeds voorzien van een recente maar wel stabiele versie van k8s. (1.20.10 op het moment van schrijven)
* Node pool:
  * 1 node volstaat voor ons. Dat is uiteraard om de kosten onder controle te houden..
  * gebruik een E2-node (*e2-standard-2*)
  * maak de machines **pre-emptible**
  * Autoscaling activeren we (nog) **niet**
* Cluster networking:
  * We gebruiken een public cluster. Op die manier zijn je nodes NIET afgescheiden van het internet. In productie-omgevingen doe je dat beter wel...
  * Enable dataplane v2. 
* cluster security:
  * Enable workload identity

Het aanmaken van de cluster kan enkele minuten duren...
Het resultaat zou er ongeveer zo moeten uitzien:

![resultaat](clusteraftercreation.png)

{{<alert type="caution">}}
Google Cloud paste vorig jaar z'n pricing Kubernetes aan. Vroeger betaalde je enkel voor de gebruikte nodes, maar niet voor de master(s). Dat is nu enkel nog zo bij de eerste zonale cluster die je aanmaakt. Vanaf de tweede cluster wordt dit wel aangerekend (0.1€/uur). Bij regionale clusters betaal je sowieso voor de master. 
Wil je de prijzen vergelijken van verschillende aanbieders, dan vertrek je best van een heel concrete case. 
Zo rekent AWS bijvoorbeeld een vaste clusterkost aan (identiek aan GKE), en wordt bij Azure geen kost aangerekend voor een cluster, maar betaal je wel extra als je een bepaalde SLA wil halen op de cluster.

Let er dus tijdens dit labo op om zeker te werken met een zonale cluster...
{{</alert>}}

**Vraag**: wat zijn preemptible virtual machines? Welk risico brengen die met zich mee?
````

````

**Vraag**: wat is het verschil tussen een zonale en regionale cluster? Wat is het minimum aantal nodes bij beide?
````

````

{{<alert type="tip">}}
voor productieomgevingen is een regionale cluster een veel beter idee, omdat nodes dan verspreid zijn over meerdere datacenters. Omdat de kost dan snel oploopt (management-node niet langer gratis, minstens 3 nodes nodig) is dat voor deze oefening niet opportuun.
{{</alert>}}

### Verbinden met de Kubernetes cluster

Het beheren van een Kubernetes cluster kan op velerlei manieren, maar vroeg of laat zal toch `kubectl` nodig zijn, de command line tool.

Om die te gebruiken op je eigen computer moet je respectievelijk

* de `gcloud` tool installeren, een onderdeel van de [Cloud SDK](https://cloud.google.com/sdk/docs/install)
* gcloud laten verbinden met jouw project
* de credentials aanvragen om aan de slag te gaan met kubectl

```bash
# authenticeren met jouw project:
gcloud init

# verbinding maken met je project. 
gcloud container clusters get-credentials ***cluster-name**** --zone ******** --project ****project-id****
```

Test als je config goed werkt door in je console op je pc onderstaand commando te testen. Je zou moeten zien dat er één node actief is..


```bash
kubectl get nodes
```
