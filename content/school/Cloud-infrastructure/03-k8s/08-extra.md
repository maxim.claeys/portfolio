---
title : "Extra"
page_title:  Extra
draft: false
weight: 79
---


Er zijn een aantal uitbreidingen te maken op de voorgaande oefeningen. Eén ervan volstaat om de punten hiervoor te behalen:

* Pas de ingress van de hello-ghent app aan zodat die automatisch een google certificaat krijgt. Je kan hiervoor onze labci-zone gebruiken op Cloudflare

* Verzamel de Qwiklabs badge: "Kubernetes solutions"  (https://www.qwiklabs.com/quests/45)

* Pas de wordpress blog aan zodat een google cloud instantie (cloud sql) gebruikt wordt ipv een docker container. Gebruik hierbij de mysql proxy container.