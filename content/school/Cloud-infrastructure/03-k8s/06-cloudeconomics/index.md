---
title : "Cloud economics"
draft: false
weight: 77
---

Koken kost uiteraard geld, en als je later projecten zal uitvoeren, is één van de onvermijdelijke vragen wat de kostprijs zal zijn van je opstelling.

Nu jullie een werkende setup gemaakt hebben, is het goed om even stil te staan bij het mogelijke kostenplaatje van zo'n setup.

Daarbij ook volgende opdracht: maak je eens een benadering van de kostprijs van onderstaande setup? Je mag een cloudprovider kiezen om deze raming te maken... (gcp/aws/azure/...)


## Omschrijving

Een klant vraagt een Kubernetes oplossing met drie nodes met telkens een ssd disk van 100GB. Met twee cpu's per server en 8GB ram hopen we genoeg te hebben. 
Uit bespreking blijkt dat naar maximale uptime gestreeft wordt door de nodes in verschillende datacenters te plaatsen, en in totaal zullen er 100 microservices actief zijn. De totale hoeveelheid persistent storage zal 200GB zijn, allemaal SSD.

Daarbij is er aanvullend ook een managed databank (dus geen microservice) nodig (compatibel met mysql) met een SLA van de cloud provider. De load op de databank wordt minimaal ingeschat, de kleinste instance moet volstaan.

We gaan ervan uit dat we één http(s) load balancer nodig hebben. We schatten zo'n 100GB per maand verkeer te hebben, waarvan de helft uitgaand.

Het project loopt één jaar. 


Wat zal de kostprijs zijn schat je? Gebruik zeker de price calculator van de verschillende cloud providers...

Noteer je inschatting, kostprijzen voor de verschillende componenten én bedenkingen (misschien ontbreekt nog info voor een goede inschatting) en bezorg ons deze via https://forms.gle/eeqGfnqPCuagG3jC8
We maken een overzicht van jullie inschattingen die achteraf tijdens het labo zullen besproken worden.


```


```