---
title : "Wordpress"
page_title: Aanmaken van de Wordpress microservice
draft: false
weight: 76
mermaid: true
---

Na het deployen van Mysql bouwen we de microservice voor Wordpress. 
Hiervoor ga je zelf aan de slag. Je zal opnieuw een deployment, pv-claim en een service moeten aanmaken. 

Vooral de deployment zal er uiteraard anders uitzien, en de officiële documentatie kan hierbij zeker helpen, maar let op: gewoon overnemen zal niet lukken... 

{{<alert type="tip">}}
Vergeet niet om bijvoorbeeld alles in de juiste namespace te plaatsen.
{{</alert>}}


## Basiscomponenten

Maak dus volgende componenten aan:
* **persistent storage claim**: deze is heel erg gelijkaardig aan wat we net deden met Mysql. Het mountpath kan je instellen als `/var/www/html`. Op die manier zullen wijzigingen aan je site ook na het herstarten van je container (vb installatie theme, uploads, ...) blijven bestaan
* **een deployment**: kies voor de laatste versie van Wordpress. Je vindt de officiële container op Docker Hub.
  * Er zijn enkele omgevingsvariabelen nodig om je container goed te starten. Je kan hiervoor je secret hergebruiken die aangemaakt werd bij de databank. Daarnaast is er ook nog een specifieke extra config nodig voor Wordpress om te vermijden dat die een redirect zal veroorzaken. Onderstaande kan je op weg helpen:

        ```
        - name: WORDPRESS_DB_PASSWORD
          value: ##########
        - name: WORDPRESS_DB_USER
          value: ############
        - name: WORDPRESS_DB_NAME
          value: ###########
        - name: WORDPRESS_DB_HOST
          value: ##########
        - name: WORDPRESS_CONFIG_EXTRA
          value: "define('WP_HOME', 'http://###########');  define('WP_SITEURL', 'http://#########');"
        ```
  * de `WP_HOME` en `WP_SITEURL` zijn nodig om overbodige redirects weg te halen. Je kan er het ip-adres van de load-balancer later in plaatsen eens je dat kent. (in 'echte' omgevingen zal je dat ip-adres op voorhand reeds reserveren, configureren en koppelen aan DNS...)

* **een service**. We kiezen hierbij voor het type loadbalancer. Daardoor is die service ook buiten de cluster bereikbaar. Door dit te doen zal op de achtergrond een tcp-load balancer aangemaakt worden, die je blog ook voor de buitenwereld beschikbaar maakt.

{{<alert type="tip">}}
Een service maken van het type LoadBalancer is zeker niet de beste manier om je service extern bereikbaar te maken, maar wel de makkelijkste. In productie-omgevingen gebruik je hiervoor nog een extra ingress-object. 
Met die component zijn er veel meer mogelijkheden, maar dat zou vermoedelijk ten koste gaan van de leesbaarheid van deze opdracht, dus laten we dat even voor wat het is.
{{</alert>}}

## Port forwarding

In de console kan je ervoor kien om vanop jouw pc een portforwarding te activeren. Zo wordt een beveiligde tunnel gemaakt waarbij je toegang hebt tot interne resources op je cluster zonder dat die al extern bereikbaar zijn.
Voer deze port forwarding uit. Als alles goed is, kan je nu lokaal surfen naar localhost:8080
![port forward](port-forward.png)

Kan je verklaren hoe dit werkt?


## Configureer Wordpress

Als de port forwarding gelukt is, dan krijg je het configuratiescherm te zien.

Bij het databanktype kies je uiteraard mysql, en maak de koppeling met je mysql-container. Je zal ook nog enkele andere variabelen moeten invullen.

Een schematisch overzicht van hoe de verschillende componenten nu aan elkaar hangen:

{{< mermaid class="bg-light text-center">}}
  graph TD;
  User==>LB;
  LB[Load Balancer]==>SW[Wordpress Service - nodeport];
  SW==>PW[Wordpress deployment/pod];
  PW-->STW[Wordpress persistent storage];
  PW-->CM
  PW==>SM[Mysql service];
  SM==>PM[Mysql deployment/pod];
  PM-->STM[Mysql persistent storage];
  PM-->CM[Secret Configuration];        
{{< /mermaid >}}

Het resultaat:

![wordpress Blog](wordpress.png)


