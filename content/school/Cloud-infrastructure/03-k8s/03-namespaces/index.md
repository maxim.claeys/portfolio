---
title : "Namespaces"
page_title: Gebruik van namespaces
draft: false
menu:
  docs:
    parent: "k8s"
weight: 74
---

Een namespace kan je zien als een tenant binnen je cluster.
We maken gebruik van de namespace 'mijnblog' om onze wordpress blog te hosten. Door meerdere namespaces parallel naast elkaar te gebruiken kan je meerdere projecten of klanten een plaats geven op je cluster zonder risico op (naamgevings-)conflicten. 

{{<alert type="caution">}}
Je mag dit niet zien als een security-boundary: diensten kunnen default wel aan diensten in andere namespaces! Wil je dat toch beperken, dan is het mogelijk om network policies op te zetten. Deze fungeren dan als firewall-regels tussen pods onderling.
{{</alert>}}

Er zijn ook steeds enkele systeem-namespaces aanwezig waarin de componenten zitten die kubernetes tot een werkbaar geheel maken. Door in de console de default filter te verwijderen bij de 'workloads' krijg je deze te zien. 
Meestal kan je deze namespaces negeren, al moet je er rekening mee houden bij je scaling dat ze ook resources gebruiken. 
Af en toe kan de logging van deze services je ook helpen bij troubleshooting.

Fans van de command line kunnen uiteraard ook `kubectl get pods --namespace kube-system` uitvoeren.

![System namespaces](system-namespaces.png)

Maak deze namespace aan:

```kubectl
kubectl create namespace mijnblog
```

{{<alert type="caution">}}
Vergeet niet om bij elk commando dat je vanaf nu gebruikt de namespace te vermelden. Als je dat niet doet, komen resources terecht in de 'default' namespace. 

```
kubectl get pods --namespace mijnblog
```
{{</alert>}}