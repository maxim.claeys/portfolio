---
title : "Modus Operandi"
draft: false
weight: 72
---

## Voorbereiding

### Basiskennis Kubernetes

Voor deze opgave werken we met Google Cloud. 

Ter voorbereiding van het labo vragen we om minstens drie labs te doorlopen op Qwiklabs die deel uitmaken van de "Kubernetes in Google Cloud"-quest: (https://google.qwiklabs.com/quests/29)

* Kubernetes Engine: Qwik start
* Orchestrating the cloud with Kubernetes
* Managing deployments using Kubernetes Engine.

Vrijblijvend kan je ook de andere labs afwerken. Indien de volledige quest werd afgewerkt, ontvang je een digitale badge die je bijvoorbeeld op je LinkedIn profiel of curriculum vitae kan vermelden.

Deze labs hebben telkens een tijdslimit, dus plan deze zorgvuldig in. Vaak is deze tijdslimiet erg ruim, wat je de mogelijkheid geeft om ook wat te experimenteren buiten de opgave.

De benodigde credits (€€€) om deze labs te doorlopen worden aangeboden via de hogeschool (als je aanmeldt met je mailadres @student.odisee.be). +
Mocht je merken dat dat voor jouw account niet zo is, neem dan zeker contact op met je docenten...

{{<alert type="tip">}}
Open de labs steeds in een privaat browservenster... Zeker als je veel labs gaat doorlopen is dat de beste manier van werken.
{{</alert>}}

### Toegang tot Google Cloud

Als student krijgen jullie een budget van $50 per persoon om deze opgave op te zetten op Google Cloud. Op Toledo vind je de link om die credits aan te vragen.

{{<alert type="caution">}}
50$ is ruim voldoende om deze opgave af te werken, maar waak er zeker over dat je je cluster steeds naar nul terugschaalt nadat je er op werkt. Een cluster die meerdere weken actief staat zal je budget snel tot nul herleiden.  
{{</alert>}}


## Leerdoelen


* Begrip van het concept microservices
* Werken met software containers en orchestration
* De verschillende componenten binnen het Kubernetes platform kunnen in hun context plaatsen
* configuratie maken en deployen op kubernetes

## Opgave 

{{<alert type="info">}}
Deze opgave wordt individueel afgewerkt... 
{{</alert>}}

Tijdens het eerste deel van deze opgave gaan jullie Wordpress opzetten in Kubernetes. We gebruiken hiervoor het Google Cloud platform, al kan dat met relatief weinig wijzigingen ook gebeuren op Microsoft Azure of Amazon AWS vanwege het open karakter van dit platform.
In het tweede stuk zetten we een zeer minimale applicatie online, waarmee we features als autoscaling en rolling updates zullen demonstreren.

## Evaluatie

Bij de verdediging van deze opdracht zal de score opgesplitst worden:

* De praktische implementatie (50%)
* De theoretische achtergrondkennis (30%)
* Uitvoering van extra functionaliteit (20%)

Zorg er dus zeker voor dat je op het moment van de verdediging de theorie nog grondig herhaalde.