---
title : "Installatie vCenter"
draft: false
weight: 63
---

## Deploy/installatie van vCenter: STAGE1

Hoewel de installatie van ESXi (of een andere hypervisor) op zich al volstaat om virtuele machines aan te maken en te hosten, zal je merken dat het toevoegen van een managementlaag grote voordelen biedt. De managementlaag bij VMware vSphere heet vCenter..

De installatie van vCenter gebeurt dmv een iso waarop een installer staat.  
Je kan ook deze iso downloaden vanop de fileserver op je lokale pc vooraleer je verder gaat.

{{<alert type="caution">}}
Als je deze opgave via vpn uitvoert, doe dan zeker de installatie van vCenter vanop een virtuele machine op de campus. Anders zal je de 8GB van de installer moeten uploaden vanop jouw thuisnetwerk, en dat zou wel eens een tijdje kunnen duren...
{{</alert>}}

locatie iso: [download](https://file.ikdoeict.be/Public/1%20ISO%20&%20Software_Installs/ISO/VMWare/vCenter/)

Mount deze ISO op je computer, en start de installer. Je vindt deze op
F:\\vcsa-ui-installer\\win32, waarbij F:\\ uiteraard de disk is waar je de ISO op gemount hebt.

**Opmerkingen tijdens de installatiewizard**:

-   Je zal een ESXi host moeten opgeven waarop je vCenter wenst te installeren.
  
-   Kies een wachtwoord voor je vCenter Server Applicance ( "root-account" => goed
    bewaren!)

-   Kies voor tiny install

-   Bij de storage kan je momenteel niet anders dan de lokale storage
    selecteren.

-   Kies voor **thin disk mode** install (Zoek ook op wat dit betekent)
    ```
    Thin Disk mode betekenis:
    ```
    
-   Network settings:

    -   fqdn laat je leeg

    -   het ip-adres dat je gebruikt is het eerste adres uit je adrespool van 12 adressen in vlan 27.

    -   DNS servers kan je invullen als 10.129.28.230, 10.129.28.232

![vCenter installer phase1](vCenter_installer_phase1.png)

De installatie van de vcenter appliance zelf ("Stage 1") kan makkelijk zo’n 10 minuten duren, afhankelijk van de snelheid van je server en netwerk. Daarna zal je nog enkele instellingen moeten wijzigen (Stage 2), gevolgd door nogmaals een tiental minuten wachten. **Plan dit dus goed in**…

### Installatie tweede host

terwijl vCenter aan het installeren is, kan je de tweede ESXi-installatie doorlopen. Daarvoor volg je de dezelfde werkwijze volgen als bij de eerste server. 

{{<alert type="tip">}}
Volg de identieke stappen zoals bij je eerste server. Vergeet ook niet de vlan voor het "VM Network" aan te passen van vlan 0 naar **vlan 27**!
{{</alert>}}

## Deploy/installatie van vCenter: STAGE 2


Opmerkingen bij Stage 2 van de installatiewizard:

-   Stel ook hier ntp.belnet.be in als NTP-server

-   SSH mag uitgeschakeld worden. In praktijk zal je dat enkel bij troubleshooting activeren.

-   Voor het SSO-gedeelte: SSO staat voor single sign on. Mocht we in een later stadium andere vmware producten gebruiken, zullen die allemaal deze zelfde authenticatiecomponent gebruiken.

    -   Kies als domein "vsphere.local"

    -   Kies een wachtwoord (goed bewaren!)

Ook deze installatie duurt zo'n 15 min…​

![vCenter installer phase2](fase2_completed.jpg)


### Connecteren met vCenter

Eens vCenter goed geïnstalleerd is, zijn er verschillende mogelijkheden om deze te bereiken.

-   **Appliance management**: basissettings van de appliance kan je hier aanpassen, zoals bijvoorbeeld het ip-adres of bijvoorbeeld het herstarten van de appliance. Je vindt deze (web)interface op poort **5480**. Hier zal je dus niet vaak gebruik van maken. Je kan er geen beheer van je ESXi servers en cluster mee uitvoeren. Te vinden op: [https://&lt;ip-adres&gt;:5480](https://<ip-adres>:5480)

-   **vSphere client (html5)**: deze webinterface kan je bereiken met een webbrowser, is snel en heeft geen plugins nodig. Je vindt deze client op
    [https://&lt;ip-adres-van-vCenter&gt;](https://<ip-adres-van-vCenter>).

### Initiële configuratiestappen in de vCenter client:

-   Connecteer met vCenter via de vSphere Client (html5)

-   Maak een nieuw **datacenter-object** aan. Je zal merken dat servers, virtuele machines en andere componenten steeds hiërarchisch georganiseerd zijn zoals je dat ook kent uit bijvoorbeeld Active directory. Een 'datacenter'-object is  hiervan het bovenste element. Als naam kies je “CloudInfra”.

-   Maak binnen dit datacenter een **cluster-object** aan. (naam: CLUSTER1) Een cluster kan je beschouwen als een logische verzameling servers en virtuele machines, die samen beheerd worden, en die intelligent op elkaar kunnen reageren. De resources van deze servers worden dan samen als één pool beschouwd. Features als DRS, HA en VSAN laten we voorlopig uitgeschakeld.

-   Eenmaal aangemaakt vind je binnen het clusterobject in het menu "Configure", de **Quickstart**. Hiermee kan je op een snelle manier je configuratie doen van enkele basissettings in je cluster.

    ![quickstart](quickstart.jpg)

Verdere configuratie van deze Quickstart behandelen we in het volgende hoofdstuk.
