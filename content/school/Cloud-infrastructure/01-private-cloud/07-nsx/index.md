---
title : "Installatie nsx"
page_title: "Network virtualisation: NSX-T installatie"
draft: false
weight: 67
---

## Situering

De opgave die tot dusver werd gemaakt, komt overeen met de installatie in de meeste enterprise-omgevingen. 
Stilaan is er wel een trend om nog een stukje verder te gaan. Daarbij wordt ook de netwerkinfrastructuur verder gevirtualiseerd.
Traditioneel zou je verschillende netwerken in je private cloud kunnen scheiden a.d.h.v. **Vlans**. Dit is echter geen flexibele noch schaalbare oplossing aangezien vlans op laag 2 werken en er maar een *beperkt aantal Vlan ID's* beschikbaar zijn. Dit laatste vergt ook dat je fysieke switches telkens voorzien moeten worden van de juiste vlans.

Via **netwerkvirtualisatie** kunnen we over bv. verschillende datacenters heen (Laag 3) volledig van elkaar geïsoleerde netwerken creëren.
Op die manier kan bijvoorbeeld een cloud provider een pak virtuele netwerken aanbieden aan zijn klanten en hoeft hij niet wakker te liggen van overlappingen in ip-adressen, tekort aan vlans, bereikbaarheid van VM's die in verschillende datacenters staan, etc...

Afhankelijk van het product dat gekozen werd voor de netwerkvirtualisatie kan daarenboven een uitbreiding gemaakt worden met **virtuele routers, switches, firewalls**.

In dit stuk van de opgave gaan we werken met NSX, de netwerkvirtualisatiecomponent van VMWARE voor zijn vSphere platform.

## Scenario

Om het concept van netwerkvirtualisatie duidelijk te maken zullen we een concreet, min of meer, realistisch scenario uitwerken. In het volgende hoofdstuk gaan we dus verder dan enkel de netwervitualisatie in installeeren en configureren. We gaan er ook een toepassing op bouwen.  
Het scenario dat we bouwen in deze opgave is een omgeving waarbij we virtuele cloudomgevingen kunnen doorverkopen aan klanten (**tenants**). We zorgen er daarbij voor dat elke klant over dezelfde ip-ranges kan beschikken en connectiviteit met de buitenwereld kan krijgen.  
Vanuit jullie netwerkachtergrond zal het duidelijk zijn dat daar extra componenten voor nodig zijn.

Ter herinnering voegen we wel nog eens de tabel toe met IP-adressen in je pool. Hou deze bij de hand om overlap te vermijden en vul dit stelselmatig aan


| Locatie in pool | IP adres     | Functie         |
| --------------- | ------------ | --------------- |
| IP 1            | 10.129.27.__ | vCenter server  |
| IP 2            | 10.129.27.__ | *NSX-T manager* |
| IP 3            | 10.129.27.__ |                 |
| IP 4            | 10.129.27.__ |                 |
| IP 5            | 10.129.27.__ |                 |
| IP 6            | 10.129.27.__ |
| IP 7            | 10.129.27.__ |
| IP 8            | 10.129.27.__ |
| IP 9            | 10.129.27.__ |
| IP 10           | 10.129.27.__ |
| IP 11           | 10.129.27.__ |
| IP 12           | 10.129.27.__ |


## Installatie NSX-T

Om NSX werkend te krijgen zijn een aantal componenten nodig.

* Er is een `manager` nodig om het geheel te beheren. 
* Deze manager stuurt (minstens) volgende elementen aan:
  * Transport nodes (de ESXi hosts waarbij TEP's gemaakt worden)
  * Edge Transport Nodes (deze zorgen voor een pool met virtuele switches, routers, load balancers,...)

{{<alert type="info">}}
Om de verschillende tunnels (TEP's), waarover het verkeer tussen de hosts, netwerken, datacenters,... loopt, mogelijk te maken, zullen nog enkele pools met ip-adressen moeten voorzien worden.  
Elke tunnel interface zal dan een ip uit deze pool kunnen krijgen. Deze pools worden ook gemaakt met ip-adressen uit bovenstaand lijstje.
{{</alert>}}

Naast de installatie van de NSX-T manager moeten de hosts (ESXi hosts en eventuele bare metal hosts) dus voorbereid worden om met deze componenten te kunnen werken (host preparation). 

Eenmaal dit klaar staat, kunnen componenten als logische switches, routers, firewalls, load balancers etc toegevoegd worden.

### Installatie NSX manager

Allereerst moet een NSX-T appliance geïnstalleerd worden die zal fungeren als `manager`.

* Kopieer de recentste versie van de appliance (*.ova) naar je lokale computer. Omwille van de grootte van het bestand is het sterk afgeraden om dit over vpn te doen...
Deze is te vinden op https://file.ikdoeict.be/Public/1%20ISO%20&%20Software_Installs/ISO/VMWare/nsx/NSX-T
* Deploy deze appliance op je vCenter omgeving.

  ![netwerk](nsx-t_install_01.png)
  ![netwerk](nsx-t_install_02.png)


* Kies de kleinste installatie-methode (**'extra-small'**)

  ![netwerk](nsx-t_install_03.png)

  {{<alert type="danger" text="warning">}}
  Let er op dat je "Thin Provisioned" storage kiest! Anders zal je iSCSI storage meteen gevuld zijn...
  {{</alert>}}

  ![netwerk](nsx-t_install_04.png)

* Kies voor het netwerk van deze appliance voor je VM-netwerk (**DPG_VM** -> vlan 27).

  ![netwerk](nsx-t_install_05.png)
 
* Bij "**Customize Template - Application**" vul je enkel de verplichte velden in (wachtwoorden voor `system root`, `admin` en `audit` user.)
  
  ![netwerk](nsx-t_install_06.png)

* Bij "**Customize Template - Network Properties**" geef je de netwerk settings mee:
  * Hostname: "NSX-Manager-TeamXX"
  * Geef je NSX-manager *het tweede ip-adres* uit je toegewezen range in vlan 27 (zie tabel bovenaan).
  * Verder geef je ook de DNS-servers mee (10.129.28.230, 10.129.28.232) en de NTP-server ntp.belnet.be

  ![netwerk](nsx-t_install_07.png)


Eenmaal de appliance geïnstalleerd werd zou je daar moeten kunnen naartoe surfen om de verdere configuratie uit te voeren.

{{<alert type="note">}}
Het kan best zijn dat het surfen naar je NSX-T appliance in het begin vrij traag verloopt. In de achtergrond gebeuren al verschillende zaken waardoor deze machine nog vrij actief is.  
Verder kozen we ook voor een appliance met minimale resources aangezien we ook maar servers hebben met beperkte mogelijkheden.  
Als dit onhoudbaar blijkt, sluit dan je NSX-T manager "netjes" af en verhoog het maximale geheugen van deze manchine naar 16GB ipv 8GB.
{{</alert>}}
