---
title : "ESXI"
page_title: Opzetten basis virtualisatie-infrastructuur
draft: false
weight: 62
---

## Adrestabel

Op Toledo vind je een tabel ("CI Verdeling servers en ip’s" ⇒
<https://docs.google.com/spreadsheets/d/14V_UU6eMyHQwDR_r5b4y5C5JEsOs9lBXPBA5LdjOZv4/edit#gid=0>
met de toewijzing van de verschillende servers en LUN’s per groepje.

{{<alert type="tip">}}
Vul eerst, op basis van het overzicht in de spreadsheet,
onderstaande tabel aan…​
{{</alert>}}

<details style="contain:content;font-size:80%;">
    <summary>Tabel met IP-adressen en netwerken</summary>

|         | Storage network                          | Vmotion network  | VM network (Guests)              | ILO/iDRAC     | Host network (Hypervisors) |
| ------- | ---------------------------------------- | ---------------- | -------------------------------- | ------------- | -------------------------- |
|         | vlan 410                                 | vlan 800         | vlan 27                          | vlan 16       | vlan 21                    |
| SAN     | 100.100.100.101/24 en 100.100.100.102/24 | -                | -                                | -             | -                          |
| SERVER1 | 100.100.100.../24                        | 172.16.254.../24 | -                                | 10.129.16.2.. | 10.129.21.1../24           |
| SERVER2 | 100.100.100.../24                        | 172.16.254.../24 | -                                | 10.129.16.2.. | 10.129.21.1../24           |
| vCenter | -                                        | -                | 10.129.27.1.. - 10.129.27.1../24 | -             | -                          |

DNS-servers: 10.129.28.232 en 10.129.28.230

</details>



## Netwerkschema

Elke server heeft 3 of meer netwerkkaarten. Kies bij voorkeur de
netwerkkaarten die "on-board" zitten.


![Netwerkschema](schema_server_en_netwerk.jpg)

Op de poorten van de switch waarmee de servers verbonden zijn, zijn
verschillende VLANS aanwezig via trunks. Afhankelijk van de fysieke
poort op de servers of virtuele switches en VM’s zullen er andere VLANS
van toepassing zijn.

## Installatie hypervisor op SERVER1

### Fysieke bekabeling

Moderne features van hypervisors vereisen het gebruik van minstens gigabit-verbindingen. Sommige features, zoals vSan zullen zelfs aanbevelen om minstens 10Gb te gebruiken. Voor dit labo gebruiken we gigabit-switches.

Elke server zal verbonden worden via 3 poorten:

-   een eerste poort (vb nic1) zal later gebruikt worden om verkeer van
    virtuele machines over de transporteren. (*)
   

-   een tweede poort (vb nic2) gebruiken we voor het storage-verkeer. (*)
    
    {{<alert type="info">}}
Je zou er ook voor kunnen kiezen om beide poorten samen als één team met trunks te configureren. Het storage- en virtuele machine verkeer zou dan samen over beide poorten verlopen. 
    {{</alert>}}
    
-   de derde poort (iDRAC) dient om idrac aan te sluiten. Zo kunnen we
    de server beheren, zoals je verder kan lezen.

-   werk zo ordelijk mogelijk: de bekabeling zal het hele semester
    gebruikt worden.

    {{<alert type="tip">}}
Kies één kleur van bekabeling per team!
    {{</alert>}}


**Werk de bekabeling af van de eerste server**, vooraleer je verder gaat.
Strikt genomen heb je met enkel de 'derde' poort (iDRAC) genoeg voor de eerste
stappen (zie hieronder) van de installatie.

### Connecteren met iDrac

De Dell servers zijn voorzien van een speciale netwerkkaart die toelaat
om het scherm remote te bekijken. Deze interface heet **iDrac**.

Zoek het iDRAC-adres op van je server, door er naar te surfen met een
browser krijg je een overzicht van de hardware-parameters van de server.
Daarnaast kan je ook een **virtual console** oproepen, waarmee je een
virtueel scherm koppelt aan de server.  
Daardoor kan je vanop afstand beheersacties uitvoeren, en zelfs tot op
bios-niveau aanpassingen doen.

Bemerking bij het overnemen van een scherm via iDRAC:

-   Bij het overnemen van het scherm is de mapping van het keyboard
    mogelijks fout: de letters die je intikt komen niet overeen met wat
    effectief op de server ingegeven wordt. Dat is vooral gevaarlijk bij
    het ingeven van een wachtwoord. We raden je daarom aan om een
    eenvoudig wachtwoord te kiezen bij de installatie van ESXi, **en dat
    uitgebreid te testen via de console** (Alt-F1). 

-   Toegang tot de console verloopt via je browser. Zorg ervoor dat
    steeds de laatste versie van de idrac firmware geïnstalleerd staat
    op je servers. **Installeer dus zeker de laatste versie van de iDRAC
    firmware**. Je vindt deze op de fileserver (file.ikdoeict.be → iso &
    software installs → Software installs → iDRAC Firmware) Concreet
    moet het versienummer hoger zijn dan 2.60.) Google Chrome werd
    gebruikt bij het testen van dit labo.

{{<alert type="info" text="Inloggegevens Dell iDrac:">}}
-   username: root
-   password: Azerty123
{{</alert>}}


### Controleren van de lokale storage

In elk van jullie servers zitten een of meerdere schijfjes. Bekijk in je
iDRAC omgeving hoeveel schijven er aanwezig zijn. Als er 2 of meer
gelijkaardige schijven aanwezig zijn dan kunnen die best in RAID
geconfigureerd worden om dataverlies te voorkomen in het geval 1 schijf
faalt. Controleer daarom in de diskcontroller als de configuratie
toelaat om een RAID configuratie in te stellen. Pas dit eventueel aan
(mocht dit mogelijk zijn en nog niet het geval is)…​ Indien er maar 1 schijf aanwezig is, dan is bovenstaande uiteraard niet van toepassing.

### Deploy van de eerste hypervisor

#### Installatie van de eerste hypervisor

Voor de bouw van onze private cloud zullen we twee fysieke servers
gebruiken. **Initieel gaan we één server installeren**. Later zullen we de tweede server toevoegen.

Volg daarvoor volgende stappen:

-   Download de iso van de hypervisor. Deze kan je gratis downloaden op
    de site van VMWARE, wij kiezen ervoor om een aangepaste versie te
    gebruiken die de drivers van DELL reeds bevat. Je vindt de bestanden op de
    fileserver: [download](https://file.ikdoeict.be/Public/1%20ISO%20&%20Software_Installs/ISO/VMWare/ESXi/).
    Kies zeker voor de laatste nieuwe versie.

-   Verkrijg toegang via iDRAC tot de virtuele console (scherm) van je
    server, en koppel de net gedownloade ISO aan de VM. Zorg ervoor dat
    de virtuele DVD zal gebruikt worden bij een volgende boot.  
    Reboot indien nodig je server zodat je de installatie kan starten
    vanop de virtuele DVD. Opgelet: zorg dat de mount ook nog na het rebooten aanwezig is!

-   Volg de installatie nauwgezet, lees alle eventuele waarschuwingen in
    detail, let in het bijzonder op de toetsenbordinstellingen.

    Tips:

    -   Zie je bij het starten van de installer geen disks staan om op te installeren, dan probeer je best eerst nog even je server te herstarten. De oorzaak is voorlopig onbekend. Mogelijks moet er een firmware update gebeuren (vraag meer info aan je docent).

    -   Gebruik SPACE in de text console om iets te selecteren. (met ENTER bevestig je waardoor je naar een volgende pagina gaat zonder iets aan de selectie te veranderen.)
    {{<alert type="caution">}}
Let er op dat de [X] effectief bij de juiste optie staat. De optie selecteren is niet voldoende en zorgt er voor dat je installatie niet zal slagen.
    {{</alert>}}


    ![hostadded](new-install.png)

   -   Kies voor een nieuwe installatie, en overschrijf eventueel
        bestaande vmfs datastores. Zo vermijd je dat je per ongeluk een upgrade doet van een vorige installatie.

    
![hostadded](start-installer.png)

{{<alert type="danger" text="opgelet">}}
Wees erg aandachtig bij de keyboardinstellingen en de keuze van het root-wachtwoord. Test het wachtwoord bijvoorbeeld eerst in een ander tekstveld. Je kan dat bijvoorbeeld doen door in de installer Alt-F1 in 
te drukken. Je komt dan in een console terecht waar je het wachtwoord kan testen. (Met ALT-F2 kan je terug naar de installer) Als je wachtwoord verkeerd ingesteld wordt, zal er niks anders opzitten dan de hypervisor opnieuw te installeren…​  
{{</alert>}}


#### Basisconfiguratie van de eerste hypervisor

Als de installatie van de ESXi hypervisor voltooid is, kan je deze met
twee interfaces bedienen:

##### *de DCUI interface*
(direct console user interface) deze interface is erg basis en is wat je
zou zien op het fysieke scherm. In principe zal je deze interface enkel
gebruiken voor initiële basisconfig (vb ip-adres instellen), of voor
troubleshooting.

![dcui](dcui.jpg)


##### *de ESXi Embedded Host Client*  
eens de hypervisor bereikbaar is over het netwerk, kan je deze ook
bereiken via de browser over https (<https://ip_van_ESXi_server/> ).
Daar heb je een pak meer configuratieopties.

![embedded host](embeddedhostclient.jpg)


-   Configureer eerst via de DCUI - nog steeds via iDRAC console - het IP-adres
    van de server op NIC0. Stel de netwerkconfiguratie verder correct
    in (gateway, dns-servers, hostname*[kolom F in tabel]*). Let vooral op het VLAN nummer, dat moet op vlan **21** staan…​

-   Voer zeker ook de management netwerktest uit in deze DCUI vooraleer
    verder te gaan (zeker de gateway en dns-server moeten positief zijn).

Vanaf hier kan je in principe alles verder configureren via de
webinterface ( ESXi embedded client).

-   Navigeer via deze webinterface
    (<https://ip-adres-van-hypervisor>) naar **host  > manage > system >
    time & data**, configureer de NTP server op **ntp.belnet.be**.
    Zorg er voor dat de service start en stopt bij het opstarten van de
    host. Start daarna eenmalig manueel deze service. (bij 'services')

{{<alert type="caution">}}
Het instellen van NTP servers wordt vaak over het hoofd gezien, met soms verregaande gevolgen. Protocollen die gebruikt worden voor authenticatie en clusterconfiguratie (zoals Kerberos) zijn immers vaak afhankelijk van synchronisatie voor een goede werking. Als de systeemklokken van je servers na verloop van tijd meer en meer afwijken kunnen moeilijk traceerbare problemen opduiken. Daarom: stel steeds overal NTP-servers in.
{{</alert>}}


-   Stel bij ** host > manage > system > Advanced settings** de
    Annotations.Welcomemessage in op "TEAMXX naam1 naam2"

-   Ga naar ** host > manage > Security & Users > Users** en stel het root-wachtwoord opnieuw in indien nodig.

{{<alert type="caution">}}
Verlies dit wachtwoord niet! Bewaar het op een veilige plaats (zoals een wachtwoordbeheerder) en zorg dat ook je teamgenoot dat doet als backup (vb in geval van ziekte, afwezigheid)
{{</alert>}}

De virtuele machines die op jullie ESXi infrastructuur zullen draaien worden in een ander netwerk geplaatst. We realiseren die via aparte Vlans. Op die manier zijn de hypervisors gescheiden van de virtuele machines.

-   Stel bij het menu networking het netwerk voor VM’s in zodat daar de juiste vlan (=27) aan gekoppeld zal worden. Het resultaat zou er zo moeten uitzien:

![esxi networking](esxi-networking.png)

