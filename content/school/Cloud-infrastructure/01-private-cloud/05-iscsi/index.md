---
title : "ISCSI"
page_title: Configuratie van iSCSI
draft: false
weight: 65
---

In deze fase van de setup zijn enkel de lokale hdd's op de servers beschikbaar als storage.
Dat is helemaal niet interessant aangezien dit geen "shared storage" is die door beide hosts kan gebruikt worden. 
Daar brengen we verandering in, door een iscsi-SAN te koppelen aan beide servers.

## Configuratie van het netwerk


*   Maak op de distributed switch een **nieuwe distributed portgroup** aan met de naam **`DPG_ISCSI1`** (vlan 410) en een tweede distributed portgroup met de naam **`DPG_ISCSI2`** (ook vlan 410). 
    Andere settings mogen hun default waarde behouden. 

*   Ga naar **je eerste server** en navigeer naar **configure > networking > VMkernel adapters > Add Networking**.  
    Op deze manier kunnen we een VMkernel adapter toevoegen aan de port group **`DPG_ISCSI1`**.   
    *   Je kan daar kiezen voor `Select an existing network`, omdat je de portgroup al eerder aanmaakte.
{{<alert type="info">}}
Een kernel adapter is een speciaal type adapter die gebruikt wordt om achtergrondservices te connecteren met het netwerk.  
Denk daarbij aan iSCSI, maar later ook vmotion, vSAN ed..    
De vmkernel adapter die we hier aanmaken is nodig zodat onze ESXi hosts zullen kunnen communiceren met de iSCSI SAN storage (Qnap TES-3085U).
{{</alert>}}
    *   Je hoeft verder geen services te enablen.  
    *   Geef een static IP-adres in. Dit is het adres dat later zal communiceren met de SAN, dus dit moet in dezelfde range als de SAN liggen.
        **Raadpleeg het overzichtsdocument om de adressen voor jouw opstelling te kennen. (100.100.100.x/24)**. 

        ![netwerk](vmkernel_iscsi.png)

*  Maak op een zelfde manier een 2de vmkernel adapter aan die gekoppeld zal worden aan **`DPG_ISCSI2`**. 

![netwerk](iscsi_kernel_adapters.png)

*   Deze procedure **herhaal** je ook **op je tweede server**. Ook daar moeten dus twee kernel-poortjes aangemaakt worden..

{{<alert type="caution">}}
Ga niet verder met de opgave voordat je zeker bent dat deze stappen succesvol werden doorlopen.  
Vraag bij twijfel een docent om een controle...
{{</alert>}}

*   Aangezien je server maar 2 fysieke netwerkkaarten heeft hoeft de distributed switch ook maar **2 uplinks** te hebben (DVUplinks).  
    Standaard kreeg je Distributed Switch 4 uplinks toegewezen.  
    Pas dit aan bij de eigenschappen van je distributed switch ("Edit Settings" > "Uplinks")
    
    ![netwerk](dswitch_uplinks.png)

{{<alert type="caution">}}
 Omdat iSCSI best redundant kan werken moeten voor **Multipathing** de fysieke NIC's apart gehouden worden. De ene distributed portgroup maakt dus uitsluitend gebruik van **vmnic0** en de andere uitsluitend van **vmnic1**.  
 Editeer hiervoor de **teaming and failover** setting bij de distributed portgroup waarbij je één NIC op "**Active**" plaatst en de andere op "**Unused**". Doe deze aanpassing uiteraard bij beide distributed portgroups.
{{</alert>}}
![configuratie iscsi kernel poorten](teamingandfailover.png)





Waarom is een default gateway niet relevant voor een iscsi connectie?


```

```

## Toevoegen en configuratie van de iscsi-adapter

* Om de configuratie bij iedereen identiek te maken, kiezen we ervoor om een software iSCSI adapter te gebruiken om te connecteren met de storage, ook al is (misschien) een hardware adapter aanwezig in je server. 
Die laatste zou vermoedelijk iets betere performantie opleveren (minder CPU-gebruik), maar dat is in onze oefening sowieso verwaarloosbaar.
* Maak, op beide servers, een software iscsi adapter aan bij **host > configure > storage > storage adapters**
* Eens toegevoegd moet je deze configureren:
    * De iSCSI software adapter is een virtueel device. Je moet dus bij de eigenschappen van deze adapter bij `network port binding` een 'echte' portgroup toevoegen zodat de virtual iSCSI adapter in het juiste netwerk zit (vlan). Uiteraard zijn dat de beide iscsi portgroups die we daarvoor eerder aanmaakten.  
    Als je de portgroups niet (beide) kan selecteren, controleer je best nog eens de failover policy (zie hierboven)
    * Voeg een target toe bij `dynamic discovery`, en vul het adres in van de storage SAN: 100.100.100.101
    * Configureer unidirectional CHAP bij de properties > authentication van je software adapter. (initiator user en wachtwoord): niet iedereen mag immers zomaar op de storage... Username is "chap" en wachtwoord is "Azerty12345678"
    ![netwerk](iscsi_config.png)
* Een rescan zal daarna nodig zijn om de storage zichtbaar te maken. Als dit goed lukte, zou je je iets dergelijks moeten zien met een grootte van ca 400GB:

![configuratie iscsi kernel poorten](iscsidevice.png)

* Als je iscsi-device zichtbaar is, ga je naar datastores.
* Als daar al een iscsi-datastore zichtbaar is, dan is dat vermoedelijk een overblijfsel van vorige studenten. Verwijder deze ("delete datastore") dan ook eerst.
* Voeg een datastore (naam=**dsISCSI_teamxx**) toe die voor jouw teamnummer voorzien is. Dat betekent een LUN van 400GB. Je kiest voor VMFS6; kies ervoor om de schijf volledig te gebruiken, en bekijk de andere instellingen aandachtig.

![add storage](addstorage.png)

* Is bovenstaande correct? Dan is je storage helemaal klaar voor gebruik!
* Voeg deze datastore ook toe aan je tweede host!

![datastore](datastore.png)

Wat is VMFS? Welke alternatieven zijn er om je virtuele machines te plaatsen en op welke manier zijn ze structureel verschillend?


```

```

Controleer je opstelling. Beide hosts zouden op dit punt volledig moeten werken met identieke netwerkconfiguratie en allebei hebben ze toegang tot de iscsi storage...

## Verplaatsen van vCenter naar shared storage

Nu jullie shared iscsi storage toegevoegd hebben, kan vCenter verplaatst worden naar die storage. ("storage migration")  
Doe dit ook, zodat je niet langer afhankelijk bent van de enkele lokale en trage serverdisk waarop die momenteel op actief is...
Bovendien is dit ook noodzakelijk als je jouw virtuele machines een hogere beschikbaarheid wil geven (High Availability - HA).