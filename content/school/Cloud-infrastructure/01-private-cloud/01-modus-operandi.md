---
title : "Modus Operandi"
draft: false
weight: 61
---

## Voorbereiding

Voor je aan de opgave begint, bekijk je best alle stappen die zullen
moeten doorlopen worden. Zo krijg je een beeld van het uiteindelijk
doel, en dat kan je helpen als onderweg keuzes moeten gemaakt worden.

## Leerdoelen


-   Begrip van het concept virtualisatie en private cloud

-   Implementatie van een gevirtualiseerd datacenter

-   Begrip van technieken voor high availability

-   Begrip van software defined networking (adhv NSX)

-   Begrip van software defined storage (adhv vSAN)

-   Implementatie van een representatieve serverinfrastructuur volgens
    de geldende regels van de kunst

## Achtergrond


In deze opgave zullen jullie de infrastructuur opzetten van een klein of
middelgroot bedrijf. We zullen daarbij gebruik maken van een private
cloud, die we uiteindelijk ook zullen koppelen aan diensten die zich in
de public cloud bevinden.

## Apparatuur & veiligheid


Voor dit labo zal je initieel per team twee fysieke servers nodig hebben
in het datacenter.  
Je krijgt hiervoor toegang tot het datacenter. Daarvoor zijn er enkele
strikte regels van toepassing:

-   Aanpassingen zijn enkel toegestaan in de studentenracks (R4 & R5)

-   Indien het brandalarm afgaat, moet het lokaal **onmiddellijk**
    verlaten worden. Na 30 seconden wordt immers automatisch een blusgas
    in het lokaal gelaten dat het zuurstofpeil drastisch zal verlagen.
    Zowel de knal waarmee dit gebeurt als het resulterend zuurstofpeil
    zijn een gevaar voor iedereen die zich in het datacenter bevindt.

-   Het datacenter mag niet onbeheerd achter gelaten worden. Vraag
    steeds een docent om de deur te sluiten als fysieke toegang niet
    langer nodig is.

-   Werk steeds ordelijk in het datacenter. Docenten kunnen (.. en
    zullen !) zonder
    waarschuwing slordige bekabeling verwijderen.

{{<alert type="caution">}}

Ga enkel verder met het labo als bovenstaande veiligheidswaarschuwingen duidelijk zijn. Vraag meer uitleg aan je docent indien nodig.

{{</alert>}}

## Evaluatie

De evaluatie van deze grote opgave gebeurt in drie fases. 

### Week 1

Op het einde van de eerste les moet de vCenter server geïnstalleerd staan en beide fysieke server opgenomen in de cluster.

### Week 4

In week 4 zal de algemene werking gecontroleerd worden van vSphere. Zaken die op dit moment moeten werken:

- ESXi installatie op beide servers volledig afgewerkt
- Netwerken en iSCSI storage geconfigureerd op de beide servers
- DRS, HA en VMOTION werkt en kan gedemonstreerd worden.
- NSX-T controller actief

De verdediging van bovenstaande items gebeurt per projectgroep.
In week 7 wordt het vervolg van dit labo (NSX-T configuratie basiscomponenten) mee geëvalueerd.
Dit valt samen met de verdediging van het "project virtualisatie".
