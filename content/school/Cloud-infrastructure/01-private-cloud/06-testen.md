---
title : "Testen"
page_title: Testen van de basissetup
draft: false
weight: 66
---


Vooraleer we verder gaan met het installeren van NSX, is het belangrijk dat we de huidige setup uitgebreid testen.


* **Vmotion**: maak een kleine virtuele machine aan, en plaats deze op je shared storage. Start deze op, en migreer die live naar de andere server. 
* **DRS**: als de load op één server te hoog wordt, moet DRS automatisch een vMotion uitvoeren om load te herverdelen. Bedenk zelf een scenario om dit te testen.


Pas als bovenstaande testen succesvol bleken, kan je doorgaan naar de volgende stap in de opgave: de installatie van NSX-T