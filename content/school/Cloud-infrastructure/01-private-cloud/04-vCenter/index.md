---
title : "vCenter: Quickstart"
page_title: "Configuratie vCenter: Quickstart"
draft: false
weight: 64
---

Vooraleer verder te gaan is het belangrijk goed te snappen hoe de verschillende netwerken (vlans) geconfigureerd en verbonden zijn met je server.
Een schema om deze fysieke verbindingen te verduidelijken vind je hieronder:

![fysieke aansluiting](schema_server_en_netwerk.jpg)

De figuur hierboven toont de fysieke netwerken. Maar virtuele machines zijn verbonden met virtuele switches. 
Er bestaan twee types virtuele switches: **standard** en **distributed switches**.  
Wat zijn de grote verschillen tussen beide en welke features worden enkel bij distributed switches ondersteund?
```

```

Als we op dit moment een schema van je netwerk zouden maken dan ziet het er als volgt uit:

![momentopname netwerk](schema_standaard_switch_1srv.jpg)


{{<alert type="info">}}
Bemerk dus dat er momenteel gebruik gemaakt wordt van een standaard switch (op beide hosts) waarbij telkens 2 Port Groups gemaakt werden. Een Port Group in vlan 21 voor Management Network en een Port Group in vlan 27 voor VM Network.  
Verder is deze standaard switch voorlopig verbonden met één fysieke NIC (vmnic0), de groene lijn in de figuur. vCenter maakt gebruik van een trunk naar de fysieke switch om het verkeer van de verschillende vlans mogelijk te maken, de rode lijn in de figuur.
{{</alert>}}

Voor onze opstelling gaan we de virtuele machines, storage en andere diensten verbinden met een **distributed switch**.

Bekijk onderstaand schema's voor je verder gaat met configuratie... Dit zal de situatie worden na de Quickstart config.

![netwerk](schema_standaard_en_distributed_switch_1srv.png)

*schema distributed switch: (na afloop van de configuratie, zie verder)*

## Quickstart configuratie

Doorloop (zeer aandachtig) de quickstart wizard die je terug kan vinden bij je cluster settings.
Die wizard neemt erg veel zorgen weg bij de configuratie. Je kan immers ook alles manueel configureren, maar het risico op fouten is dan een pak groter. 
Met deze wizard worden tal van controles uitgevoerd die fouten minder waarschijnlijk maken. Ook wordt de overdracht van Portgroups op de standaard switch automatisch overgezet naar een distributed switch.
Bij deze **Quickstart** moet je **3 stappen** doorlopen.

{{<alert type="caution">}}
De quickstart wizard kan je maar één keer uitvoeren. (daarna moet config manueel gebeuren). Controleer dus zeer goed de settings die je ingeeft...
{{</alert>}}

### Quickstart Stap 1:

- Activeer HA en DRS in de wizard, VSAN configureren we niet.
  ![momentopname netwerk](quickstart_step1.jpg)



### Quickstart Stap 2:

- Voeg beide hosts toe.  
  Wellicht krijg je een warning over het feit dat er VM's actief zijn op een bepaalde host. Dit mag je uiteraard negeren want het gaat over vcenter zelf.  
  In de boomstructuur verschijnen beide servers dan onder je cluster. Een server zal automatisch in "Maintenance Mode" geplaatst zijn. De andere, waar vCenter op draait, functioneert gewoon.
  ![structuur hosts](boomstructuur_hosts.jpg)

  {{<alert type="tip">}}
Merk de extra VM op met de naam "vCLS-xxxx". Dit is een extra VM die sinds vcenter 7.0 toegevoegd wordt om cluster services zoals DRS en HA beter te laten functioneren. Je kan deze VM's niet verwijderen of aanpassen.
  {{</alert>}}
  

### Quickstart Stap 3:

- Maak met de wizard 
  - één distributed switch aan, **waar alle fysieke adapters aan gekoppeld zijn**. (naam: **DISTR-SWITCH**)
  - we zorgen er voor dat er ook onmiddellijk een portgroup met de naam **DPG_VMOTION** aangemaakt wordt voor vmotion verkeer.
  ![Quickstart](quickstart_dist_switch.jpg)

  - Voor het vMotion verkeer moet de portgroup de **vlan 800** gebruiken.
  - en verder configureren we een kerneladapter voor vMotion met een statisch ip-adres.  
    Kijk voor het ip-adres van beide servers in je overzichtstabel.

  ![Quickstart vMotion](quickstart_vmotion.jpg)

  - de "advanced options" mogen op hun standaard waarde blijven.

- Als de wizard doorlopen is zal je wellicht onderstaand resultaat verkrijgen.
  ![Quickstart vMotion](quickstart_result.jpg)

### Post Quickstart

Zoals eerder aangegeven heeft Quickstart veel zaken automatisch voor ons geconfigureerd. We willen dit graag wat nog  wat aanpassen en opkuisen.

- Hernoem de PortGroup voor Management (vlan 21, waar de ESXi hosts zitten) naar **DPG_HOSTS**.
- Hernoem de PortGroup voor Virtuele Machines (vlan 27) naar **DPG_VM**.
- Als er een extra Portgroup werd gecreëerd voor VM's in vlan 27 (DISTR-SWITCH-VM Network-ephemeral), verplaats dan de vmnic van deze VM naar de andere PortGroup DPG_VM en **verwijder deze extra Portgroup**.
  
  {{<alert type="tip">}}
Een VM in een ander netwerk plaatsen kan je bij de settings van die VM aanpassen, ook als die VM actief is. Let wel op dat je hier geen fouten maakt, anders wordt je vCenter onbereikbaar.
  {{</alert>}}
- Verwijder ook de standaard switch op beide hosts samen met hun bijhorende portgroups.

Na het doorlopen van de quickstart wizard verifieer je de configuratie van de netwerken op je verschillende servers. 
Verifieer als
- de vlans overal correct staan ingesteld
- de ip-adressen van kernelpoorten correct zijn.  
  Dit kan je verifiëren bij elke host individueel bij **Configure &gt; Networking &gt; VMKernel adapters**
- de namen van de distributed portgroups gelijk zijn aan deze die hierboven werden vermeld
- overtollige componenten (vb lege standard switch) verwijderd zijn

{{<alert type="info">}}
Als je nog aanpassingen moet doen, dan kan dat gebeuren op clusterniveau als het om de distributed switch gaat (vb toevoegen/hernoemen van distributed portgroups), of op hostniveau als het gaat om aanpassingen die bij de host horen (vb toevoegen kernel adapter aan distributed port group, ...)
{{</alert>}}

Het resultaat zou moeten aansluiten bij onderstaande screenshot:

![Quickstart result](afterquickstartbeforeiscsi.png)