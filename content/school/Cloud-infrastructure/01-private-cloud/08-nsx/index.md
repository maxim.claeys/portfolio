---
title : "Configuratie nsx"
page_title: "NSX-T configuratie basiscomponenten"
draft: false
weight: 68
---

Vooraleer we NSX-T echt kunnen inzetten voor wat het bedoeld is (nl netwerken virtualiseren waardoor bv. Layer 2 communicatie over netwerken, vlans, datacenter,... heen mogelijk wordt of distributed firewalling) moeten nog heel wat onderdelen geconfigureerd worden.  
Enerzijds zullen de ESXi nodes moeten bewust gemaakt worden van het feit dat ze getunneld NSX-verkeer zullen moeten transportern en anderzijds zullen we componenten moeten toevoegen zodat East/West en North/South communicatie (inclusief Load balancers, Security profiles, firewalls, virtual routers,...) mogelijk gemaakt kan worden.

## Toevoegen license key vCenter en NSX-T

Via https://vmware.ikdoeict.be kan je een licentie key verkrijgen voor NSX-T en vCenter licentie.  
Voeg beiden toe aan je NSX-T installatie.

## Upgrade Distributed Switch

Eén van de nieuwigheden in de nieuwste versies van NSX-T is dat er nu gebruik kan gemaakt worden van de reeds aanwezige Distributed Switches (vDS) in je vSphere cluster. Voorheen moest nog gebruik gemaakt worden van een speciale, aparte  switch N-VDS. Deze N-VDS switch moet nog steeds gebruikt worden als de NSX-T manager ook andere hypervisors dan VMWare (vb. KVM) of Bare Metal Servers moet aansturen of ook in de Edge Nodes (zie verder). In deze situatie (vanaf NSX-T 3.1) kan de vDS gebruikt worden. Het is echter wel belangrijk dat je distributed switch met de juiste versie werkt (by default 6.6.0).
* Doe een **upgrade** van je distributed switch naar versie 7.0.3 
   ![netwerk](nsx-t_install_08.png)

## Configuratie NSX-T basis

{{<alert type="tip">}}
Om jullie wat extra houvast te geven is het goed om een voorbeeld setup er bij te halen waarbij de verschillende stappen aan bod komen.  
**Let wel**: interpreteer goed de situatie die daar beschreven staat want **deze is niet 100% dezelfde als onze setup**.  
NSX-T is een vrij complex product. Probeer dus heel goed bij de acties die je uitvoert goed te begrijpen.

https://significant-bit.com/2020/10/19/getting-started-with-nsx-t-part-1/ en verder...
{{</alert>}}

### Overzichtstabel ip-adressen

Vul hieronder het overzicht van je verschillende netwerken aan op basis van het overzicht op https://docs.google.com/spreadsheets/d/14V_UU6eMyHQwDR_r5b4y5C5JEsOs9lBXPBA5LdjOZv4/edit#gid=0.  
De nieuwe vlans kies je uit de range van beschikbare vlans in de kolom L van dat document.

| Netwerk                | VLAN         | Functie                                                                        |
| ---------------------- | ------------ | ------------------------------------------------------------------------------ |
| 10.129.16.0/24         | vlan 16      | iLO/iDRAC                                                                      |
| 10.129.21.0/24         | vlan21       | ESXi hosts                                                                     |
| 10.129.27.0/24         | vlan 27      | Virtual Machines! (**en North/South uplink verkeer**)                          |
| 100.100.100.0/24       | vlan 410     | Storage Netwerk                                                                |
| 172.16.254.0/24        | vlan 800     | VMotion Netwerk                                                                |
| 172.16.**TEAMNR**.0/25 | **vlan ...** | **TEP's encaps. traffic** (voor transport nodes en Edge Node)                  |
| 10.0.0.0/16            | /            | Gebruikt voor verkeer in de tenants (**GENEVE verkeer**) voor deze handleiding |
| 192.**TEAMNR**.0.0/16  | /            | Gebruikt voor verkeer in de tenants (**GENEVE verkeer**) voor project          |

### Toevoegen Compute Manager

Zoals ook aangegeven in de tutorial kan je na het inloggen in je NSX-T omgeving controleren of de "appliance" goed functioneert. Je krijgt op die plaats onmiddellijk een `warning` dat er een `compute manager` noodzakelijk is. Een `compute manager` is een instantie die een groep van resources beheert en aanstuurt. In onze situatie is dat vCenter maar aangezien  NSX-T veel producten ondersteunt kan dit ook iets anders zijn dan vCenter .

![netwerk](nsx-t_config_01.png)


Als resultaat zouden de ESXi hosts nu moeten gekend zijn in de NSX-T manager. Dit kan je checken bij de "Host Transport Nodes".

![netwerk](nsx-t_config_00.png)

### Configureren van een Transport Zone

De transport zone bepaalt over welke delen van je netwerk het NSX-T verkeer (geëncapsuleerde verkeer) mag lopen. Of m.a.w. welke `transport nodes` (hosts) mogen deelnemen aan het vervoer van dit gevirtualiseerde netwerkverkeer. Dit verkeer (van geïsoleerde netwerken met hun VM's) verloopt dus tussen de hosts (East/West verkeer). Dit verkeer, van verschillende geïsoleerde netwerken, scheiden van elkaar kan a.d.h.v. `VLANS` of `Overlay`. Vlans geeft ons geen meerwaarde en confronteert ons terug met de beperking dat er maar een beperkt aantal VLANs mogelijk zijn én dat de tussenliggende switches ook deze vlans moeten geconfigureerd hebben.  
Wij kiezen dus voor scheiding m.b.v. `Overlay`.

Overlay maakt dus gebruik van een encapsulatie protocol. `VXLAN` en `GENEVE` zijn de 2 bekendste open standaarden met elk hun voor- en nadelen.  
NSX-T maakt gebruik van `GENEVE`.

*   Maak een nieuwe `Transport Zone` aan met als naam "**nsx-overlay-transportzone-teamxx**".   
    Deze zone zal gebruikt worden voor de transport nodes en hun East/West verkeer.

![netwerk](nsx-t_config_02.png)

*   Maak een gelijkaardige `Transport zone` aan voor "uplink verkeer" (verkeer naar het echte fysieke netwerk en de buitenwereld). Dit uplink verkeer kan niet over een overlay netwerk gaan (het moet immers aan de buitenwereld geraken) en moet dus over een onderliggende VLAN lopen. 
    *   Gebruik hier de naam "**nsx-vlan-transportzone-teamxx**". Deze Edge uplinks zullen instaan voor het North/South verkeer.
    *   Kies voor een Traffic Type "VLAN"

*   Op dit worden deze Transport Zones nog nergens gebruikt. Later koppelen we dit aan "Transport Nodes" en "Edge nodes" waarmee aangegeven zal worden of hun verkeer via Overlay of VLANs moet lopen.

{{<alert type="info">}}
In een productie-omgeving zullen er aparte ESXi nodes voorzien worden die enkel voor `compute` dienen en andere ESXi nodes uitsluitend voor `management` (vCenter, NSX Manager, NSX-T Edge VM's,...). In dit geval zullen de twee transport zones dan heel duidelijk naar verschillende hosts refereren.  
In onze setup is dat wat anders. Aangezien zowel de workload (VM's) als alle Edge nodes (met T0 en T1 routers) op dezelfde hosts staan, zullen beide zones naar dezelfde hosts refereren.
{{</alert>}}

## Transport Nodes

Transport nodes zijn de hosts die verantwoordelijk zijn voor de workload communicatie. Deze hosts zullen NSX-T softwarecomponenten moeten geïnstalleerd krijgen (VIB's) zodat ze de nodige encapsulation tunnels met bijhorende TEP's (GENEVE) kunnen aanmaken. Op die manier kunnen de hosts onderling verkeer van verschillende geïsoleerde netwerken naar elkaar doorsturen eventueel over datacenters heen.

### IP Address Pools

Wat verder in deze opgave zullen `TEP's` gecreëerd worden. Deze tunnel eindpoints op de hosts zullen elk een ip-adres moeten krijgen. Om dit alles vlot en consistent te laten verlopen (ook als er later hosts zouden bijgevoegd worden) is het makkelijk om vooraf `IP Pools` te definiëren waardoor automatisch ip-adressen kunnen toegewezen worden aan deze TEP's.  

*   Voeg een nieuwe IP Pool toe voor je TEP's. Gebruik hiervoor de range uit je tabel hierboven (ip's van .1 t.e.m. .16).

    ![netwerk](nsx-t_config_03.png)
    ![netwerk](nsx-t_config_04.png)

### Profiles

Zoals daarnet een IP Pool werd aangemaakt om in de toekomst makkelijk en automatisch TEP's van een IP te voorzien zullen we  a.d.h.v. `profiles` een soort template maken die we kunnen toepassen op onze hosts en edges. Op die manier moeten we niet elke host (of straks Edges) apart configureren maar wordt telkens de template toegepast.

We zullen 2 profielen aanmaken.  
Eéntje voor het configureren van de transport nodes (hosts met hun TEP's)zelf. Dit wordt dan een `Transport Node Profile`.  
Daarnaast een profiel dat bepaalt hoe het verkeer van die transport nodes onderling verloopt `Uplink Profile`.


#### Uplink Profile

Dit profiel beschrijft dus hoe het geëncapsuleerde verkeer tussen de hosts (transport nodes) kan lopen. Als dit verkeer een host moet verlaten (naar een andere host) dan bepaal je hiermee over welke vlan dit geëncapsuleerde verkeer mag lopen. 
Verder bepaal je hiermee ook hoe de communicatie zelf verloopt vanaf de uplink poorten (vb bij meerdere poorten: load balanced of failover)

*   Maak een nieuw `Uplink Profile` aan.
    *   Geef dit profiel de naam "TransportNode-Uplink-Profile-Teamxx"
    *   We gebruiken geen LAG's (Link Aggregation) maar wel de standaard Teaming functionaliteit.  
        Pas de `teaming policy` aan zodat er "load balancing" gebruik wordt.
        Voorzie daarvoor 2 "uplinks" met de naam `up1`en `up2`. Dit zijn voorlopig ook "placeholders" die we later gaan koppelen aan echte uplinks.
    *   Stel je VLAN (zie tabel) in die voorzien werd voor TEP-verkeer.
    *   Aangezien het verkeer geëncapsuleerd wordt volstaat de standaard MTU van 1500 bytes niet. Laat het veld leeg om een MTU van 1600 bytes te gebruiken.

![netwerk](nsx-t_config_05.png)

#### Transport Node Profile

De transport nodes zelf (ESXi compute hosts) moeten een configuratie toegepast krijgen. En ook hier creëren we daarvoor een profiel.  
**In dit profiel komt eigenlijk alles samen.**  
De `vds` uit vcenter, de `transport zone`, het `uplink profiel` dat daarbij moet toegepast worden en de `ip pool` dei gebruikt mag worden voor de ip's van de TEP's.

*   Geef dit profiel de naam "TransportNode-Profile-Teamxx"
*   We kiezen voor onze bestaande vDS switch uit vCenter in de "standard" mode.
*   Aangezien deze hosts zowel TEP-verkeer als later ook verkeer naar buiten zullen leiden via Edges (en dus via onderliggende vlans), voegen we hier beide Transport Zones toe. Deze hosts zullen dus enerzijds "overlay" verkeer als anderzijds verkeer via "vlans" transporteren.  
    Zoals eerder gezegd zal dit in een productie-omgeving duidelijker gesplitst zijn van elkaar.
*   Selecteer je "Upload Profile"
*   Maak gebruik van je IP Pool.
*   Bij "Teaming Policy Uplink Mapping" koppelen we de placeholders van daarnet aan de "echte" uplinkpoorten van je vDS uit vCenter.

![netwerk](nsx-t_config_06.png)

{{<alert type="tip">}}
Mogelijks moet je de MTU van je vDS in vCenter nog verhogen. Aangezien we in het NSX-T profile bepaald hebben dat er een MTU van 1600 bytes gebruikt moet worden moet de onderliggende switch dat ook effectief aankunnen.
Dit kan je bij de settings van je vDS in vCenter.
{{</alert>}}

#### Toepassen configuratie op de echte ESXi nodes.

Waar voorgaande stappen vooral het aanmaken van templates en profielen betrof is het hier de bedoeling om deze nu toe te passen op de echte hosts.

*   Ga in je manager naar **"Nodes > Host Transport Nodes"** en zorg dat je je nodes ziet.
*   Selecteer je cluster en pas het "Transport Node Profile", dat je daarnet aangemaakt hebt, toe via "Configure NSX".

![netwerk](nsx-t_config_07.png)

In de achtergrond worden de ESXi hosts geconfigureerd. Als alles vlot verlopen is zou je dat zowel in de NSX-T manager moeten kunnen zien als in vCenter.

*   Controleer in NSX-T het resulaat.  
    Bemerk de tunnel end point IP adressen (TEP ip's) die uit je IP pool komen.
    ![netwerk](nsx-t_config_08.png)

*   Controleer in vCenter het resulaat. Daar zouden 2 op beide hosts nieuwe VMKernel apdapters moeten aangemaakt zijn.
    ![netwerk](nsx-t_config_09.png)

## Edge Nodes

Edge nodes (Edge VM's of bare metal servers) zullen instaan voor de nodige resources en netwerk connecties om North/South verkeer mogelijk te maken. Deze Edge nodes kunnen voorzien worden van de nodige virtuele routers (T0 en T1). Dus als verkeer van een VM "buiten" moet geraken, vb naar het internet, dan zal via een T0 en T1 gateway in een Edge node het geëncapsuleerde verkeer naar buiten gerouteerd kunnen worden. 

Voor de configuratie van deze Edge nodes zal op gelijkaardige manier gebruik gemaakt worden van `profiles`.

{{<alert type="tip" text="BELANGRIJK!">}} 
**Lees eerst** het stukje op https://significant-bit.com/2020/12/23/getting-started-with-nsx-t-components-intermezzo/ om een beter zicht te krijgen op de componenten die uiteindelijk het gevirtualiseerde verkeer zullen transporteren.
Dus na onze basisconfiguratie die we hier aan het opzetten zijn worden daarna eventuele `segments`, `Tier 0` en `Tier1`-gateways toegevoegd.  
Op die manier kunnen we effectief toepassingen gaan opzetten waarbij verkeer volledig geïsoleerd en gevirtualiseerd van elkaar moet lopen.
In het beschreven voorbeeld wordt er per tenant (vb klant) een eigen router voorzien, een Tier-1 Gateway. Deze T1-router per klant wordt dan via een Tier-0 Gateway verbonden met de buitenwereld.
{{</alert>}}

### Extra segment

De Edge nodes die we straks gaan deployen hebben ook hun TEP's nodig. Sinds NSX-T 3.1 is het mogelijk (als de Edge VM's ook op de transport nodes aanwezig zijn zoals in onze situatie) om deze TEP's in hetzelfde subnet te plaatsen met de Compute node TEP's. Dat kon bij vroegere versies niet.

Om bovenstaande mogelijk te maken moet er voor gezorgd worden dat de Edge Nodes geconnecteerd worden via een segment.  
Een segment is vergelijkbaar met een PortGroup op een logische switch in vCenter. Maar een segment is een met GENEVE geëncapsuleerd netwerk dat loopt over daarvoorgeconfigureerde vlan.  
In de toekomst gaan we VM's met hun netwerkkaart koppelen op een segment van NSX-T en niet meer met een PortGroup in vCenter.  
Het segment hier zal specifiek dienen om het uplink verkeer van de Edge node mogelijk te maken.


*   Maak een `segment` aan
    *   Gebruik de naam "edge-trunk-teamxx"
    *   Gebruik je aangemaakte "vlan transport zone" (nsx-vlan-transportzone-teamxx)
    *   Configueer bij "VLAN" de range "0-4094" waardoor dit segment alle VLAN's (voor zover toegelaten door de fysieke switches) zal bevatten.  
        De Edge node kan dan zelf kiezen welke Vlan precies nodig is om de uplink van een bepaalde T0 gateway te realiseren.
    
    ![netwerk](nsx-t_config_10.png)




### Uplink Profile

Ook voor de uplinks van de Edge nodes maken we een `uplink profile` aan.  
Eigenlijk gaat het hier over een uplink profile voor de virtuele poorten op de N-VDS switch in de Edge Node.
In dit scenario ziet het profiel er gelijkaardig uit als bij de transport nodes (identiek zelfs...).
Dat betekent dat er op de N-VDS van de Edge node 2 uplink poorten verbonden worden met de vNics van de Edge node om in load balancing mode het verkeer naar de vDS van je ESXi host brengen.

Onderstaand schema kan dit wat verduidelijken. Let hier enkel op de N-VDS switch met zijn 2 uplink poorten in de Edge node.  
Je ziet hier ook duidelijk dat de Edge Node 4 netwerkpoorten heeft waarvan er 2 gebruikt worden om de uplinks van de N-VDS te connecteren. De Edge Node (die in ons geval een VM is) wordt dan met zijn 4 poorten verbonden op de vDS op je ESXi hosts (vCenter).
![netwerk](nsx-t_config_13.png)

*   Maak een nieuw `Upload Profile` aan met de naam "EdgeNode-Uplink-Profile-teamxx".
    *   Pas de `teaming policy` aan zodat er "load balancing" gebruik wordt.
        Voorzie daarvoor 2 "uplinks" met de naam `up1`en `up2`. Dit zijn voorlopig ook "placeholders" die we later gaan koppelen aan echte uplinks.
    *   Stel je VLAN (zie tabel) in die voorzien werd voor TEP-verkeer voor Edges. Dat is hier identiek als de voor de transport nodes!!
    *   Laat het veld leeg om een MTU van 1600 bytes te gebruiken.

### Edges nodes deployen

{{<alert type="caution" text="Vooraf!">}} 
Creëer eerst een nieuwe IP-pool met de naam **EDGE-TEP-IP-Pool** uit je voorziene TEP vlan (range ip's .20 t.e.m. .35)  
Deze pool zit in hetzelfde subnet als de TEPs van je transport nodes. Maar op die manier zullen we mooi zien welke ip's op welke TEPs terechtkomen.
{{</alert>}}

Edge nodes zullen de resources voorzien om de nodige T0 en T1 gateways, met bijhorende services, te huisvesten. Deze edge nodes kunnen als VM gedeployed worden of op een aparte fysieke node (bare metal) als het zeer performant moet zijn.

*   Voeg een nieuwe `Edge node` toe. ("System > Fabric > Nodes > Edge Transport Nodes")
    *   Geef deze de naam "**nsx-edge-01**" en fqdn "nsx-edge-01.teamxx.ikdoeict.be"
    *   Small Deployment
    *   CLI user: admin / Azerty12345! (of aangepast en gedocumenteerd)
    *   SSH login toegelaten
    *   Root Credentials: Azerty12345! (of aangepast en gedocumenteerd)
    *   Deploy de Edge node op de vCenter cluster en de iSCSI storage
    *   Het Management IP voor deze Edge node is het eerstvolgende vrije ip uit je DPG_VM vlan 27.  
        Zie schema hierboven: we koppelen de eerste vNIC van deze Edge VM aan het Management netwerk. Langs deze weg kunnen we dan eventueel troubleshooten via SSH.
        DNS: 10.129.28.232 en 10.129.28.230, NTP: ntp.belnet.be
    *   Bij "Configure NSX":
        *   Edge Switch name: default naam behouden (dit wordt een N-VDS switch intern in de Edge VM. Hiervoor kan geen vDS gebruikt worden)
        *   Voeg beide transport zones toe. Dit betekent dat de Edge node zowel Overlay als VLAN netwerken zal kunnen gebruiken.
        *   Selecteer je upload profile voor de edges
        *   gebruik je recente IP Pool voor de edges
        *   Voor de Uplinks kies je het apart aangemaakte segement "edge-trunk-teamxx".
        ![netwerk](nsx-t_config_12.png)

Als alles goed verloopt zou je nu in vCenter de deployment moeten kunnen volgen van een nieuwe Edge VM.  
Controleer ook opnieuw in de manager of de **node status "up"** wordt en de **configuration state "Success"** (dit kan wel een poosje duren).
Ook hier zouden de TEP ip's moeten terug te vinden zijn.  
Als je even doorklikt kan je zelfs zien met welke andere TEP's ze tunnels gevormd hebben.

### Edge Cluster

Om je opstelling redundant te maken kan je in principe best meerdere Edge nodes configureren. De Edges kan je dan bundelen in een Edge cluster.  
Dit doen we hier voorlopig niet om wat resources te besparen... We maken echter wel de Edge Cluster aan, weliswaar met één Edge node.

*   Maak een nieuwe Edge Cluster aan met de naam "edgecluster01-teamxx"
*   Voeg je Edge node toe aan de cluster

## Tier 0 Gateway

Nu alle resources voor eventuele routing instances (T0 en T1) klaar staan, kunnen we die ook effectief uitrollen.
Een T0 gateway zal hier typisch zorgen voor North/South routering en dus uitgaand/inkomend verkeer regelen. Een T0 gateway kan ook met de fysieke switches en routers communiceren en bv OSPF- of BGP routes uitwisselen. Op die manier kunnen de interne netwerken op de hoogte gebracht worden van externe netwerken en omgkeerd. Indien nodig kan er ook via NAT naar de buitenwereld geconnecteerd worden.  

### Segment

Deze T0 gateway zal dus moeten verbonden worden met de "buitenwereld". Hiervoor moet je uiteraard bepalen met welk netwerk van de buitenwereld dit mag gebeuren.  
We creëren hiervoor een nieuw segment ( VLAN netwerk) met de gewenste VLAN.

{{<alert type="note">}}
Hier gebruiken we dus een segment in een bepaalde VLAN van het buitenwereld netwerk.  
Deze Vlan zal door de T0 gateway getagd worden in de Trunk verbinding die eerder op de Edge Node geconfigureerd werd (edge-trunk-teamxx).
{{</alert>}}
 
*   Maak een segment aan voor het `upstream verkeer` (verkeer tussen T0 en de buitenwereld)
    *   Gebruik de naam "upstream-segment"
    *   Gebruik je aangemaakte "vlan transport zone" (nsx-vlan-transportzone-teamxx)
    *   Configueer bij "VLAN" de vlan voor upstream North/South verkeer. Dit is hier vlan 27
    ![netwerk](nsx-t_config_11.png)

{{<alert type="info">}}
Wij kozen hier om de uplink in vlan 27 te plaatsen. Dit is de vlan die we eerder al gebruikten als Management Vlan voor oa. vCenter, de NSX-T manager, de Edge Node.  
In productie is dit uiteraard geen zo'n goed idee en zal je hier zeker een andere vlan voor kiezen. Nog beter is om dit redundant te voorzien en de T0 gateway bv 2 upstream vlans te geven zodat er ook redundant geconnecteerd kan worden met de buitenwereld (zie voorbeeld hieronder: vlan 160 en 170 voor BGP connectie met de buitenwereld).


{{</alert>}}
![netwerk](nsx-t_config_14.png)

### T0 gateway

Zoals eerder gezien zal de T0 gateway het North/South verkeer behandelen en dus het interne verkeer (het overlay netwerk) koppelen met de buitenwereld.

*   Creëer een nieuwe T0-gateway
    *    met de naam "t0-gw-01"
    *    Active/Standby Mode
    *    Verwijs naar je Edge cluster waar deze T0 mag terecht komen.
    *    Druk "Yes" om verder details te configureren.

Nu kunnen verschillende onderdelen en services verder geconfigureerd worden:

#### De interfaces

Deze virtuele router zal uiteraard geconfigureerde interfaces moeten bevatten om de connectie met de netwerken te kunnen maken.

*   Via het onderdeel "interfaces" voeg je een nieuwe interface toe:
    *   Geef die interface de naam "T0-edge01-int01".
    *   Geef deze interface een correct ip-adres uit de voorziene ip-range van de VLAN voor upstream verkeer (eerstvolgende vrije ip uit je vlan 27 range)
    *   Connecteer deze T0 met het upstream netwerk segment dat je eerder aangemaakt hebt (upstream-segment).

![netwerk](nsx-t_config_15.png)

{{<alert type="note">}}
Indien er voor high availability meerdere Edge Nodes uitgerold worden in de Edge cluster, dan zal de T0 gateway ook op beide Edge nodes aanwezig zijn. Dat betekent dan ook dat er 2 interfaces zullen moeten voorzien worden: één voor beide instanties.
{{</alert>}}


#### Routing

In principe zal je in productienetwerken gebruik maken van een routing protocol zoals BGP of OSPF om de interne netwerken te publiceren naar de buitenwereld en omgekeerd.
In deze opstelling is dit echter niet het geval en werken we voorlopig enkel met statische routering.

*   Bij BGP
    *   Schakel je BGP uit
*   Bij routering
    *   voeg een statische route toe, hier de default route
    *   Als Next Hop gebruik je de gateway van Vlan 27: 10.129.27.254


## T1 gateway

Deze T1 gateway zal verantwoordelijk zijn voor het East/West verkeer (routering tussen interne overlay netwerken). De uiteindelijke VM's zullen via het segment waarmee ze verbonden zijn de T1 kunnen gebruiken als hun gateway.

{{<alert type="info">}}
In ons scenario zal de T1 gateway geen extra services zoals NAT, DHCP,... nodig hebben. In dit geval zal de T1 gateway enkel als distributed router (DR) aanwezig zijn op elke host (in kernel). Mochten er wel bepaalde services nodig zijn, dan zal sowieso een T1 instantie (SR) uitgerold worden in de Edge Node.
{{</alert>}}

*   Voeg een nieuwe T1 gateway toe:
    *   Geef die als naam "t1-gw-01"
    *   **Connecteer hier voorlopig nog niet met de T0 gateway!**
    *   **Selecteer ook geen Edge cluster!** (anders wordt een SR in de Edge node gedeployed)
    *   Bij "Route Advertisement" adverteer je alle lokale segmenten: "All Connected Segments & Service Port"

### Intermezzo: uittesten overlay networking

Nu we een eenvoudige T1 gateway hebben (nog niet geconnecteerd met een T0) kunnen we even het routeren tussen verschillende overlay-netwerken uittesten.

We maken daarvoor 2 overlay segmenten aan (web-a en web-b) en connecteren die met de T1.
Daarnaast deployen we 2 kleine VM's (Tiny Linux) en connecteren we die met de zonet gemaakte segmenten.

*   Voeg 2 nieuwe **overlay** netwerken toe
    *   naam: web-a en web-b
    *   connecteer met de T1-gateway
    *   transportzone (nsx-overlay-transportzone-teamxx)
    *   gateway-ip adres: web-a => 10.0.1.1/24 en web-b => 10.0.2.1/24

*   Deploy de 2 VM's () elk verbonden met een van de segmenten
    *   Deploy adhv een OVA template => https://file.ikdoeict.be/Public/1%20ISO%20&%20Software_Installs/ISO/Andere/Tiny%20Linux/)
    *   Naam: Tiny01 en Tiny02
    *   ip-adressen: [Tiny01; 10.0.1.11/24; GW: 10.0.1.1] [Tiny01; 10.0.2.11/24; GW:10.0.2.1]
  
{{<alert type="info">}}
In de NSX-T manager kan je een mooi schema krijgen van de NSX-T componenten.
{{</alert>}}
![netwerk](nsx-t_config_16.png)

Als je de Overlay-segmenten web-a en web-b, samen met de T1 gateway, goed geconfigueerd hebt, dan kan je pingen tussen de 2 VM's.

![netwerk](nsx-t_config_17.png)

## Koppeling T1 met T0 router

*   Pas nu de configuratie van de T1 gateway aan zodat deze gelinkt is aan de T0 gateway
    ![netwerk](nsx-t_config_18.png)

{{<alert type="info">}}
Ook hier kan er nu een schema gegenereerd worden van de situatie.
{{</alert>}}
![netwerk](nsx-t_config_19.png)

## Connectiviteit met de buitenwereld

Om nu effectief de VM's te laten communiceren met de buitenwereld moeten de nodige **routes en/of NAT** geconfigureerd worden.  
Zoals eerder beschreven zal in de meeste situaties de T0 routes kunnen adverteren naar en ontvangen van de fysieke wereld via een routing protocol.  
In onze setup is er op de VLAN 27 (waarmee de T0 extern connecteert) geen routing protocol aanwezig.  
Hier moet dus gekozen worden om alle intern verkeer uitgaand te vertalen.

*   Voeg een `SNAT` regel toe die het verkeer van beide Overlay segmenten (web-a en web-b) zal vertalen naar een extern adres van vlan 27.
    *   Gebruik het eerstvolgende beschikbare ip-adres van je ontvangen range in vlan 27.

Als alles goed geconfigureerd is zou verkeer van de VM's naar de buitenwereld mogelijk moeten zijn.

*   Test a.d.h.v. een ping naar de gateway van vlan 27 en verderop naar bv. 8.8.8.8

## Inkomend verkeer

!!!!!
DNAT nog uit te schrijven....
!!!!!!