---
title: Source repository
draft: false
weight: 86
---


In het labo rond Kubernetes maakte je al kennis met deze functionaliteit. 

Ook voor deze opgave zullen we een source repository gebruiken. Je kan de werkwijze herhalen die je in het vorige lab (k8s) gebruikte. Hiervoor hoef je dus geen Terraform te gebruiken.

De bedoeling is duidelijk: we willen een container bouwen die bij elke push vernieuwd wordt.