---
title: Service Accounts
draft: false
weight: 87
---

Om de veiligheidsrisico's te beperken werken we met twee specifieke accounts voor de applicatie:

* **app_web**: deze account wordt gebruikt voor de cloud run instantie. De rechten op Bigquery zijn beperkt tot het aanmaken van een job en het lezen van de data in de dataset. (`roles/bigquery.dataViewer`)
* **app_cron**: we gebruiken deze account voor het periodiek ophalen van de data op de site van stad Gent. Deze account heeft wel schrijfrechten nodig op de BigQuery dataset. (`roles/bigquery.dataEditor`)


Maak deze twee accounts uiteraard aan met Terraform, en koppel bovenstaande permissies.

