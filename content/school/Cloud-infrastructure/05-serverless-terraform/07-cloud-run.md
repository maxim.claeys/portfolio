---
title: Cloud Run
draft: false
weight: 88
---

Nu we een Docker container hebben in een container-repository die bij elke push-actie vernieuwd wordt, is het tijd om dat te koppelen aan Google Cloud Run. 
Cloud Run is een serverless platform waar je containers kan hosten die webserver bevatten. Afhankelijk van de load worden 0 tot 1000 containers opgestart.

Ook deze dienst deployen we met Terraform.

Aandachtspunten:


* Standaard heeft niemand rechten om naar je website te surfen. Daarvoor moet je run.invoke-rechten toekennen aan de allUsers groep. Lees zorvuldig de output van Terraform. Soms gebeurt het dat een deel van de settings niet toegepast wordt waardoor je apply opnieuw moet toepassen. Voorbeeld: de rechten op cloud run kunnen enkel toegepast worden als de cloud-run service gedeployed is, en dat kan enkele minuten duren..

* maak een output variabele aan, zodat je de url van de Cloud Run instantie te zien krijgt in de terraform output na het uitvoeren van een 'apply'-actie.

{{<alert type="danger">}}

In de broncode sloop een foutje: in de query staat de naam van de bigquery dataset hardcoded, terwijl die bij jullie uiteraard anders zal zijn. Je kan de bron aanpassen (van main.py) en je eigen waardes kopiëren, of het oplossen met een omgevingsvariabele in je Cloud Run configuratie. Uiteraard moet je dan ook de Python-code aanpassen zodat die de environment-variabele oppikt...

{{</alert>}}
