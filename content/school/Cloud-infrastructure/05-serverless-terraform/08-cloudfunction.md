---
title: Cloud Function
draft: false
weight: 89
---

Een cloud functie wordt aangemaakt door een zip met daarin de broncode online wordt gezet in een bucket. 

Die broncode wordt dan uitgepakt en uitgevoerd in de functie. Bij de opstart van de functie kunnen variabelen meegegeven worden, en configuratie kan gebeuren met omgevingsvariabelen.


De broncode is de volgende: (main.py)

```python
import os

import requests
import json
from google.cloud import bigquery
import logging
import datetime
import time


def stream_to_bq(data, table_id=None):
    # Construct a BigQuery client object.
    # table_id = "your-project.your_dataset.your_table"

    client = bigquery.Client()

    errors = client.insert_rows_json(table_id, data)  # Make an API request.
    if errors == []:
        logging.info("new rows added")
    else:
        logging.error("Encountered errors while inserting rows: {}".format(errors))

def poll(request):
    table_id = os.getenv("TABLE_ID", "empty_table_id")

    api_response = requests.get(
        url="https://data.stad.gent/api/records/1.0/search/?dataset=bezetting-parkeergarages-real-time&q=&sort=-occupation&facet=name&facet=lastupdate&facet=description&facet=lichtfestival&facet=lf_pr&facet=b_park")
    parking_status = json.loads(api_response.content)

    rows = []
    for parking in parking_status["records"]:
        row = {}
        row["timestamp"] = datetime.datetime.fromisoformat(parking['fields']['lastupdate']).timestamp()
        row["name"] = parking['fields']['name']
        row["id"] = parking["fields"]['id']
        row["url"] = parking["fields"]['urllinkaddress']
        row["total_capacity"] = int(parking['fields']['totalcapacity'])
        row["occupation"] = parking['fields']['occupation']

        rows.append(row)
    logging.info(rows)
    stream_to_bq(rows, table_id=table_id)
    return "ok", 200


if __name__ == '__main__':
    while (True):
        poll("empty request")
        time.sleep(60)
```

In dezelfde zip hoort ook een requirements file te staan:

```
requests
google-cloud-bigquery
```

Maak een `google_storage_bucket`, een `google_storage_bucket_object` (met de zip file) en een `google_cloudfunctions_function` aan.

De functie zelf heeft genoeg met 128MB Ram, de runtime is python39, en de functie wordt getriggerd met http.

Er is ook een omgevingsvariabele nodig: `TABLE_ID`. Stel die gelijk aan de tabelnaam van je bigquery-tabel. (formaat projectnaam.datasetnaam.tabelnaam)
