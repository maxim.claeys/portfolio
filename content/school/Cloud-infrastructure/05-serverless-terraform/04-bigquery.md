---
title : Bigquery
draft: false
weight: 85
---

### Aanmaak dataset
Maak (uiteraard opnieuw met Terraform) een dataset en tabel aan in Bigquery. 

### Aanmaak tabel
De tabel moet uiteraard de waardes kunnen bewaren die door de cloud functie doorgestuurd worden.

Gebruik daarvoor deze structuur:

```[terraform]
  schema = <<EOF
[
    {
    "name": "timestamp",
    "type": "TIMESTAMP"
    },
    {
    "name": "name",
    "type": "STRING",
    "mode": "NULLABLE"
    },
    {
    "name": "id",
    "type": "STRING",
    "mode": "NULLABLE"
    },
    {
    "name": "url",
    "type": "STRING",
    "mode": "NULLABLE"
    },
    {
    "name": "text",
    "type": "STRING",
    "mode": "NULLABLE"
    },
    {
    "name": "total_capacity",
    "type": "INT64",
    "mode": "NULLABLE"
    },
    {
      "name": "occupation",
      "type": "INT64",
      "mode": "NULLABLE"
   }
]
EOF
```