---
title : "Modus Operandi"
page_title: Inleiding
draft: false
weight: 82
---

## Voorbereiding


{{<alert type="info">}} 
Deze opgave wordt individueel afgewerkt... 
{{</alert>}}

### Basiskennis Terraform

Voor deze opgave werken we met Terraform. 

Ter voorbereiding van het labo vragen we om minstens 2 labs uit te voeren uit de Qwiklabs quest "Automating Infrastructure on Google Cloud with Terraform":
* Terraform fundamentals
* Infrastructure as code with terraform

Vrijblijvend kan je ook de andere labs afwerken. Indien de volledige quest wordt afgewerkt, ontvang je een digitale badge die je bijvoorbeeld op je LinkedIn profiel of curriculum vitae kan vermelden.

Deze labs hebben telkens een tijdslimit, dus plan deze zorgvuldig in. Vaak is deze tijdslimiet erg ruim, wat je de mogelijkheid geeft om ook wat te experimenteren buiten de opgave.

De benodigde credits (€€€) om deze labs te doorlopen worden aangeboden via de hogeschool (als je aanmeldt met je mailadres @student.odisee.be). +
Mocht je merken dat dat voor jouw account niet zo is, neem dan zeker contact op met je docenten...

{{<alert type="tip">}}
Open de labs steeds in een privaat browservenster... Zeker als je veel labs gaat doorlopen is dat de beste manier van werken.
{{</alert>}}

Daarnaast vragen we om de documentatie te bestuderen op https://learn.hashicorp.com/terraform?track=gcp#gcp. Vergewis je er nadien van dat je onderstaande termen begrijpt en kan aanvullen...

* State
* Plan/Apply
* Provider
* Block / List
* ...

{{<alert type="caution">}}
De tutorials zijn bedoeld om de algemene werking te begrijpen. Neem de voorbeelden dus niet 'klakkeloos' over: vaak zijn bijvoorbeeld versies die aangehaald worden stevig verouderd...
{{</alert>}}

### Toegang tot Google Cloud

Als student krijgen jullie een budget van $50 per persoon om deze opgave op te zetten op Google Cloud. Op Toledo vind je de link om die credits aan te vragen.

{{<alert type="caution">}}
50$ is ruim voldoende zijn om deze opgave af te werken, maar waak er zeker over dat je je cluster steeds naar nul terugschaalt nadat je er op werkt. Een cluster die meerdere weken actief staat zal je budget snel tot nul herleiden.  
{{</alert>}}

## Leerdoelen


* Basisfuncties van Terraform toepassen: plan/apply/init
* Het concept 'serverless' begrijpen, en de consequenties kunnen inschatten op je infrastructuur en applicatie
* Serverless componenten met elkaar linken in Google Cloud
* Conceptueel begrip van serverless diensten als Cloud Functies, BigQuery als data lake, Cloud Run.

## Opgave & evaluatie


Tijdens dit labo zullen jullie enkele componenten in Google Cloud met elkaar linken. Het deployen daarvan doen we zoveel mogelijk met Terraform, een configuration state management tool die specifiek ontwikkeld werd voor cloudtoepassingen.

Bij de verdediging van deze opdracht zal de score opgesplitst worden:

* De praktische implementatie (50%)
* De theoretische achtergrondkennis (30%)
* Uitvoering van extra functionaliteit (20%)

Zorg er dus zeker voor dat je op het moment van de verdediging de theoretische achtergrond nog grondig herhaalde.


