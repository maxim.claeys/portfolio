---
title : Architectuur
page_title: Architectuur en componenten
draft: false
weight: 83
---

## Gebruikte cloud-componenten


**Cloud Run**: deze serverless dienst laat toe om containers in de cloud te plaatsen zonder dat je zelf moet omzien naar implementatiedetails als serverplatform, load balancing, ...
De dienst zal op basis van de hoeveelheid inkomende trafiek zelf beslissingen nemen ivm scaling. Bij deze dienst moet je applicatie wel volledig stateless zijn. Containers worden immers gestart en gestopt zonder notificatie.

**Bigquery**: deze serverless dienst is een `data lake`. Je kan er ongelofelijk grote hoeveelheden data naartoe sturen om analyses op te doen. Groot verschil met bijvoorbeeld MySQL is dat de databank niet relationeel is: tabellen zijn dus niet gelinkt aan elkaar...

**Cloud Build**: het CI/CD platform bij Google Cloud. 

**Artefact registry**: 'depot' voor containers, maar ook bijvoorbeeld rpm of deb files

**Cloud storage**: (ook wel gewoon buckets genoemd) het equivalent van AWS S3 storage buckets.

**Cloud function**: Serverless component die toelaat om code uit te voeren. Daar kan een http trigger aan toegevoegd worden, waarmee je de functie kan triggeren. De broncode van de functie plaats je in een Cloud storage bucket.

**stad Gent Open Data**: platform waarmee data kan verzameld worden.

## Schema

![schema](diagram-serverless.png)