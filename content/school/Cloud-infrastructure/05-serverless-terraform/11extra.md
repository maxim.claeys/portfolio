---
title: Extra
draft: false
weight: 92
---

Met deze oefening koppelde je een aantal serverless producten. Daar stoppen de mogelijkheden natuurlijk niet. Voor de laatste twintig procent van de punten kan je facultatief één van volgende uitbreidingen uitwerken:

* Voeg met Cloud Logging een notificatie toe. Als meer dan 1 keer een error 404 voorkomt op de cloud-run site, moet je daarvan een mailtje krijgen. 
* Voeg caching toe. De manier kan je kiezen: maar nu is de pagina traag omdat bigquery steeds moet bevraagd worden. Door een cachinglaag (naar keuze) toe te voegen moet dit (vanaf de tweede request) stukken sneller lukken.
* Bouw een nieuwe grafiek die de een interessante nieuwe kijk geeft op de data.