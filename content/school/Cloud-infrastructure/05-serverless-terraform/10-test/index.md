---
title: Testen
page_title: Testen van het resultaat
draft: false
weight: 91
---

## Tijd om te testen!

Als alles goed gelukt is, moet je de applicatie helemaal werkend hebben. Dat betekent dat elke 5 minuten automatisch de API van stad Gent bevraagd wordt, en dat de resultaten in Bigquery terecht komen. 
Met de webapp kunnen de gebruikers steeds de laatste resultaten zien die in de databank zitten.

Daarvoor surf je naar een specifiek endpoint van de app: `/parking`

Het spreekt voor zich dat het een weekje kan duren vooraleer je iets te zijn krijgt als het onderstaande:

![resultaat](result.png)

