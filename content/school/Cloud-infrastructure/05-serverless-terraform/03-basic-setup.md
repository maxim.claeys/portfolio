---
title: Basic setup
draft: false
weight: 84
---

## Installatie en gebruik van Terraform

Installeer eerst en vooral Terraform op je computer. (http://www.terraform.io)
Let daarbij op: terraform-versies zijn onderling vaak niet compatibel. Dit labo werd voorbereid op versie 1.0.11.
Volg de aanwijzingen in de 'getting started'-tutorial op Terraform.io om je systeem verder voor te bereiden: maak een service account aan op GCP (en geef deze voldoende rechten), en maak ook een file met een provider aan. Zorg ervoor dat de regio `eu-west1` en de zone `eu-west1-a` gebruikt worden.

{{<alert type="tip">}}
 Online vind je vaak voorbeelden van providers-objecten waar oude versies gebruikt worden. Gebruik bij voorkeur een recente versie, want we gebruiken elementen op Google Cloud die relatief nieuw zijn.
{{</alert>}}

## Docenten-toegang tot je GCP-project

Als eerste Terraform-taak vragen we je om het project te delen met je docenten zodat wij kunnen assisteren bij problemen. Gelieve via de IAM-instellingen van je GCP-project op console.cloud.google.com rechten (editor role) te geven aan roel.vansteenberghe@ikdoeict.be en Sven.knockaert@ikdoeict.be. 

Hernoem je GCP-project ook naar 2122-VOORNAAM-NAAM.

Dat zou makelijk kunnen via de grafische console of de command line, maar om manueel werk te vermijden doen we ook deze stap met Terraform.

{{<alert type="danger" text="warning">}}
Deze twee accounts worden *niet* als emailadres gebruikt. Gebruik bij vragen dus steeds onze Odisee-adressen.
{{</alert>}}

Maak hiervoor dus je eerste resources aan in je Terraform-file. 

{{<alert type="tip">}} 
Je zal hiervoor een google_project_iam_binding nodig hebben. 
{{</alert>}}

Nog even ter herhaling (na de Qwiklabs en het bestuderen van de documentatie) de stappen die je zal moeten uitvoeren uitvoeren:

* Resource aanmaken (adhv de Terraform provider documentatie op https://www.terraform.io/docs/providers/google/) in een tekstbestand op je pc.
* `terraform init` uitvoeren om de provider te downloaden
* `terraform plan` uitvoeren om te zien wat gaat gebeuren eens je gaat voor apply kiezen
* `terraform apply` uitvoeren: mogelijks zal je de melding krijgen dat je eerst nog de 'cloud resource manager api' moet activeren. Doe dat dan uiteraard ook (de activatielink vind je terug in de waarschuwing op het scherm)

De output lijkt dan vermoedelijk op onderstaande:

```terraform
Terraform will perform the following actions:

  # google_project_iam_binding.project will be created
  + resource "google_project_iam_binding" "project" {
      + etag    = (known after apply)
      + id      = (known after apply)
      + members = [
          + "user:roel.vansteenberghe@ikdoeict.be",
          + "user:sven.knockaert@ikdoeict.be",
        ]
      + project = "**************"
      + role    = "roles/editor"
    }

Plan: 1 to add, 0 to change, 0 to destroy.
```