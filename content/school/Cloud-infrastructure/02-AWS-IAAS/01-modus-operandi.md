---
title : "Modus Operandi"
draft: false
weight: 69
---
## Voorbereiding

De cursus van AWS Academy **AWS Academy Cloud Foundations [10551]** moet doorgenomen zijn en de `knowlegde checks` afgelegd.

## Leerdoelen

* Begrip van het concept public cloud provider
* Implementatie van resources in een public cloud
* Begrip en implementatie van technieken zoals Autoscaling, VPC, Security Groups, CDN
* Toepassing van Infrastructure-as-code in een public cloud

## Opgave 

informatie over de opgave zelf. VB: hoe moet ze gemaakt worden, eventuele requirements, ...


## Evaluatie

verdeling in % van de score op deze opgave.
