---
title : "Basic lab"
page_title: "Public Cloud Infrastructure met AWS"
draft: false
weight: 70
---
## Vereisten & voorbereiding

Voor dit labo werk je individueel met een publieke cloud provider.
In dit geval kiezen we voor `Amazon Web Services (AWS)`.
Als je zelf eens naast de lessen wil experimenteren met AWS kan je vrij makkelijk een "free-tier" account aanmaken waarmee je al heel wat interessante mogelijkheden van deze cloud provider kan uittesten.

Binnen het kader van het vak "Cloud Infrastructure" hebben we voor jou een sandbox-AWS-omgeving afkomstig van de AWS academy voorzien waarmee je de nodige resources kan aanvragen en consumeren om dit lab uit te voeren.  
Niet alle AWS-services en features zijn toegelaten, maar de belangrijkste die je nodig hebt zijn beschikbaar!

* Aanmelden kan met je @student.odisee.be account op het AWS Academy portaal: https://awsacademy.instructure.com/login/canvas  
  Je kreeg daarvoor eerder een mailtje om je te registreren.
* Eenmaal aangemeld ga je verder in de cursus **AWS Academy Learner Lab - Foundation Services [8599]**
* Je gaat er naar **"modules> Learner Lab - Foundational Services"**

{{<alert type="caution">}}
Lees het README venster aan de rechterkant eens door. Daar staat belangrijke informatie in hoe je de AWS sandbox kan gebruiken. Er zitten nl. beperkingen op het gebied van maximale kosten ($100) en de tijdsduur voor een lab vooraleer het gestopt wordt.  
Er staat ook in welke gevallen je lab vernietigd zal worden!!
{{</alert>}}

* Via deze weg kan je naar de echte AWS-omgeving gaan. Klik daarvoor de link aan boven het command-line venster.

  ![aws-omgeving](aws_01.png)

* Er wordt nu voor jou een tijdelijke AWS-account voorzien. Een `AWS-account` kan je aanzien als een soort "tenant" binnen je bedrijf. Daaronder zitten resources, maar ook gebruikers, hun rechten, etc... gegroepeerd. Als bedrijf kan je misschien met 1 account voldoende hebben of werk je met meerdere accounts om bijvoorbeeld verschillende afdelingen van elkaar te scheiden. Heel dikwijls wordt het ook gebruikt om bijvoorbeeld een "Test-omgeving" te scheiden van een "Production-omgeving".  Hier is jullie login een "federated user" onder een AWS-account. De username ziet er in dit geval uit als `voclabs/user1704276=naam_student`

* Binnen dergelijke AWS-account kan dan in principe via *IAM* (Identity & Account Management) verder rechten gegeven worden aan verschillende gebruikers die met resources aan de slag moeten gaan.  
Het is ten sterkste aangeraden om nooit rechtstreeks resources aan te maken/te gebruiken onder je root-account. Best practice is om dit altijd via een IAM user te doen  (hier nu niet van toepassing).

{{<alert type="tip">}}
Voor je aan de opgave begint, bekijk je best alle stappen die zullen moeten doorlopen worden. Zo krijg je een beeld van het uiteindelijk doel, en dat kan je helpen als onderweg keuzes moeten gemaakt worden.
{{</alert>}}

{{<alert type="danger">}}
Wees alert bij het aanvragen van resources! Bepaalde diensten kunnen heel snel heel duur worden en dus zorgen voor een hoge factuur... 
Let er dan ook steeds op om resources die blijvend kosten genereren uit te schakelen! Zie ook de README in de AWS academy course!
{{</alert>}}


## Regio's, Availability Zones en prefixen


* Publieke cloud providers werken met datacenters die overal ter wereld verspreid zijn. Je zal dus altijd je opstelling en resources in een bepaalde `Regio` aanmaken, configureren en beheren. Soms zal je nog specifieker moeten aangeven in welke `Availability Zone` je die resources wenst te plaatsen. Een Availability zone is een onderdeel (een specifiek datacenter) in een bepaalde regio. Meestal zijn er 3 of 4, soms meer, availability zones in een regio. Deze availability zones in een regio liggen slechts enkele (tientallen) kilometers van elkaar om de latency laag te houden en op die manier redundantie te voorzien in de regio. Meer info op: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html

![regios](Regions_AZ.png)

{{<alert type="tip">}}
Zoals je gelezen hebt kunnen jullie enkel resources deployen in de 2 regio's, nl. US East (N. Virginia) en US West (Oregon).  
Eenmaal ingelogd in AWS kan je indien nodig in de rechterbovenhoek, naast je accountnaam, je regio wijzigen.
{{</alert>}}

* Om in dergelijk grote omgeving, waar je dikwijls met meerdere personen resources beheert, goed je eigen resources te kunnen herkennen vragen we om **overal een prefix** voor te plaatsen (bij elk item dat je moet aanmaken, vb EC2-vm, een VPC, security group,...)
    *   Als prefix gebruik je telkens je **team-naam en nummer** binnen je team in hoofdletters, zonder voorloopnul  
        vb:
        * team 6, student 1 **TEAM61**-EC2_Linux_Webserver of 
        * team 12, student 2 **TEAM122**-Keypair 

## Achtergrond


In dit labo is het de bedoeling om een drietal cases uit te werken.

* Een eerste scenario focust zich op het aanmaken van virtuele machines in een publieke cloud en die in te zetten als webserver. +
Dit scenario moet schaalbaar gemaakt worden zodat bij een grotere vraag (bv ticketing-service) automatisch extra webservers gedployed worden.
* Een tweede scenario heeft eenzelfde doel maar zullen we de resources via "automation" (infrastructure-as-code) gaan deployen. Er wordt ook afgeweken van de standaard beschikbare netwerken.
* In een derde scenario gaan we gebruik maken van een Content Delivery Network (CDN) om je service (website) geografisch wereldwijd bijschikbaar te maken. 

### Scenario 1: Virtuele machines met loadbalancer

#### Overview

![overview](Autoscaling.png)

#### Networking

Voor dit scenario is het de bedoeling dat onze virtuele machines in een apart, geïsoleerd netwerk van de private cloud functioneren.
Bij AWS creëer je hiervoor een `Virtual Private Cloud` (VPC). Dergelijke VPC voorzie je ook van een adresblok, ipv4 en/of ipv6, dat eventueel nog verder kan opgedeeld worden in logische eenheden of `subnets` genaamd. Een VPC zorgt er voor dat resouces in AWS volledige van elkaar gescheiden zijn (vb. verschillende klanten of tenants). Subnets daarentegen functioneren zoals in de fysieke wereld en kunnen via routering binnen de VPC met elkaar verbonden worden. In principe gebruik je voor je subnets private IPv4 adressen. Meer info over VPC en subnets op https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html

Verder kan je er voor kiezen om tijdens het deployen van een VM (EC2 Instance) onmiddellijk een publiek IPv4 adres te koppelen. Via een `Internet Gateway` kan een VM dan via zijn publiek ip-adres bereikt worden of zelf toegang tot internet verkrijgen (d.m.v. NAT).

Je kan er natuurlijk ook voor kiezen om zelf een `NAT Gateway` te voorzien (zie scenario 2). De VM's zullen in dat geval enkel voorzien worden van een privaat IP-adres.


* Maak een nieuwe **VPC** aan (*TEAMxy-VPC-scen1*) met adresblok 192.168.10.0/24
* Creëer drie **nieuwe subnetten** (*TEAMxy-subnet1,2 en 3*) die telkens een kwart van je adres-space gebruiken (192.168.10.0/26,...)  
Je creëert elk subnet in een aparte **Availability Zone**  
Let even erop of, na aanmaak, publieke ip-adressen zullen voorzien worden voor elke instantie die aangemaakt wordt.
* Maak een nieuwe **Internet Gateway** aan (*TEAMxy-igw-scen1*) en koppel die aan je VPC.

#### VM Instance aanmaken

##### Key Pair voor SSH

* Maak eerst en vooral een nieuw **Key Pair** aan (bij de EC2 service). Dit zal je nodig hebben om met elke machine via SSH te kunnen connecteren. 
Bewaar de private sleutel (*.pem) op een veilige, maar voor jou makkelijk bereikbare plaats.


##### EC2 Instance aanmaken.


* Maak een nieuwe virtuele machine instantie aan (EC2)
  * Neem de *Amazon Linux 2 AMI* (free Tier)
  * Type hardware resources: *t2.micro*
  * Plaats die in je zonet aangemaakte VPC en subnet 1 (* => zie verder).
  * Laat de VM automatisch een publiek ip-adres krijgen.
  * Geef bij het deployen enkele commandline opdrachten mee (*User Data* veld):  
    * *package manager updaten*
    * *Apache webserver installeren*
    * *Apache webserver als service enablen* (automatisch opstarten)
    * *Apache service starten*
    * *bootstrap website downloaden* (zip-file *AWS-bootstrap-lab.zip* op https://tinyurl.com/4xe2jh9y )
    * *bootstrap website uitpakken in de juiste locatie voor de Apache webservice *
    {{<alert type="tip">}}
  Je kan de syntax voor user data best opzoeken in de AWS documentatie.
  ```bash
  yum update -y
  ```
    {{</alert>}}
    

  * Voeg verder een **tag** "student" met als waarde je prefix en een **tag** "service" met als waarde "scen1-webserver" toe.
  * Creëer bij het aanmaken van de instance een nieuwe **Security Group** aan met de naam "Teamxy-sg-scen1" en een relevante description.
    * Laat SSH en HTTP(s) door afkomstig van alle source adressen.


{{<alert type="tip">}}
Zoals de wizard van AWS aangeeft is deze aanpak helemaal niet zo veilig. Je VM is immers rechtstreeks op het internet, publiekelijk bereikbaar.
Dit gaan we in het volgende scenario anders aanpakken.
{{</alert>}}

{{<alert type="tip">}}
(*) Soms is een bepaalde instance (vm template) niet beschikbaar in een bepaalde zone. Daar moet je rekening mee houden bij de verdere opgaves zodat je in die gevallen eventueel de instance dan niet uitrolt in die zone (of subnet).
{{</alert>}}

Als alles goed verlopen is zou, na het deployen van deze instance, je VM bereikbaar moeten zijn via zijn publiek ip-adres.  
**Opgelet**: Wellicht zal je de routing table voor je VPC moeten aanpassen zodat die over een default route beschikt.

* Controleer of je via SSH op je VM kan aanmelden.
* Controleer of je script in "User Data" effectief goed werkt door te surfen naar je VM.  
Je zou er een bootstrap website moeten te zien krijgen.
* Zorg er nu ook nog voor dat je EC2 instance een **Public IPv4 DNS**-naam krijgt. (vb: ec2-15-228-35-208.sa-east-1.compute.amazonaws.com )


{{<alert type="caution">}}
Laat dit resultaat eventjes zien aan je docent vooraleer verder te werken.
{{</alert>}}

* Verwijder je aangemaakte instance(s) door deze te vernietigen (**Terminate**), na controle.

##### AutoScaling Group

`Autoscaling` kan er voor zorgen dat op basis van bepaalde parameters (CPU, Netwerk traffic, memory, requests, tijd,...) extra instanties van je VM opgestart worden.
Op die manier kan je makkelijk inspelen op de noden en automatisch extra VM's inzetten of net afsluiten als de piekbelasting over is (besparend).

###### Autoscaling Group vs Autoscaling Launch Configuration

Een AutoScaling Group (ASG) moet uiteraard gebruik maken van een VM (EC2 instance) die als basis moet dienen om verder te schalen.
Deze instance, met zijn specifieke settings zoals de "User Data" in ons voorbeeld, heet een **Launch Template** of **Launch Configuration**  
Eenmaal er een dergelijke Launch template of Launch configuration voorzien is, kan deze gekoppeld worden aan een ASG. 


* Maak een nieuwe "Launch Configuration" aan.
  * op basis van *Amazon Linux 2 AMI (x86)*, *t2.micro*
  * Naam: _TEAMxy-lc-scen1_
  * Bij *User Data* dezelfde commands als bij je eerste instantie.
  * Zorg dat er een publiek IP-adres gekoppeld zal worden: Auto-Assign a public IP address to every instance.
  * Gebruik je eerder aangemaakte *Security Group*
  * Gebruik hetzelfde *Key Pair*.

* Maak een nieuwe AutoScaling Group aan met de naam _TEAMxy-asg-scen1_ op basis van de Launch Configuration die je net aangemaakt had:
  * Zorg er voor dat je geschaalde instanties in je eigen VPC gedeployed worden.
  * Zorg dat extra gedeployde instances in je 3 subnets (availability zones) terecht komen.
  * Gewenst aantal gewenste instanties in de ASG:  Group Size 1  (Desired Capacity)
  * Voorlopig nog geen Autoscaling Policies configureren: "Keep this group at iets initial size"
  * geef de tags "student" met waarde *TEAMxy* en "service" met waarde "autoscaling_group_scen1" mee.


In principe zou de AutoScaling nu al automatisch 1 instantie opstarten aangezien we aangegeven hebben dat er altijd minimum 1 instantie actief moet zijn.

* Check even de "Activity History" om de uitgevoerde acties en bijhorende reden van de ASG te bekijken.


###### Autoscaling Actions

Om je opstelling automatisch te laten schalen zullen parameters of events moeten gebruikt worden om ofwel instances extra op te starten of uit te schakelen.  
Meestal zullen deze parameters op basis van metrics zoals CPU, memory, IO, #requests, etc... gebaseerd zijn om *Scaling Policies* te definiëren.  
Wij kiezen initieel voor een eenvoudigere parameter op basis van tijd. Hiermee kan je dan *Scheduled Actions* configureren.  


* Maak een *Scheduled Action* aan met als naam _Teamxy-sa-up-scen1_
  * Minumium: 1, Maximum: 2 en Desired capacity: 2
  * Recurrence: Once
  * Start Time: vandaag over 10 minuten (wij zijn UTC+1)


* Controleer of je na enkele minuten (max. 10) bij de "Action History" van je ASG een nieuw event kan zien.


* Ga ook even na bij je EC2 instances of er effectief een instance extra gedeployed werd.
* Contoleer ook hier even of de website op de instance bereikbaar is via zijn publiek IP-adres.

##### Load Balancer (LB)


Indien je meerdere servers (instances) gedeployed hebt die dezelfde content aanleveren dan kan je de belasting tussen deze servers verdelen door daar een **Load Balancer** voor te plaatsen.
Er zijn verschillende types Load Balancers mogelijk (vb. Application LB, Network LB,...) afhankelijk op basis van welk protocol je de load wenst te verdelen.  
In ons geval maken we gebruik van een webserver via HTTP, wat dan ook meteen duidelijk een applicatie protocol is.

###### LB aanmaken

* Maak een nieuwe **Application Load Balancer** aan met de naam _TEAMxy-elb-scen1_
* De listener is uiteraard voor HTTP geconfigureerd
* De Load Balancer mag geplaatst worden in je eigen VPC en
* de LB mag de load verdelen naar 3 verschillende datacenters in je Regio.  
  Dat komt in ons geval overeen met onze subnets.  
  (Houd rekening met zones waar de instance niet beschikbaar is!)
* Creëer een nieuwe "Security Group" met de naam _TEAMxy-sg-lb-scen1_
  * HTTP toelaten
  * Source: van overal
* Bij de routing wordt bepaald hoe inkomende request moeten gerouteerd worden.
  * Geef je *Target Group* de naam _TEAMxy-tg-lb-scen1_
  * De targets zelf zullen uiteraard de instances (VM's) zijn.
  * bekijk ook eens de functie van de *Health Checks*
* Bij "Register Targets" gaan we *nog geen instances toevoegen*!  
Dit omdat we de Load Balancer voor de Autoscaling Group willen "hangen" die 1,2 of soms meer instances kan bevatten dan deze die je nu kan selecteren...  
De Load Balancer moet m.a.w. rekening kunnen houden met de dynamische toestand van de Autoscaling Group.


###### LB koppelen aan de Autoscaling Group

Zoals hierboven beschreven hebben we nu een Load Balancer maar deze heeft momenteel nog geen targets waar hij zijn requests kan naartoe sturen.

* Pas je Autoscaling Group aan zodat deze gebruik maakt van je Load Balancer.
  * gebruik hiervoor je Target group.


##### Publieke toegang vermijden


* Pas nu de security group van je webservers aan (_Teamxy-sg-scen1_) zodat deze **enkel** HTTP verkeer van de Load Balancer kunnen ontvangen.

{{<alert type="tip">}}
In een AWS security group kan je een andere security group als source of destination ingeven. Op die manier zijn alle resources die lid zijn van de groep automatisch toegelaten, ook al zou de samenstelling van de groep veranderen (door bvb autoscaling).
{{</alert>}}

* Controleer of je nu inderdaad niet meer rechtstreeks via het publieke IP-adres aan je webservers geraakt.
* Controleer of je via de DNS-naam van je Load Balancer afwisselend de webpagina van de ene of de andere instantie te zien krijgt. 

{{<alert type="tip">}}
Pas eventueel de index.html aan van de webservers zodat je duidelijk kan zien op welke server je terecht komt.
{{</alert>}}

##### Terugschalen

* Maak een nieuwe "Scheduled Action" aan die je ASG terug schaalt naar 1 instance met de naaml _Teamxy-sa-down-scen1_
  * Minumium: 1, Maximum: 2 en Desired capacity: 1
  * Recurrence: Once
  * Start Time: vandaag over 10 minuten (wij zijn UTC+1)

* Controleer of de terugschaal-actie ook werkt.
* Kijk wat er gebeurt als je de enige resterende VM aflsuit en/of verwijderd.

{{<alert type="caution">}}
Laat dit resultaat eventjes zien aan je docent vooraleer verder te werken.
{{</alert>}}