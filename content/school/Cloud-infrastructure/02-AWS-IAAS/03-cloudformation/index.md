---
title : "Cloudformation lab"
page_title: "Scenario 2: Infrastructure as Code"
draft: false
weight: 71
---

In het vorige scenario werden alle stappen manueel doorlopen. In een grotere omgeving, waar alles snel en flexibel moeten kunnen geïntegreerd worden, is dat uiteraard niet meer haalbaar. In zo'n geval zullen we moeten gebruik maken van `Infrastructure as Code`.
Op die manier kan makkelijk bv. versiebeheer toegepast worden op je te deployen infrastructuur en is een complexe setup heel makkelijk reproduceerbaar.
Binnen AWS is `CloudFormation` de built-in tool die dit mogelijk maakt. 

Op basis van **Templates** en **Stacks** wordt een volledige infrastructuur beschreven en gedeployed.

## Templates

**Templates** worden als een `JSON` of `YAML` tekst bestand opgeslagen en worden door AWS als een soort "blueprint" bekeken.
Templates beschrijven typisch alle elementen die je anders  manueel zou moeten configureren (vb instances, VPC, subnets,...).

## Stacks

Een **stack** is eigenlijk een verzameling van AWS resources die als 1 geheel bekeken/beheerd worden. +
Dergelijke stack wordt opgebouwd of beschreven door een **template**. Cloudformation zorgt er voor dat alle onderdelen van de stack gedeployed worden als geheel maar zorgt ook voor een volledige roll back indien bv. enkele onderdelen falen tijdens de deploy.  
Of... Als de stack verwijderd zou worden, dan worden alle bijhorende resources ook terug verwijderd.  
Op die manier blijft alles netjes beheersbaar en overzichtelijk.

## Uitwerking Scenario 2 met Cloudformation

In dit scenario is het de bedoeling om een gelijkaardige setup te maken zoals in scenario 1 (webservers met loadbalancer). +
Alleen gaan we dit enerzijds automatiseren via Cloudformation en gaan we anderzijds gebruik maken van **NAT gatways** per Availability Zone naast de **Internet Gateway**.
Verder zullen we gebruik maken van publieke en private subnets.

![cloudformation](Cloudformation.png)

Er werd voor jullie reeds een CloudFormation template aangemaakt. 

[(download hier)](as-origineel.zip)

Verschillende templates zijn vrij beschikbaar op internet of je kan ze in AWS via een "Template Designer" makkelijk opbouwen.

* Download en importeer (of Copy/Paste) deze template eens in de Template Desiger en bekijk het gevisualiseerde resultaat van deze template.


* Bewerk deze verder (in bv. Visual Studio Code) met onderstaande opdrachten:
  * Voeg de *User Data* toe zoals in scenario 1 

    {{<alert type="tip">}}
Voeg dit uiteraard toe op de juiste locatie van de template en in het verwachte formaat (zoek op in de documentatie van AWS hoe je dit moet toevoegen)  
(bv. via Base64 function, en de CF function "Join")
    {{</alert>}}
    

  * Er ontbreekt een property in de "VPC gateway attachment". Zoek ook hier in de documentatie van AWS wat er precies verplicht is en vul dit aan.


## Deployment van de template


* Maak een nieuwe *Stack* aan met als naam _TEAMxy-stack-scen2_
  * Gebruik hierbij de naam van je eigen "Key Pair"
  * ScaleDownTime: Deze tijd + 10 minuten
  * ScaleUpTime: Deze tijd + 20 minuten
  * Geef bij "Surname" je prefix mee _TEAMxy_
  * Geef ook opnieuw de 2 tags "student" met waarde _TEAMxy_ en "service" met waarde "scenario2"mee.


Op dit moment zullen alle resources uit je template gedeployed worden en als 1 geheel  gemanaged worden.  
Dat betekent dan ook dat bij het verwijderen van de *stack* alle betrokken resources weer netjes opgeruimd worden.


Als je "stack-deployment" goed is verlopen moet je even de resouces en services gaan bekijken:

* VPC
  * VPC
  * Subnets
  * Routing tables
  * NAT gateways
  * Internet gateway
* EC2
  * Launch Config
  * Autoscaling group
  * Security groups
  * Instances
  * Load balancer

{{<alert type="tip">}}
Probeer eens de gekoppelde onderdelen te "volgen". bv. bekijk een instance en zijn (uitsluitend) private adres.  
Deze zit in een subnet. Dat subnet heeft een route tabel en een (NAT). enz...
{{</alert>}}

* **Uiteraard probeer je de ultieme test eens uit... Probeer te surfen naar je webservers!!! (HTTP)**

* Lukt het je nog om de configuratie van je Apache webserver (*.html) pagina te wijzigen?



{{<alert type="caution">}}
Sla je template op met de naam *Labo_AutoScaling_TEAMxy_Basis.json* en *laat dit resultaat eventjes zien aan je docent vooraleer verder te werken*.
{{</alert>}}

## Change Set / Update Stack

Zoals je zal gemerkt hebben kan je een Stack die je probeert te deployen niet zomaar aanpassen.
Als je een gedeployde Stack wil aanpassen moet je deze ofwel helemaal opnieuw inladen en opbouwen ofwel kan je een *Change Set* (of update) doorvoeren.

Bij zo'n Change Set of update kan je dan een aangepaste CF-template meegeven. 
Cloudformation zal dan de nieuwe template vergelijken met de bestaande deployment en wijzigingen doorvoeren.

In de volgende opdracht moet je 2 aanpassingen in 2 stappen uitproberen en doorvoeren:


In de bestaande template die je van ons kreeg merk je op dat in de structuur van de JSON-file momenteel de *secties* _"Description"_,_"Parameters"_, _"Mappings"_ en _"Resources"_ gebruikt worden.  
Er zijn echter nog andere secties mogelijk om de functionaliteit van je CF-template uit te breiden.

* Bestudeer de anatomie van dergelijke CF-template op https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/template-anatomy.html 

* Maak nu in je eigen template (kopie!) gebruik van de sectie *"Output"*. Hiermee kan je waardes, parameters,... die gegenereerd werden tijdens het deployen van de stack in een key-value pair output-variabele steken (eventueel na manipulatie).  
  * Concreet zorg je er voor dat na het deployen 2 URL's gegenereerd worden:
    * URL van je website (pad naar de loadbalancer) met key WebsiteURL.
    * URL naar de README.md pagina van je website met key BootstrapURLReadMe

![Output](Cloudformation_Output.png)


{{<alert type="caution">}}
Sla je template op met de naam *_Labo_AutoScaling_TEAMxy_Output.json_* en *laat dit resultaat eventjes zien aan je docent vooraleer verder te werken*..
{{</alert>}}

## Parameters in CloudFormation
Er werden in de opgegeven template al verschillende parameters gebruikt.
Hier moet je nu zelf deze sectie aanpassen:


* Zorg er voor dat via de sectie "Parameters" de gebruiker via een "drop-down" box kan kiezen hoeveel instanties er moeten opgestart worden bij de "Autoscaling group"

![Parameters](Cloudformation_Parameters.png)

{{<alert type="caution">}}
Sla je template op met de naam *_Labo_AutoScaling_TEAMxy_Parameter.json_* en *laat dit resultaat eventjes zien aan je docent vooraleer eventueel verder te werken*..
{{</alert>}}

## Uitdaging 

Een Cloudformation template kunnen we heel interactief maken. Je kon al opmerken dat er in de opgegeven template gebruik gemaakt werd van "Parameters".
Hiermee kan je er voor zorgen dat bij elke deployment bepaalde parameters aangepast worden.
Maar je kan dergelijke templates nog wat intelligenter maken door gebruik te maken van *condities*.
In de template kan je hiervoor een nieuwe "sectie" gebruiken met de naam *Conditions*".


* Zorg er voor dat de gebruiker een via een True/False input veld opgeeft of de AutoscaleUp ("ScheduledActionUP") en Autoscaledown ("ScheduledActionDown") resource al dan niet uitgevoerd moet worden.
  * Controleer je template en kijk of er bij een "False" geen "Scheduled Actions" aangemaakt worden in de AutoScaling Group.


{{<alert type="caution">}}
Sla je template op met de naam *_Labo_AutoScaling_TEAMxy_Condition.json_* en *laat dit resultaat eventjes zien aan je docent vooraleer verder te werken*..
{{</alert>}}