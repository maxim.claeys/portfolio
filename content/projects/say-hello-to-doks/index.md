---
title: "CI/CD Project"
description: "School project - Use gitlab CI/CD and absible to configure routers in a CML environment"
lead: "Introducing Doks, a Hugo theme helping you build modern documentation websites that are secure, fast, and SEO-ready — by default."
date: 2020-11-04T09:19:42+01:00
lastmod: 2020-11-04T09:19:42+01:00
draft: false
weight: 50
images: ["say-hello-to-doks.png", "clusteraftercreation.png"]
contributors: ["Maxim Claeys"]
mermaid: true
---


# title
![e](say-hello-to-doks.png)
![e](clusteraftercreation.png)

{{< mermaid class="bg-light text-center" >}}
graph TD
  A[Hard] -->|Text| B(Round)
  B --> C{Decision}
  C -->|One| D[Result 1]
  C -->|Two| E[Result 2]
{{< /mermaid >}}